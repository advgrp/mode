#include <a_samp>
#include <a_mysql>
#include <streamer>
#include <sscanf2>
#include <Pawn.CMD>
#include <md5>
#include <foreach>
#include <crp>
#include <crashdetect>
#include <dc_fixes>
#include <mxdate>
#include <TOTP>
 

/*����� ��������. ���� ����*/
#define _fix_gettime_count	(-3600*13)//������� 13 �����, �� ������ ����� ���.  
#include <fix\\fixtime>
//====================================[Define]==================================
//---------------------------------[MySQL Settings]-----------------------------
new MySQL:dbHandle;

#define 	SERVER_LOCAL   2
#if SERVER_LOCAL == 1

#define 	MYSQL_HOST				"localhost"
#define 	MYSQL_USER				"root"
#define 	MYSQL_PASS				""
#define 	MYSQL_DATABASE			"attract_null"

#else

#define 	MYSQL_HOST				"127.0.0.1"
#define 	MYSQL_USER				"gs3469"
#define 	MYSQL_PASS				"pasha2001109"
#define 	MYSQL_DATABASE			"gs3469"
#endif
//---------------------------------[/MySQL Settings]----------------------------
#if defined MAX_PLAYERS
#undef MAX_PLAYERS
#endif
#define		MAX_PLAYERS 		50

#if !defined MAX_WEAPON_NAME 
	#define MAX_WEAPON_NAME 	(19) 
#endif

#if !defined IsValidVehicle
     native IsValidVehicle(vehicleid);
#endif

#define 	PASSWORD_MAX_LENGTH	(16)
//-----------------------[���������]--------------------------------------------
#define 	SERVER_MODE			"Advantage RolePlay"
#define 	SERVER_NAME			"Advantage RolePlay | ���� ������"
#define 	SERVER_MAP			"Criminal Russia"
#define     WEBSITE             "advantage-rp.ru"
#define     OFFICAL_VK_GROUP    "vk.com/advantagerp"
#define     LIBERTY_VK_GROUP    "vk.com/advantagerp"
//------------------------[/�����]-----------------------------------------------
#define     COLOR_WHITE   		0xFFFFFFFF
#define		COLOR_GREY			0x999999FF
#define     COLOR_YELLOW    	0xF7FF00FF
#define     COLOR_RED       	0xF0320CFF
#define     COLOR_ORANGEYELLOW  0xFFDF0FFF
#define     COLOR_LIGHTRED      0xFF4530FF
#define     COLOR_LIGHTGREY     0xc2c2c2FF
#define     COLOR_BLUE          0x3657FFFF
#define 	COLOR_LIGHTBLUE 	0x0099FFAA
#define     COLOR_SALAD         0xA0FF33FF
#define     COLOR_GREEN         0x4BCC2BFF
#define     COLOR_ORANGE        0xFF6600FF
#define 	COLOR_DARKORANGE	0xFF6600FF
#define     COLOR_LIGHTYELLOW   0xce9a00FF
#define 	COLOR_DARKGREEN		0x99CC00FF
//------------------------[�����]-----------------------------------------------
#define 	REFERAL_CASH_REWARD	(50000)

#define 	MALE_SKINS_COUNT	(6)
#define 	FEMALE_SKINS_COUNT	(3)
#define 	SKIN_TYPE_MALE		(1)
#define 	SKIN_TYPE_FEMALE	(2)

#define 	SETTINGS_COUNT 		(4)

#define 	ADMIN_FIRST			(1)
#define 	ADMIN_SECOND		(2)
#define 	ADMIN_THIRD			(3)
#define 	ADMIN_FOURTH		(4)
#define 	ADMIN_FIFTH			(5)
#define 	ADMIN_SIXTH			(6)

#define     JOB_LOADER_SALARY   (50) // - ��� �������� �������� �� 1 ����
#define     JOB_LJ_SALARY       (300) // - �������� �������� �� 1...

#define 	CarPassCategory_A	(1)
#define 	CarPassCategory_B	(2)
#define 	CarPassCategory_C	(3)
#define 	CarPassCategory_D	(4)

//====================================[/Define]=================================


//======================================[News]==================================
const EXP_TO_NEXT_LEVEL = 2; // ���������� �� 2 EXP � ������� ������� ������.

new ServerLoadTime;
new Iterator:AdmVeh<MAX_VEHICLES>;
new Job_Area[2];
new PayDayMultiplier = 1;
//-----------------------------------[Player Info]------------------------------
enum pInfo
{
	pID,
	pName[MAX_PLAYER_NAME+1],
	pEmail[32],
	pSex,
	pLevel,
	pSkin,
	Float:pHealth,
	Float:pArmour,
	pExp, 
	pSpawn,
	pSettings,
	pAdmin,
	pReferalid,
	pCash,
	pGoogleAuth[16+1],
	pCarLic[4],
}

enum
	E_GLOBAL_MAP_ICONS
{
	Float: map_icon_x,
	Float: map_icon_y,
	Float: map_icon_z,
	map_icon_type,
};

//--------------------[������� ���������]---------------------------------------
new vehicle_driving_school[9]; // - ���������� ��� ���������
new pLessonCar[MAX_PLAYERS]; // - ��� ����������
//-------------------------------[/������]---------------------------------------

//-----------------------------[���������]--------------------------------------
new entrance_autoschool[3]; // 1 - ���� � ���������, 2 - ����� � ���������, 3 - ��������� ��.
//------------------------------------------------------------------------------

new pData[MAX_PLAYERS][pInfo];
new pNullData[pInfo];
new DerevoTimer[MAX_PLAYERS];

new bool:UpdateSpeed[MAX_PLAYERS] = {false, ...};
new bool: Player_Gps_Status[MAX_PLAYERS] = {false, ...};
new Float: player_gps_pos[MAX_PLAYERS][3];

new pLoginTimer[MAX_PLAYERS];
new bool:pLogged[MAX_PLAYERS];
new pOnlineTime[MAX_PLAYERS];
new PlayerAFK[MAX_PLAYERS];
new pLastAct[MAX_PLAYERS];
new bool:pFreezed[MAX_PLAYERS];
new Player_Anims_Preloaded[MAX_PLAYERS char] = {false, ...};

enum (<<=1)
{
	pShowChat = 1,
	pShowNames,
	pShowAdmActions,
	pChatShowId,
}
//-----------------------------------[/Player Info]-------------------------------------

//-----------------------------------[Vehicle Info]-------------------------------------
enum vInfo
{
	vEngine,
	vFuel, 
	Float:vHealth, 
	Float:vMileage,
	vLight,
	vDoors,
	vLeftIndicator,
	vRightIndicator,
}
new VehData[MAX_VEHICLES][vInfo];

static const stock VehicleName[][] =
{
	{"BMW X5"}, // 400
	{"��-2715"}, // 401 ��-2715
	{"���l �m�g� B"}, // 402
	{"��� 133 05�"}, // 403 ��� 133 05�
	{"��� 2101"}, // 404 ��� 2101
	{"Mitsubishi L�n��r"}, // 405
	{"��� ���-555"}, // 406 ��� ���-555
	{"��������"}, // 407 ��������
	{"Tr�shm�st�r"}, // 408
	{"��� 14"}, // 409 ��� 14
	{"Mitsubishi ��li�s�"}, // 410
	{"L�d� ������"}, // 411 L�d� ������
	{"�� 21251"}, // 412 �� 21251
	{"��� 672"}, // 413 ��� 672
	{"����-52565"}, // 414 ����-52565
	{"�h��t�h"}, // 415
	{"������ ������"}, // 416 ������ ������
	{"L�vi�th�n"}, // 417
	{"M��nb��m"}, // 418
	{"�s��r�nt"}, // 419
	{"��� 2401 �����"}, // 420 ��� 2401 �����
	{"W�shingt�n"}, // 421
	{"��� 3741"}, // 422 ��� 3741
	{"Mrwh���"}, // 423
	{"��� �-3�"}, // 424 ��� �-3�
	{"��-24"}, // 425 ��-24
	{"���-41048"}, // 426 ���-41048
	{"������ ������"}, // 427 ������ ������
	{"������ 3221"}, // 428 ������ 3221
	{"��� �22"}, // 429 ��� �22
	{"�r�d�t�r"}, // 430
	{"����-677�"}, // 431 ����-677�
	{"����"}, // 432 ����
	{"����"}, // 433 ����
	{"��t�nif�"}, // 434
	{"������"}, // 435 ������
	{"T���t� M�r� II"}, // 436
	{"������ 260"}, // 437 ������ 260
	{"���-2402 �����"}, // 438 ���-2402 �����
	{"��-412"}, // 439 ��-412
	{"Rum��"}, // 440
	{"R� B�ndit"}, // 441
	{"��� 11-73"}, // 442 ��� 11-73
	{"�����r"}, // 443
	{"M�nst�r"}, // 444
	{"���-24"}, // 445 ���-24
	{"Squ�l�"}, // 446
	{"S��s��rr�w"}, // 447
	{"�izz�b��"}, // 448
	{"Tr�m"}, // 449
	{"������"}, // 450 ������
	{"Turism�"}, // 451
	{"S���d�r"}, // 452
	{"R��f�r"}, // 453
	{"Tr��i�"}, // 454
	{"�������"}, // 455 �������
	{"���-3309"}, // 456 ���-3309
	{"G�lf ��r"}, // 457
	{"���-2412"}, // 458 ���-2412
	{"B�r�l��'s R� V�n"}, // 459
	{"S�imm�r"}, // 460
	{"��� 380"}, // 461 ��� 380
	{"F�ggi�"}, // 462 �����
	{"Fr��w��"}, // 463
	{"R� B�r�n"}, // 464
	{"R� R�id�r"}, // 465
	{"BMW 535i"}, // 466
	{"�� 412 ��"}, // 467 �� 412 ��
	{"S�n�h�z"}, // 468
	{"S��rr�w"}, // 469
	{"�umm�r"}, // 470
	{"Qu�d"}, // 471
	{"�����"}, // 472
	{"�����"}, // 473
	{"��� 21"}, // 474 ��� 21
	{"Mitsubishi ��j�r�"}, // 475
	{"�������"}, // 476 �������
	{"ZR-350"}, // 477
	{"��-27151"}, // 478 ��-27151
	{"��� 2402"}, // 479 ��� 2402
	{"��m�t"}, // 480
	{"����"}, // 481 ����
	{"Burrit�"}, // 482
	{"��� 3205"}, // 483 ��� 3205
	{"�������"}, // 484 �������
	{"��� 2402 ��������"}, // 485 ��� 2402 ��������
	{"���������"}, // 486 ���������
	{"��������"}, // 487 ��������
	{"��������� ��������"}, // 488 ��������� ��������
	{"�h�vr�l�t Niv�"}, // 489
	{"FBI R�n�h�r"}, // 490
	{"Virg�"}, // 491
	{"��� 2109"}, // 492 ��� 2109
	{"�����"}, // 493 �����
	{"��tring"}, // 494
	{"S�nd�ing"}, // 495
	{"��� 1102 ������"}, // 496 ��� 1102 ������
	{"����������� ��������"}, // 497 ����������� ��������
	{"��� 53"}, // 498 ��� 53
	{"���-69�"}, // 499 ���-69�
	{"T�st"}, // 500
	{"R� G�blin"}, // 500
	{"��tring R���r"}, // 501
	{"��tring R���r"}, // 502
	{"Bl��dring B�ng�r"}, // 503
	{"R�n�h�r"}, // 504
	{"Su��r GT"}, // 505
	{"��� 2401"}, // 506 ��� 2401
	{"��� 2206"}, // 507 ��� 2206
	{"�������"}, // 508 �������
	{"������"}, // 509 ������
	{"�������"}, // 510 �������
	{"����������"}, // 511 ����������
	{"����������"}, // 512 ����������
	{"����� 54115"}, // 513 ����� 54115
	{"��� 608�"}, // 514 ��� 608�
	{"��� 21099"}, // 515 ��� 21099
	{"��� 2203"}, // 516 ��� 2203
	{"����-762"}, // 517 ����-762
	{"��-40"}, // 518 ��-40
	{"�����������"}, // 519 �����������
	{"��-������� 5"}, // 520 ��-������� 5
	{"NRG-500"}, // 521
	{"����-���"}, // 522 ����-���
	{"����������"}, // 523 ����������
	{"���������"}, // 524 ���������
	{"F�rtun�"}, // 525
	{"��dr�n�"}, // 526
	{"������ ���"}, // 527 ������ ���
	{"Will�rd"}, // 528
	{"��������������"}, // 529 ��������������
	{"�40-�"}, // 530 �40-�
	{"�������"}, // 531 �������
	{"F�ltz�r"}, // 532
	{"R�mingt�n"}, // 533
	{"Sl�mv�n"}, // 534
	{"Bl�d�"}, // 535
	{"�����"}, // 536 �����
	{"����������"}, // 537 ����������
	{"�������"},// 538 �������
	{"��� 3111"}, // 539 ��� 3111
	{"F�RD GT"}, // 540
	{"���� 2121"}, // 541
	{"������� 434"}, // 542 ������� 434
	{"��������"}, // 543 ��������
	{"������� 440"}, // 544 ������� 440
	{"�� 2125"}, // 545 �� 2125
	{"������� 2140"}, // 546 ������� 2140
	{"��rg�b�b"}, // 547
	{"���-968�"}, // 548 ���-968�
	{"Sunris�"}, // 549
	{"��� 31105"}, // 550 ��� 31105
	{"��������"}, // 551 ��������
	{"�������"}, // 552 �������
	{"��� 3303"}, // 553 ��� 3303
	{"��� 968�"}, // 554 ��� 968�
	{"M�nst�r"}, // 555
	{"M�nst�r"}, // 556
	{"Ur�nus"}, // 557
	{"T���t� ��li��"}, // 558
	{"Sult�n"}, // 559
	{"������� 427"}, // 560 ������� 427
	{"�l�g�"}, // 561
	{"R�ind�n��"}, // 562
	{"R� Tig�r"}, // 563
	{"��� 2108"}, // 564 ��� 2108
	{"��� 2104"}, // 565 ��� 2104
	{"S�v�nn�"}, // 566
	{"B�ndit�"}, // 567
	{"�����"}, // 568 �����
	{"����� ����������"}, // 569 ����� ����������
	{"�������"}, // 570 �������
	{"�������������"}, // 571 �������������
	{"����"}, // 572 ����
	{"������� ����"}, // 573 ������� ����
	{"��� �20"}, // 574 ��� �20
	{"������� 408"}, // 575 ������� 408
	{"�T-400"}, // 576
	{"��� 157"}, // 577 ��� 157
	{"Gelendwagen"}, // 578
	{"��� 13"}, // 579 ��� 13
	{"BF-400"}, // 580
	{"N�wsv�n"}, // 581
	{"�����������"}, // 582 �����������
	{"������ ���������"}, // 583 ������ ���������
	{"�m��r�r"}, // 584
	{"������ 3�"}, // 585 ������ 3�
	{"�ur�s"}, // 586
	{"���� 677 ����"}, // 587 ���� 677 ����
	{"�lub"}, // 588
	{"�����"}, // 589 �����
	{"������"}, // 590 ������
	{"�ndr�m�d�"}, // 591
	{"����������"}, // 592 ����������
	{"������"}, // 593 ������
	{"L�un�h"}, // 594
	{"���-21 �������"}, // 595 ���-21 �������
	{"��� 21099 �������"}, // 596 ��� 21099 �������
	{"��� 1114 �������"}, // 597 ��� 1114 �������
	{"��� �������"}, // 598 ��� ������
	{"���� 2335"}, // 599 ���� 2335
	{"S.W.�.T."}, // 600
	{"�l�h�"}, // 601
	{"�h��ni�"}, // 602
	{"Gl�nd�l�"}, // 603
	{"������"}, // 604 ������
	{"�����"}, // 605 �����
	{"�����"}, // 606 �����
	{"��������"}, // 607 ��������
	{"B��vill�"}, // 608
	{"����"}, // 609 ����
	{"�����"} // 610 �����
};

static const stock Float:Tree[][3] = {
	{2027.3979, 1655.8640, 15.3554},
	{2024.0251, 1648.7725, 15.3793},
	{2020.7529, 1657.0375, 15.4025},
	{2010.8778, 1655.7705, 15.4724},
	{2019.2876, 1635.2832, 15.4314},
	{2031.0734,1634.7625,15.3342}
};
//-----------------------------------[/Vehicle Info]------------------------------------

//-------------------------------------[TextDraws]--------------------------------------
new Text:TD_server_logo[5],
	Text:RegSkinTD[9],
	Text:RegSkinNumbTD[2][6];

const SpeedTDStaticSize = 18;
new Text:SpeedTDStatic[SpeedTDStaticSize];

const SpeedTDDynamicSize = 12;
new PlayerText:SpeedTDDynamic[MAX_PLAYERS][SpeedTDDynamicSize];

const JobLoaderTDSize = 10;
new PlayerText:JobLoaderTD[JobLoaderTDSize][MAX_PLAYERS];
//-------------------------------------[/TextDraws]-------------------------------------
enum dInfo
{
	dNull,
	dRegistration,
	dRegistration2,
	dRegistration3,
	dRegistration4,
	dRegistration5,
	dRegistration6,
	dAuth,
	dMenu,
	dMenuVisual,
	dMenuReport,
	dMenuPrivateSettings,
	dGps,
	dGps1,
	dGps2,
	dGps3,
	dGps4,
	dChangeName,
	dChangePassword,
	dGoogleAuth,
    dGoogleAuth2,
    dGoogleAuth3,
    dGoogleAuth4,
    dJobLoader,
    dJobLoader1,
    dJobLumberJack,
    dJobLumberJack1,
    dDriver_License
}
enum checkpoint_autoschool
{
	CHECKPOINT_1 = 1,
	CHECKPOINT_2,
	CHECKPOINT_3,
	CHECKPOINT_4,
	CHECKPOINT_5,
	CHECKPOINT_6,
	CHECKPOINT_7,
	CHECKPOINT_8,
	CHECKPOINT_9,
	CHECKPOINT_10,
	CHECKPOINT_11,
	CHECKPOINT_13,
	CHECKPOINT_14,
	CHECKPOINT_15,
	CHECKPOINT_16,
	CHECKPOINT_17,
	CHECKPOINT_18,
	CHECKPOINT_19,
	CHECKPOINT_20,
	CHECKPOINT_21,
	CHECKPOINT_22,
	CHECKPOINT_23,
	CHECKPOINT_24,
	CHECKPOINT_25,
	CHECKPOINT_26,
	CHECKPOINT_27,
}
//----------------------------------[RegistrationSkin]----------------------------------
new RegSkins[][] = 
{
	{14,20,21,22,24,25},
	{12, 13, 56}
};
//----------------------------------[/RegistrationSkin]---------------------------------
static const stock SettingName[SETTINGS_COUNT][] = {
	"���������� ����� ���\t\t\t",
	"���������� ���� �������\t\t\t",
	"���������� �������� �������������\t",
	"���������� ID ������� � ����\t\t"
};
static const stock GoogleCode[32][] ={
	"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "2", "3", "4", "5", "6", "7"
};
//======================================[/News]=========================================
main(){}
public OnGameModeInit()
{
	ServerLoadTime = GetTickCount();
	mysql_connects();

	Streamer_SetVisibleItems(0, 650);
	DisableInteriorEnterExits();
	EnableStuntBonusForAll(0);
	ShowPlayerMarkers(PLAYER_MARKERS_MODE_STREAMED);
	ManualVehicleEngineAndLights();

	SetGameModeText(""SERVER_MODE"");
	SendRconCommand("hostname "SERVER_NAME"");
	SendRconCommand("mapname "SERVER_MAP"");
	SendRconCommand("weburl "WEBSITE"");

	#include <TD\\Logotype.inc>
	#include <map\\map.inc>
	#include <map\\ChooseSkin.inc>
	#include <TD\\regskin.inc>
	#include <TD\\SpeedTD.inc>
	#include <map\\meria.inc>
	
	LoadPickup(); // - �������� �������
	new LastVehicleID = LoadVehicles(); // - �������� ����������� ��� �������
	
	for(new i = 0; i <= LastVehicleID; i++)
	{
	    if(!IsValidVehicle(i)) continue;
		VehData[i][vFuel] = 50;
		VehData[i][vMileage] = 0.0;
		VehData[i][vEngine] = false;
		VehData[i][vHealth] = 1000.0;
		SetVehicleParamsEx(i, false, false, false, false, false, false, false);
	}

	new hour;
	gettime(hour,_,_);
	SetWorldTime(hour);

	SetTimer("@__MinuteTimer",  60*1000, false);
	SetTimer("@__SecondsTimer", 1000,  false);
	SetTimer("@__FuelUpdate", 15*1000, false);
	printf("������ �������� �� %d ��", GetTickCount() - ServerLoadTime);
	return 1;
}

public OnGameModeExit()
{
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	SetSpawnInfo(playerid, 255, 0, 0.0, 0.0, 2.5, 0.0, 0, 0, 0, 0, 0, 0);
 	TogglePlayerSpectating(playerid, true);

 	if(pLogged[playerid]) 
	{
		TogglePlayerSpectating(playerid, false);
	}
	else
	{
		SetTimerEx("@__SetCameraWhenPlayerConnect", 150, false, "i", playerid);
		SendClientMessage(playerid, 0x0099FFAA, !"����� ���������� �� Advantage RP");
	}
	return 1;
}

public OnPlayerConnect(playerid)
{
	ClearPlayerData(playerid);
	
	GetPlayerName(playerid, pData[playerid][pName], MAX_PLAYER_NAME + 1);
	CheckPlayerName(playerid);

	ShowPlayerLogotype(playerid);
	SetTimerEx("@__PlayerConnected", 200, false, "i", playerid);
	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
	if(!pLogged[playerid]) return 0;
    //------------------------[���������� ������]-------------------------------
    if(GetPVarInt(playerid,"InJob") == 1)// - ������ ��������
    {
	   	pData[playerid][pCash] += GetPVarInt(playerid,"JobY")*JOB_LOADER_SALARY;
	    SetPlayerMoney(playerid, pData[playerid][pCash]);

	    DeletePVar(playerid,"Job");
	    DeletePVar(playerid,"InJob");
	    DeletePVar(playerid,"JobY");
	    DeletePVar(playerid,"PSkin");
    }
    if(GetPVarInt(playerid,"InJob") == 2)// - ������ ��������
    {
	   	pData[playerid][pCash] += GetPVarInt(playerid,"JobY")*JOB_LJ_SALARY;
	    SetPlayerMoney(playerid, pData[playerid][pCash]);

	    DeletePVar(playerid,"Job");
	    DeletePVar(playerid,"InJob");
	    DeletePVar(playerid,"JobY");
	    DeletePVar(playerid,"PSkin");
		DeletePVar(playerid, "LessStatus");
	}
    //------------------------[���������� ������]-------------------------------
	UpdatePlayerData(playerid, "playedtime", pOnlineTime[playerid]);
	ClearPlayerData(playerid);
	DestroyPlayerSpeedTD(playerid);
	return 1;
}

public OnPlayerSpawn(playerid)
{
	if(!pLogged[playerid]) return
	    SendClientMessage(playerid, COLOR_RED, !"�� �� ������������"), Kick(playerid);
	    
	TogglePlayerControllable(playerid, true);
 	PlayerAFK[playerid] = 0;

	if(GetPVarInt(playerid, "RegistrationSpawn") == 1)
	{
		TogglePlayerControllable(playerid, false);
		SetPlayerPos(playerid, 1828.9404,2248.6221,312.3576);
		SetPlayerInterior(playerid, 4);
		SetPlayerVirtualWorld(playerid, playerid+10);
		SetPlayerCameraPos(playerid, 1828.8966,2243.8474,313.3576);
		SetPlayerCameraLookAt(playerid, 1828.9404,2248.6221,312.3576);
		SetPlayerFacingAngle(playerid, 180);
		SetPVarInt(playerid, "RegChooseSkin", 1);
		SetPVarInt(playerid, "RegistrationSpawn", 0);
		SetPlayerSkin(playerid, RegSkins[pData[playerid][pSex]-1][0]);
	}
	else
	{
		switch(pData[playerid][pSpawn])
		{
			case 1:
			{
				SetPlayerPos(playerid, 1789.9006,2173.8508,15.9916);
				SetPlayerSkin(playerid, pData[playerid][pSkin]);
				SetPlayerColor(playerid, 0xFFFFFF40);
				SetPlayerInterior(playerid, 0);
				SetPlayerVirtualWorld(playerid, 0);
				SetPlayerFacingAngle(playerid, 274.3186);
				SetCameraBehindPlayer(playerid);
			}
		}
	}
	if(!Player_Anims_Preloaded[playerid])
	{
		PreloadAnimLib(playerid,"BOMBER");
		PreloadAnimLib(playerid,"RAPPING");
		PreloadAnimLib(playerid,"SHOP");
		PreloadAnimLib(playerid,"BEACH");
		PreloadAnimLib(playerid,"SMOKING");
		PreloadAnimLib(playerid,"FOOD");
		PreloadAnimLib(playerid,"ON_LOOKERS");
		PreloadAnimLib(playerid,"DEALER");
		PreloadAnimLib(playerid,"CRACK");
		PreloadAnimLib(playerid,"CARRY");
		PreloadAnimLib(playerid,"COP_AMBIENT");
		PreloadAnimLib(playerid,"PARK");
		PreloadAnimLib(playerid,"INT_HOUSE");
		PreloadAnimLib(playerid,"FOOD");
		PreloadAnimLib(playerid,"PED");

		Player_Anims_Preloaded[playerid] = 1;
	}
	return 1;
}

public OnPlayerDeath(playerid, killerid, reason)
{
	PlayerAFK[playerid] = -2;

	pData[playerid][pArmour] = 0.0;
	pData[playerid][pHealth] = 100.0;

	return 1;
}

public OnVehicleSpawn(vehicleid)
{
	if(Itter_Contains(AdmVeh, vehicleid))
	{
		//DestroyVehicle(vehicleid);
		//Iter_Remove(AdmVeh, vehicleid);
	}
	return 1;
}

public OnVehicleDamageStatusUpdate(vehicleid, playerid)
{
	if(GetPlayerVehicleSeat(playerid) == 0 && pData[playerid][pAdmin] >= ADMIN_SIXTH)
	{
		SetVehicleHealth(vehicleid, 1000);
		RepairVehicle(vehicleid);
	}
	return 1;
}

public OnVehicleDeath(vehicleid, killerid)
{
	return 1;
}

public OnPlayerText(playerid, text[])
{
	if(!pLogged[playerid]) return 1;

	if(GetTickCount() - pLastAct[playerid] < 400) 
		return 0;
    pLastAct[playerid] = GetTickCount();

	if(!(pData[playerid][pSettings] & pShowChat)) 
		return SendClientMessage(playerid, COLOR_RED, !"�� ��������� ���"), 0;

	if(strlen(text) > 90) 
		return SendClientMessage(playerid, COLOR_RED, !"���� ��������� ������� �������"), 0; 

	if(!strcmp(text, ")", true))
	{
	    format(text, 144, "%s ���������", pData[playerid][pName]);
	    ProxDetector(15.0, playerid, text, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	    return 0;
	}
	else if(!strcmp(text, "))", true))
	{
	    format(text, 144, "%s ������", pData[playerid][pName]);
	    ProxDetector(15.0, playerid, text, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	    return 0;
	}
	else if(!strcmp(text, "(", true))
	{
	    format(text, 144, "%s �����������", pData[playerid][pName]);
	    ProxDetector(15.0, playerid, text, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	    return 0;
	}
	else if(!strcmp(text, "((", true))
	{
	    format(text, 144, "%s ������ �����������", pData[playerid][pName]);
	    ProxDetector(15.0, playerid, text, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	    ApplyAnimation(playerid,"GRAVEYARD","mrnF_loop",4.1,0,0,0,0,0,1);
	    return 0;
	}
	else if(!strcmp(text, "=0", true))
	{
	    format(text, 144, "%s ��������", pData[playerid][pName]);
	    ProxDetector(15.0, playerid, text, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	    return 0;
	}
	else ProxDetectorChat(15.0, playerid, text, 0xFFFFFFFF, 0xFFFFFFFF, 0xF5F5F5FF, 0xE6E6E6FF,0xB8B8B8FF);
	return 0;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
	return 0;
}

public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	if(ispassenger == 0)/*��������*/
	{
		new pCarLicCategory = GetPVarInt(playerid, "Category");
		if(pCarLicCategory == 0)/*�� ����� �� �����*/
		{
			if(vehicle_driving_school[0] <= vehicleid <= vehicle_driving_school[8]) 
			{
				SendClientMessage(playerid, COLOR_DARKORANGE, !"���� ��������� ����������� ���������");
				return ClearAnimations(playerid);
			}
			else if(ScooterVeh(vehicleid) && !BikeVeh(vehicleid) && pData[playerid][pCarLic][0] == 0)
		    {
			    SendClientMessage(playerid, COLOR_DARKORANGE, !"� ��� ��� ������������� ������������� ��������� A!");
				return ClearAnimations(playerid);
		    }
		    else if(!BikeVeh(vehicleid) && !ScooterVeh(vehicleid) && pData[playerid][pCarLic][1] == 0)
		    {
			    SendClientMessage(playerid, COLOR_DARKORANGE, !"� ��� ��� ������������� ������������� ��������� B!");
				return ClearAnimations(playerid);
			}
		    else if(CargoVeh(vehicleid) && !BikeVeh(vehicleid) && pData[playerid][pCarLic][2] == 0)
		    {
			    SendClientMessage(playerid, COLOR_DARKORANGE, !"� ��� ��� ������������� ������������� ��������� C!");
				return ClearAnimations(playerid);
		    }
		    else if(BusVeh(vehicleid) && !BikeVeh(vehicleid) && pData[playerid][pCarLic][3] == 0)
		    {
			    SendClientMessage(playerid, COLOR_DARKORANGE, !"� ��� ��� ������������� ������������� ��������� D!");
				return ClearAnimations(playerid);
		    }
		}
		else
	    //-------------------------------[����� �� �����]--------------------
		{
			new CarpassExamVehid = GetPVarInt(playerid, "CarpassExamVehid");
			if(CarpassExamVehid == 0)
			{
				if(!(vehicle_driving_school[0] <= vehicleid <= vehicle_driving_school[8]))
				{
					SendClientMessage(playerid, COLOR_DARKORANGE, !"�� ������ ������� �� �������� � ������ ����� � ������������������ ��� ����� ���������");
					return ClearAnimations(playerid);
				}
				else if((vehicleid >= vehicle_driving_school[0] && vehicleid <= vehicle_driving_school[3]) && pCarLicCategory != CarPassCategory_B)
				{
				    SendClientMessage(playerid, COLOR_DARKORANGE, !"���� ��������� ��� ����� �� ����� ��������� B.");
					return ClearAnimations(playerid);
				}
			    else if((vehicleid >= vehicle_driving_school[4] && vehicleid <= vehicle_driving_school[5]) && pCarLicCategory != CarPassCategory_D)
				{
				    SendClientMessage(playerid, COLOR_DARKORANGE, !"���� ��������� ��� ����� �� ����� ��������� D.");
					return ClearAnimations(playerid);
				}
			    else if((vehicleid >= vehicle_driving_school[6] && vehicleid <= vehicle_driving_school[7]) && pCarLicCategory != CarPassCategory_A)
				{
				    SendClientMessage(playerid, COLOR_DARKORANGE, !"���� ��������� ��� ����� �� ����� ��������� A.");
					return ClearAnimations(playerid);
				}
			    else if(vehicleid == vehicle_driving_school[8] && pCarLicCategory != CarPassCategory_C)
				{
				    SendClientMessage(playerid, COLOR_DARKORANGE, !"���� ��������� ��� ����� �� ����� ��������� C.");
					return ClearAnimations(playerid);
				}
			}
			else if(CarpassExamVehid != vehicleid)
			{
				SendClientMessage(playerid, COLOR_DARKORANGE, !"�� ������ ����� � ��� ����������, �� ������� ������ �������!");
				return ClearAnimations(playerid);
			}
		}	
	}
	return 1;
}

public OnPlayerExitVehicle(playerid, vehicleid)
{
	return 1;
}

public OnPlayerStateChange(playerid, newstate, oldstate)
{
	if(newstate == PLAYER_STATE_DRIVER)
	{
		if(GetPlayerVehicleSeat(playerid) == 0)
		{
			new vehicleid = GetPlayerVehicleID(playerid);
			new veh = GetVehicleModel(vehicleid);

			PlayerTextDrawSetString(playerid, SpeedTDDynamic[playerid][10], VehicleName[veh-400]);

			for(new idx = 5; idx <= 7; idx++)
				PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][idx], VehData[vehicleid-1][vLight] == 1 ? COLOR_ORANGE : COLOR_WHITE);

			PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][9], VehData[vehicleid-1][vEngine] == 1 ? COLOR_ORANGE : COLOR_WHITE);

			PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][8], VehData[vehicleid-1][vDoors] == 0 ? COLOR_GREEN : COLOR_RED);

			ShowPlayerSpeedTD(playerid);
			UpdateSpeed[playerid] = true;
			SetTimerEx("@__SpeedUpdate", 500, false, "i", playerid);

			SendClientMessage(playerid, COLOR_WHITE, !"����������� ������ {0099FF}L.CTRL{FFFFFF} ��� ����, ����� �������/��������� ����������");
		
			/*-----------------------------------------[����� �� �����]------------------------------------------*/

			new Category = GetPVarInt(playerid, "Category");

			if(Category != 0)
			{
				if(GetPVarInt(playerid, "CarpassExamVehid") == 0)
				{
					static const categories[] = {"A", "B", "C", "D"};
				
					SetPlayerVirtualWorld(playerid, 1);
		            SetVehicleVirtualWorld(vehicleid, 1);
					SetPlayerRaceCheckpoint(playerid,0,1899.6517,2246.5349,15.9095,1887.0807,2201.9712,16.0329,5.0);
		
					static const fmt_str[] = "�� ������ ����� �� ����� ��������� %s";
					new string[sizeof fmt_str + (-2 + 1)];
		
					format(
						string, sizeof string,
						fmt_str,
						categories[Category-1]
					);
					SendClientMessage(playerid, COLOR_GREEN, string);
		
					SendClientMessage(playerid, COLOR_GREEN, "�� ������� ��� ����� ����� 10 �����!");
					SendClientMessage(playerid, COLOR_GREEN, "������ ���������� �� ������ � ���������� ��� ������� ���!");
					SendClientMessage(playerid, COLOR_GREEN, "���� �� ��������� ���������� ��� �� ������� ������� ��������, �� ������� ����� ��������!");
					SetPVarInt(playerid, "CarpassExamVehid", vehicleid);
					pLessonCar[playerid] = CHECKPOINT_1;
					SetPVarInt(playerid, "LESSONTIME", gettime() + 600);
				}
			}
		}
	}
	else if(newstate == PLAYER_STATE_ONFOOT && oldstate == PLAYER_STATE_DRIVER)
	{
		/*������ ���������*/
		HidePlayerSpeedTD(playerid);
		UpdateSpeed[playerid] = false;
		/*����� �� �����*/
		if(GetPVarInt(playerid, "Category") != 0)
		{
			SendClientMessage(playerid, COLOR_DARKORANGE, !"�� �� ��������� ������� �� ��������");
			SendClientMessage(playerid, COLOR_DARKORANGE, !"� ��� ���� 15 ������ ��� ����, ����� ��������� � ������");
			@__PlayerNotFinishedCarpassExam(playerid, 15);
		}
	}
	return 1;
}

public OnPlayerEnterCheckpoint(playerid)
{
	if(GetPVarInt(playerid,"Job") == 1)
    {
        if(IsPlayerInRangeOfPoint(playerid, 3, 1758.0327,2270.0376,15.8668))
	    {
            ApplyAnimation(playerid,"CARRY","crry_prtial",4.1,0,1,1,1,1);
            SetPlayerAttachedObject(playerid, 1 , 3052, 1,0.11,0.36,0.0,0.0,90.0);

            SetPVarInt(playerid,"Job",2);

	        SetPlayerCheckpoint(playerid, 1781.5320,2270.1335,15.7895,1.5);
        }
    }
    if(GetPVarInt(playerid,"Job") == 2)
    {
        if(IsPlayerInRangeOfPoint(playerid, 3, 1781.5320,2270.1335,15.7895))
        {
		    new str[148];

		    DisablePlayerCheckpoint(playerid);
		    RemovePlayerAttachedObject(playerid,1);
		    ApplyAnimation(playerid,"PED","IDLE_tired",4.1,0,1,1,0,1);

		    SetPVarInt(playerid,"Job",1);
		    SetPVarInt(playerid,"JobY",GetPVarInt(playerid,"JobY")+1);

		    format(
				str,sizeof(str), "�� �������� ����. ����� ����������: {FF9900}%d {FFFFFF}������.",
				GetPVarInt(playerid,"JobY")
			);
			SendClientMessage(playerid,COLOR_WHITE,str);

			format(
				str,14,"%d", GetPVarInt(playerid,"JobY")
			);
            PlayerTextDrawSetString(playerid, JobLoaderTD[7][playerid], str);

			format(
				str,14,"%d", GetPVarInt(playerid,"JobY")*JOB_LOADER_SALARY
			);
            PlayerTextDrawSetString(playerid, JobLoaderTD[8][playerid], str);

		    SetPlayerCheckpoint(playerid, 1758.0327,2270.0376,15.8668,2.0);
	    }
    }
    if(GetPVarInt(playerid,"Job") == 3)
	{
        TogglePlayerControllable(playerid, 0);
        ApplyAnimation(playerid,"CHAINSAW","WEAPON_csaw",1.0,1,0,0,0,6200,0);
        DerevoTimer[playerid] = SetTimerEx("@__GateTree", 7000, false, "playerid", playerid);
	}
	if(GetPVarInt(playerid,"Job") == 4 && IsPlayerInRangeOfPoint(playerid, 3.0, 2005.1436,1620.6907,15.7599))
	{
        new checkpoint_id = RandomEX(0,4);
        new str[100];

	    DisablePlayerCheckpoint(playerid);
	    RemovePlayerAttachedObject(playerid, 4);

	    SetPVarInt(playerid,"Job",3);
	    SetPVarInt(playerid,"JobY",GetPVarInt(playerid,"JobY")+1);
	    SetPVarInt(playerid, "LessStatus", 0);

	    ClearAnimations(playerid);
		ApplyAnimation(playerid,"PED","IDLE_tired",4.1,0,1,1,0,1);

		SetPlayerAttachedObject(playerid, 3, 341, 6, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000, 1.000000, 1.000000);
		SetPlayerCheckpoint(playerid,Tree[checkpoint_id][0],Tree[checkpoint_id][1],Tree[checkpoint_id][2], 2.0);

	    format(
			str,sizeof(str), "�� �������� ����� �� �����. ����� ���������� : {FF9900}%d {FFFFFF}����.",
			GetPVarInt(playerid,"JobY")
		);
		SendClientMessage(playerid,COLOR_WHITE,str);
	}
	return 1;
}

public OnPlayerLeaveCheckpoint(playerid)
{
	return 1;
}

public OnPlayerEnterRaceCheckpoint(playerid)
{
	// - ����� �� �����
	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
	{
		if(GetPVarInt(playerid, "Category") >= 0)
		{
			switch(pLessonCar[playerid])
			{
			case CHECKPOINT_1:
				{

					pLessonCar[playerid] = CHECKPOINT_2;
					SetPlayerRaceCheckpoint(playerid, 0,1887.0807,2201.9712,16.0329,1887.1703,2141.7307,16.0340, 5.0);
				}
			case CHECKPOINT_2:
				{

					pLessonCar[playerid] = CHECKPOINT_3;
					SetPlayerRaceCheckpoint(playerid, 0,1887.1703,2141.7307,16.0340,1887.2716,2065.8665,16.0506, 5.0);
				}
			case CHECKPOINT_3:
				{

					pLessonCar[playerid] = CHECKPOINT_4;
					SetPlayerRaceCheckpoint(playerid, 0,1887.2716,2065.8665,16.0506,1961.3928,2033.7277,16.0333, 5.0);
				}
			case CHECKPOINT_4:
				{

					pLessonCar[playerid] = CHECKPOINT_5;
					SetPlayerRaceCheckpoint(playerid, 0,1961.3928,2033.7277,16.0333,1962.7013,1918.0162,15.6653, 5.0);
				}
			case CHECKPOINT_5:
				{

					pLessonCar[playerid] = CHECKPOINT_6;
					SetPlayerRaceCheckpoint(playerid, 0,1962.7013,1918.0162,15.6653,2042.0293,1903.9966,16.0687, 5.0);
				}
			case CHECKPOINT_6:
				{

					pLessonCar[playerid] = CHECKPOINT_7;
					SetPlayerRaceCheckpoint(playerid, 0,2042.0293,1903.9966,16.0687,2231.6516,1903.4723,16.0765, 5.0);
				}
			case CHECKPOINT_7:
				{

					pLessonCar[playerid] = CHECKPOINT_8;
					SetPlayerRaceCheckpoint(playerid, 0,2231.6516,1903.4723,16.0765,2355.4775,1910.8282,16.0987, 5.0);
				}
			case CHECKPOINT_8:
				{

					pLessonCar[playerid] = CHECKPOINT_9;
					SetPlayerRaceCheckpoint(playerid, 0,2355.4775,1910.8282,16.0987,2450.4067,1894.6167,16.3244, 5.0);
				}
			case CHECKPOINT_9:
				{

					pLessonCar[playerid] = CHECKPOINT_10;
					SetPlayerRaceCheckpoint(playerid, 0,2450.4067,1894.6167,16.3244,2575.0854,1894.3824,16.3788, 5.0);
				}
			case CHECKPOINT_10:
				{

					pLessonCar[playerid] = CHECKPOINT_11;
					SetPlayerRaceCheckpoint(playerid, 0,2575.0854,1894.3824,16.3788,2714.0862,1896.9119,16.4075, 5.0);
				}
			case CHECKPOINT_11:
				{

					pLessonCar[playerid] = CHECKPOINT_13;
					SetPlayerRaceCheckpoint(playerid, 0,2714.0862,1896.9119,16.4075,2755.3105,1925.2411,16.7373, 5.0);
				}
			case CHECKPOINT_13:
				{

					pLessonCar[playerid] = CHECKPOINT_14;
					SetPlayerRaceCheckpoint(playerid, 0,2755.3105,1925.2411,16.7373,2761.7229,2067.5271,18.1328, 5.0);
				}
			case CHECKPOINT_14:
				{

					pLessonCar[playerid] = CHECKPOINT_15;
					SetPlayerRaceCheckpoint(playerid, 0,2761.7229,2067.5271,18.1328,2757.0786,2338.6079,16.2255, 5.0);
				}
			case CHECKPOINT_15:
				{

					pLessonCar[playerid] = CHECKPOINT_16;
					SetPlayerRaceCheckpoint(playerid, 0,2757.0786,2338.6079,16.2255,2716.0344,2587.1267,16.1037, 5.0);
				}
			case CHECKPOINT_16:
				{

					pLessonCar[playerid] = CHECKPOINT_17;
					SetPlayerRaceCheckpoint(playerid, 0,2716.0344,2587.1267,16.1037,2670.1716,2780.2522,19.0839, 5.0);
				}
			case CHECKPOINT_17:
				{

					pLessonCar[playerid] = CHECKPOINT_18;
					SetPlayerRaceCheckpoint(playerid, 0,2670.1716,2780.2522,19.0839,2406.0046,2958.2695,21.9037, 5.0);
				}
			case CHECKPOINT_18:
				{

					pLessonCar[playerid] = CHECKPOINT_19;
					SetPlayerRaceCheckpoint(playerid, 0,2406.0046,2958.2695,21.9037,2108.9197,2967.3086,11.8601, 5.0);
				}
			case CHECKPOINT_19:
				{

					pLessonCar[playerid] = CHECKPOINT_20;
					SetPlayerRaceCheckpoint(playerid, 0,2108.9197,2967.3086,11.8601,2055.1235,2871.2405,13.8533, 5.0);
				}
			case CHECKPOINT_20:
				{

					pLessonCar[playerid] = CHECKPOINT_21;
					SetPlayerRaceCheckpoint(playerid, 0,2055.1235,2871.2405,13.8533,1932.4110,2704.2976,15.4065, 5.0);
				}
			case CHECKPOINT_21:
				{

					pLessonCar[playerid] = CHECKPOINT_22;
					SetPlayerRaceCheckpoint(playerid, 0,1932.4110,2704.2976,15.4065,1784.3219,2562.0911,15.3670, 5.0);
				}
			case CHECKPOINT_22:
				{

					pLessonCar[playerid] = CHECKPOINT_23;
					SetPlayerRaceCheckpoint(playerid, 0,1784.3219,2562.0911,15.3670,1768.1477,2482.2603,16.1258, 5.0);
				}
			case CHECKPOINT_23:
				{

					pLessonCar[playerid] = CHECKPOINT_24;
					SetPlayerRaceCheckpoint(playerid, 0,1768.1477,2482.2603,16.1258,1858.2102,2346.9963,16.1209, 5.0);
				}
			case CHECKPOINT_24:
				{

					pLessonCar[playerid] = CHECKPOINT_25;
					SetPlayerRaceCheckpoint(playerid, 0,1858.2102,2346.9963,16.1209,1897.2427,2247.2283,15.9712, 5.0);
				}
			case CHECKPOINT_25:
				{

					pLessonCar[playerid] = CHECKPOINT_26;
					SetPlayerRaceCheckpoint(playerid, 0,1897.2427,2247.2283,15.9712,1915.2113,2246.1807,15.9017, 5.0);
				}
			case CHECKPOINT_26:
				{

					pLessonCar[playerid] = CHECKPOINT_27;
					SetPlayerRaceCheckpoint(playerid, 0,1915.2113,2246.1807,15.9017,0.0,0.0,0.0, 5.0);//
				}
			case CHECKPOINT_27:
				{
					new Float:hph,
						vehicleid = GetPlayerVehicleID(playerid);
					GetVehicleHealth(vehicleid,hph);
					if(hph >= 850)
					{
						switch(GetPVarInt(playerid, "Category"))
						{
							case 1:
							{
                                pData[playerid][pCarLic][0] = 1;
                                UpdatePlayerData(playerid, "car_lic_a", pData[playerid][pCarLic][0]);
							} 
							case 2:
							{
							    pData[playerid][pCarLic][1] = 1;
							    UpdatePlayerData(playerid, "car_lic_b", pData[playerid][pCarLic][1]);
							}
							case 3:
							{
							    pData[playerid][pCarLic][2] = 1;
							    UpdatePlayerData(playerid, "car_lic_c", pData[playerid][pCarLic][2]);
							}
							case 4:
							{
							    pData[playerid][pCarLic][3] = 1;
						    	UpdatePlayerData(playerid, "car_lic_d", pData[playerid][pCarLic][3]);
						    }
						}
						SendClientMessage(playerid, COLOR_BLUE, "�����������! �� ������� ��������� ������� �� ��������. ��� ���� �����.");
					}
					else
					{
						SendClientMessage(playerid, COLOR_DARKORANGE, "� ��������� �� ��������� ������� �� ��������. � ��������� ��� ������������ �� ���������� ���������.");
					}
					SetVehicleVirtualWorld(vehicleid, 0);
					SetPlayerVirtualWorld(playerid, 0);
					DeletePVar(playerid, "Category");
					DeletePVar(playerid, "CarpassExamVehid");
					DisablePlayerRaceCheckpoint(playerid);
					SetVehicleToRespawn(vehicleid);
				}
			}
		}
	}
	return 1;
}

public OnPlayerLeaveRaceCheckpoint(playerid)
{
	return 1;
}

public OnRconCommand(cmd[])
{
	return 1;
}

public OnPlayerRequestSpawn(playerid)
{
	return 1;
}

public OnObjectMoved(objectid)
{
	return 1;
}

public OnPlayerObjectMoved(playerid, objectid)
{
	return 1;
}

public OnPlayerPickUpPickup(playerid, pickupid)
{
	// - ���� � ���������
	if(pickupid == entrance_autoschool[0])
	{
		SetPlayerPos(playerid, 2313.0496,-1928.0374,2022.9600);
		FreezePlayer(playerid, 1500);
		SetPlayerInterior(playerid, 1);
		SetPlayerVirtualWorld(playerid, 1);
		SetPlayerFacingAngle(playerid, 90.4346);
		SetCameraBehindPlayer(playerid);
	}
	// - ����� � ���������
	else if(pickupid == entrance_autoschool[1])
	{
		SetPlayerPos(playerid, 1909.3533,2227.7549,15.7139);
		SetPlayerInterior(playerid, 0);
		SetPlayerVirtualWorld(playerid, 0);
		SetPlayerFacingAngle(playerid, 91.4276);
		SetCameraBehindPlayer(playerid);
	}
	return 1;
}

public OnVehicleMod(playerid, vehicleid, componentid)
{
	return 1;
}

public OnVehiclePaintjob(playerid, vehicleid, paintjobid)
{
	return 1;
}

public OnVehicleRespray(playerid, vehicleid, color1, color2)
{
	return 1;
}

public OnPlayerSelectedMenuRow(playerid, row)
{
	return 1;
}

public OnPlayerExitedMenu(playerid)
{
	return 1;
}

public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid)
{
	return 1;
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys) 
{
	if(newkeys & KEY_WALK)
	{
		// - ���������� �� ������ ���������
        if(IsPlayerInRangeOfPoint(playerid, 1, 1754.7878,2251.7822,15.8603))
        {
		 	if(GetPVarInt(playerid,"InJob") == 0)
               ShowPlayerDialog(playerid,dJobLoader,DIALOG_STYLE_MSGBOX,
			       !"{FFCC00}������ ��������","{FFFFFF}������ �������� ����������� �\n�������������� ������.\n\n�� ������ ������������ ����\n��� ������������ {689ddb}50 {FFFFFF}������ � ��������.\n\n{35AA28}�� ������������� ������ ������ ������?","������","���"
			   );

			else if(GetPVarInt(playerid,"InJob") == 1)
                    ShowPlayerDialog(playerid,dJobLoader1,DIALOG_STYLE_MSGBOX,
					!"{FFCC00}������ ��������","{FFFFFF}�� ������ ��������� ���������� ���������?","��","���"
				);
		}
		// - ���������� �� ������ ���������
        else if(IsPlayerInRangeOfPoint(playerid, 1, 1967.0968,1612.2507,16.3733))
        {
		 	if(GetPVarInt(playerid,"InJob") == 0)
               ShowPlayerDialog(playerid,dJobLumberJack,DIALOG_STYLE_MSGBOX,
			       !"{FFCC00}������ ��������","{FFFFFF}������ �������� ����������� �\n������� ��������.\n\n�� ������ ������������ ����� �� �����\n��� ������������ {689ddb}300 {FFFFFF}������ � ��������.\n\n{35AA28}�� ������������� ������ ������ ������?","������","���"
			   );

			else if(GetPVarInt(playerid,"InJob") == 2)
                    ShowPlayerDialog(playerid,dJobLumberJack1,DIALOG_STYLE_MSGBOX,
					!"{FFCC00}������ ��������","{FFFFFF}�� ������ ��������� ���������� ���������?","��","���"
				);
		}
		// - ����� �� �����
		else if(IsPlayerInRangeOfPoint(playerid,1,2303.5242,-1927.3441,2022.9600))
		{
            if(GetPVarInt(playerid, "LESSON") != 0)
			    return SendClientMessage(playerid, COLOR_DARKORANGE, "� ��� ��� ������� �������.");
			ShowPlayerDialog(playerid, dDriver_License, DIALOG_STYLE_LIST,
	            !"{FFCC00}��������� ��",
				"{0099FF}1. {FFFFFF}��������� {4BCC2B}A {FFFFFF}| \t{689ddb}3000 ������\t {FFFFFF}(�����������)\
				\n{0099FF}2. {FFFFFF}��������� {4BCC2B}B {FFFFFF}| \t{689ddb}4000 ������\t {FFFFFF}(�������� ����������)\
				\n{0099FF}3. {FFFFFF}��������� {4BCC2B}C {FFFFFF}| \t{689ddb}5500 ������\t {FFFFFF}(�������� ����������)\
				\n{0099FF}4. {FFFFFF}��������� {4BCC2B}D {FFFFFF}| \t{689ddb}6000 ������\t {FFFFFF}(��������)",
				"������","������"
			);
		}
	}
    if(GetPVarInt(playerid,"Job") == 2)
    {
        if(newkeys == KEY_SECONDARY_ATTACK || newkeys == KEY_JUMP || newkeys == KEY_SECONDARY_ATTACK || newkeys == KEY_FIRE || newkeys == KEY_CROUCH)
        {
            RemovePlayerAttachedObject(playerid,1);
            
            ApplyAnimation(playerid,"PED","IDLE_tired",4.1,0,1,1,0,1);
            
            SetPVarInt(playerid,"Job",1);
            SetPlayerCheckpoint(playerid, 1758.0327,2270.0376,15.8668,1.5);
            
            SendClientMessage(playerid, COLOR_ORANGE, !"�� ������� ����, ����� �� �����.");
       }
    }
	if(newkeys == KEY_FIRE || newkeys == KEY_HANDBRAKE || newkeys == KEY_CROUCH || newkeys == KEY_JUMP)
    {
        if(GetPVarInt(playerid,"Job") == 4 && GetPVarInt(playerid,"LessStatus") == 1 && IsPlayerAttachedObjectSlotUsed(playerid, 4))
        {
            new checkpoint_id = RandomEX(0,4);
            DisablePlayerCheckpoint(playerid);
	        RemovePlayerAttachedObject(playerid, 4);
	        
			ClearAnimations(playerid);
			ApplyAnimation(playerid,"PED","IDLE_tired",4.1,0,1,1,0,1);
			
			SetPVarInt(playerid,"Job",3);
		    SetPVarInt(playerid, "LessStatus", 0);
		    
			SetPlayerAttachedObject(playerid, 3, 341, 6, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000, 1.000000, 1.000000);
            SetPlayerCheckpoint(playerid,Tree[checkpoint_id][0],Tree[checkpoint_id][1],Tree[checkpoint_id][2], 2.0);
            
            SendClientMessage(playerid, COLOR_GREY, !"�� ������� ��������� ������!");
		}
    }
	new pState = GetPlayerState(playerid);
	if(pState == PLAYER_STATE_DRIVER)
	{
		new vehicleid = GetPlayerVehicleID(playerid);
		if(GetPlayerVehicleSeat(playerid) == 0)
		{
			if(newkeys & KEY_ACTION)//CTRL
			{
				if(VehData[vehicleid-1][vFuel] <= 0.0 || VehData[vehicleid-1][vHealth] <= 300.0)
					return SendClientMessage(playerid, COLOR_GREY, !"������ �� ���������");

				new engine, lights, alarm, doors, bonnet, boot, objective;
				GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);

				VehData[vehicleid-1][vEngine] = engine = (engine == 0) ? 1 : 0;
				SetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);

				PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][9], engine == 0 ? COLOR_WHITE : COLOR_ORANGE);
				UpdatePlayerVehParamsTD(playerid);
			}
			else if(newkeys & KEY_FIRE)
			{
				new engine, lights, alarm, doors, bonnet, boot, objective;
				GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);

				VehData[vehicleid-1][vLight] = lights = (lights == 0) ? 1 : 0;
				SetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);

				PlayerPlaySound(playerid, 17001, 0.0, 0.0, 0.0);

				PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][5], lights == 0 ? COLOR_WHITE : COLOR_ORANGE);
				PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][6], lights == 0 ? COLOR_WHITE : COLOR_ORANGE);
				PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][7], lights == 0 ? COLOR_WHITE : COLOR_ORANGE);
				UpdatePlayerVehParamsTD(playerid);
			}
			else if(newkeys & KEY_LOOK_LEFT)
			{
				if(VehData[vehicleid-1][vLeftIndicator] != 0)
				{

					DestroyDynamicObject(VehData[vehicleid-1][vLeftIndicator]);
					DestroyDynamicObject(VehData[vehicleid-1][vLeftIndicator]+1);

					VehData[vehicleid-1][vLeftIndicator] = 0;

					PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][0], -1);
				}
				else
				{

					if(VehData[vehicleid-1][vRightIndicator] != 0)
					{

						DestroyDynamicObject(VehData[vehicleid-1][vRightIndicator]);
						DestroyDynamicObject(VehData[vehicleid-1][vRightIndicator]+1);

						VehData[vehicleid-1][vRightIndicator] = 0;

						PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][1], -1);
					}

					VehData[vehicleid-1][vLeftIndicator] = 
						CreateDynamicObject(19294, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

					CreateDynamicObject(19294, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

					AttachDynamicObjectToVehicle(VehData[vehicleid-1][vLeftIndicator], vehicleid, -0.9, 2.5, 0.1, 0.0, 0.0, 0.0);
					AttachDynamicObjectToVehicle(VehData[vehicleid-1][vLeftIndicator]+1, vehicleid, -0.9, -2.7, 0.1, 0.0, 0.0, 0.0);

					PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][0], COLOR_ORANGE);
				}
				UpdatePlayerVehParamsTD(playerid);
			}
			else if(newkeys & KEY_LOOK_RIGHT)
			{
				if(VehData[vehicleid-1][vRightIndicator] != 0)
				{

					DestroyDynamicObject(VehData[vehicleid-1][vRightIndicator]);
					DestroyDynamicObject(VehData[vehicleid-1][vRightIndicator]+1);

					VehData[vehicleid-1][vRightIndicator] = 0;

					PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][1], -1);
				}
				else
				{
					if(VehData[vehicleid-1][vLeftIndicator] != 0)
					{

						DestroyDynamicObject(VehData[vehicleid-1][vLeftIndicator]);
						DestroyDynamicObject(VehData[vehicleid-1][vLeftIndicator]+1);

						VehData[vehicleid-1][vLeftIndicator] = 0;

						PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][0], -1);
					}

					VehData[vehicleid-1][vRightIndicator] = 
						CreateDynamicObject(19294, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

					CreateDynamicObject(19294, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

					AttachDynamicObjectToVehicle(VehData[vehicleid-1][vRightIndicator], vehicleid, 0.9, 2.5, 0.1, 0.0, 0.0, 0.0);
					AttachDynamicObjectToVehicle(VehData[vehicleid-1][vRightIndicator]+1, vehicleid, 0.9, -2.7, 0.1, 0.0, 0.0, 0.0);

					PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][1], COLOR_ORANGE);
				}
				UpdatePlayerVehParamsTD(playerid);
			}

		}
	}
	return 1;
}

public OnRconLoginAttempt(ip[], password[], success)
{
	return 1;
}

public OnPlayerUpdate(playerid)
{
	if(PlayerAFK[playerid] > -2)
	{
	    if(PlayerAFK[playerid] > 2) SetPlayerChatBubble(playerid, "", -1, 25.0, 200);
	    PlayerAFK[playerid] = 0;
	}
	if(Player_Gps_Status[playerid])
	{
		if(IsPlayerInRangeOfPoint(playerid, 30.0, player_gps_pos[playerid][0], player_gps_pos[playerid][1], player_gps_pos[playerid][2]))
		{
			DisableGPS(playerid);
		}
	}
	return 1;
}

public OnPlayerStreamIn(playerid, forplayerid)
{
	if(!(pData[forplayerid][pSettings] & pShowNames)) 
		ShowPlayerNameTagForPlayer(forplayerid,playerid,0);
	return 1;
}

public OnPlayerStreamOut(playerid, forplayerid)
{
	return 1;
}

public OnVehicleStreamIn(vehicleid, forplayerid)
{
	return 1;
}

public OnVehicleStreamOut(vehicleid, forplayerid)
{
	return 1;
}
public OnPlayerClickTextDraw(playerid, Text:clickedid)
{
	if(clickedid == Text:INVALID_TEXT_DRAW)
	{
		if(GetPVarInt(playerid, "RegChooseSkin") != 0)
		{
			SelectTextDraw(playerid, 0xFFFFFFFF);
			return SendClientMessage(playerid, COLOR_GREY, !"� ��� ������� TextDraw'� ������ �����");
		}
	}
	else if(clickedid == RegSkinTD[4] || clickedid == RegSkinTD[5])//Left Arrow(Prev) || Right Arrow(Next)
	{
		new skin = GetPVarInt(playerid, "RegChooseSkin");
		switch(pData[playerid][pSex])
		{
			case 1:	
			{
				if(clickedid == RegSkinTD[4])
				{
					if(skin == 1) skin = MALE_SKINS_COUNT; else skin--;
				}
				else
				{
					if(skin == MALE_SKINS_COUNT) skin = 1; else skin++;
				}
			}
			case 2:	
			{
				if(clickedid == RegSkinTD[4])
				{
					if(skin == 1) skin = FEMALE_SKINS_COUNT; else skin--;
				}
				else
				{
					if(skin == FEMALE_SKINS_COUNT) skin = 1; else skin++;
				}
			}
		}
		HideRegSkinChooseTD(playerid);
		pData[playerid][pSkin] = RegSkins[pData[playerid][pSex]-1][skin-1];
		SetPlayerSkin(playerid, pData[playerid][pSkin]);
		TextDrawShowForPlayer(playerid, RegSkinNumbTD[pData[playerid][pSex]-1][skin-1]);
		SetPVarInt(playerid, "RegChooseSkin",  skin);
	}
	else if(clickedid == RegSkinTD[7])//Key Select
	{
		SetPVarInt(playerid, "RegChooseSkin", 0);
		CancelSelectTextDraw(playerid);
		HidePlayerChangeRegSkin(playerid);
		SpawnPlayer(playerid);
		UpdatePlayerData(playerid, "skin", pData[playerid][pSkin]);
	}
	return 1;
}
public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	switch(dialogid)
	{
		case dNull: return 1;
		case dAuth:
		{
			if(!response) return Kick(playerid);
			if(!(3 <= strlen(inputtext) <= 16)) return ShowPlayerLogin(playerid);
			KillTimer(pLoginTimer[playerid]);
			new pass[32+1];
			md5(pass, inputtext);
			static const fmt_query[] = "SELECT `googleauth` FROM `accounts` WHERE `name` = '%s' AND `password` = '%e' LIMIT 1";
			new query[sizeof(fmt_query) + (-2+MAX_PLAYER_NAME) + (-2+sizeof pass)];
			mysql_format(dbHandle, query, sizeof query, fmt_query, pData[playerid][pName], pass);
			mysql_tquery(dbHandle, query, "@__PlayerAuth", "i", playerid);
		}
		case dRegistration:
		{
			if(!response) return Kick(playerid);
			if(!(3 <= strlen(inputtext) <= PASSWORD_MAX_LENGTH)) return ShowPlayerRegister(playerid);
			new pass[PASSWORD_MAX_LENGTH+1];
			mysql_escape_string(inputtext, pass);
			SetPVarString(playerid, "RegistrPass", pass);
			ShowPlayerEnterEmail(playerid);
		}
		case dRegistration2:
		{
			if(!response) return strmid(pData[playerid][pEmail], "none", 0, strlen("none"), 32+1), ShowPlayerEnterReferal(playerid);
			if(!(6 <= strlen(inputtext) <= 32)) return ShowPlayerEnterEmail(playerid);
			if(strfind(inputtext, "@", true) == -1 || strfind(inputtext, ".", true) == -1)
			{
				PlayerPlaySound(playerid, 1055, 0.0, 0.0, 0.0);
			    SendClientMessage(playerid, COLOR_GREY, !"�� ����� �������� ����� email");
			    return ShowPlayerEnterEmail(playerid);
			}
			static const fmt_query[] = "SELECT `id` FROM `accounts` WHERE `email` = '%s' LIMIT 1";
			new email[32];
			mysql_escape_string(inputtext, email); 
			new query[sizeof fmt_query + (-2+32)];
			mysql_format(dbHandle, query, sizeof query, fmt_query, email);
			mysql_tquery(dbHandle, query, "@__CheckEmail", "is", playerid, inputtext);
		}
		case dRegistration3:
		{
			if(response)
			{
				pData[playerid][pSex] = 1;
			}
			else
			{	
				pData[playerid][pSex] = 2;
			}
			ShowPlayerRules(playerid);
		}
		case dRegistration4:
		{
			if(!response) return Kick(playerid);
			SetPVarInt(playerid, "RegistrationSpawn", 1);
			SendClientMessage(playerid, COLOR_GREEN, "");
			SendClientMessage(playerid, COLOR_GREEN, "");
			SendClientMessage(playerid, COLOR_GREEN, "");
			SendClientMessage(playerid, COLOR_GREEN, !"����������� ���������");
			SendClientMessage(playerid, COLOR_GREY, !"����������� {25BA2A}�������{AFAFAF} � ������ {49BCE6}�������{AFAFAF} ��� ������");
			ShowPlayerChangeRegSkin(playerid);
			pData[playerid][pSkin] = RegSkins[pData[playerid][pSex]-1][0];

			pLogged[playerid] = true;
			//----------------------------------[Create Account DataBase]----------------------------------
			new ip[16],
				regdata[12],
				password[32+1];
			GetPlayerIp(playerid, ip, sizeof ip);
			GetPVarString(playerid, "RegistrPass", password, sizeof password);
			md5(password, password);
			format(regdata, sizeof regdata, "%s", date("%dd.%mm.%yyyy",  gettime()));
			static const fmt_query[] = "INSERT INTO `accounts` (`name`, `password`, `email`, `regip`, `regdata`, `sex`, `skin`, `referalid`) \
			 VALUES ('%s', '%s', '%s', '%s', '%s', '%d', '%d', '%d')";
			new query[sizeof fmt_query + (-2+MAX_PLAYER_NAME) +(-2+PASSWORD_MAX_LENGTH) +(-2+32)+(-2+16)+(-2+10)+(-2+3)+(-2+3)+(-2+9)]; 
			mysql_format(dbHandle, query, sizeof query, fmt_query, 
				pData[playerid][pName], password, pData[playerid][pEmail], ip, regdata, pData[playerid][pSex], pData[playerid][pSkin], pData[playerid][pReferalid] 
			);
			//mysql_tquery(dbHandle, query,  "@__PlayerInsertID", "i", playerid);
			mysql_tquery(dbHandle, query,  "", "");

			static const fmt_query2[] = "SELECT * FROM `accounts` WHERE `name` = '%s' LIMIT 1";
			mysql_format(dbHandle, query, sizeof query, fmt_query2, pData[playerid][pName]);
			mysql_tquery(dbHandle, query, "@__LoadPlayerData", "i", playerid);
			//----------------------------------[/Create Account DataBase]----------------------------------
			TogglePlayerSpectating(playerid, false);
		}
		case dRegistration5:
		{
			if(!response) return ShowPlayerChooseSex(playerid);
			if(!(3 <= strlen(inputtext) <= MAX_PLAYER_NAME)) 
				return SendClientMessage(playerid, COLOR_GREY, !"����� ���� 3-24 ��������"),
					ShowPlayerEnterReferal(playerid);

			if(!strcmp(pData[playerid][pName], inputtext, false))	
				return ShowPlayerDialog(
					playerid, dRegistration6, DIALOG_STYLE_MSGBOX, 
					!"{FFCC00}������", 
					!"{FFFFFF}������ ��������� ����������� ���.\n���� �� �� ������ ������ ������� ������� ������ \"����������\"", 
					!"������", !"����������"
				);

			static const fmt_query[] = "SELECT `id` FROM `accounts` WHERE `name` = '%e' LIMIT 1";
			new query[sizeof fmt_query + (-2+MAX_PLAYER_NAME)];
			mysql_format(dbHandle, query, sizeof query, fmt_query, inputtext);
			mysql_tquery(dbHandle, query, "@__PlayerEnterReferal", "is", playerid, inputtext);
		}
		case dRegistration6:
		{
			if(!response) ShowPlayerChooseSex(playerid);
			else ShowPlayerEnterReferal(playerid);
			return 1;
		}
		case dMenu:
		{
			if(!response) return 1;
			switch(listitem)
			{
				case 0: 
				{
					ShowPlayerStats(playerid, playerid);
				}
				case 1: ShowPlayerVisualSettings(playerid);
				case 2: return callcmd::menu(playerid);
				case 3:
				{
					return ShowPlayerPrivateSettings(playerid);
				}
				case 4: 
				{
					if(GetPVarInt(playerid, "last_report") > gettime()) 
				    	return SendClientMessage(playerid, COLOR_GREY, !"������ � ������ ����� ��� � ������");

				    ShowPlayerReportDlg(playerid);
					return 1;
				}
				case 5: return callcmd::menu(playerid);
				case 6: return callcmd::gps(playerid);
			}
		}
		case dMenuVisual:
		{
			if(!response) return callcmd::menu(playerid);
			new settings = 1 << listitem;
			if(!(pData[playerid][pSettings] & settings)) pData[playerid][pSettings] |= settings;
			else pData[playerid][pSettings] ^= settings;
			UpdatePlayerData(playerid, "settings", pData[playerid][pSettings]);
			if(settings == pShowNames)
			{
				if(pData[playerid][pSettings] & pShowNames)
					for(new i; i < MAX_PLAYERS; i++) ShowPlayerNameTagForPlayer(playerid, i, 1);
				else
					for(new i; i < MAX_PLAYERS; i++) ShowPlayerNameTagForPlayer(playerid, i, 0);
			}

			ShowPlayerVisualSettings(playerid);
		}
		case dMenuReport:
		{
			if(!response) return 1;
		    if(!(1 <= strlen(inputtext) <= 80)) 
		    	return ShowPlayerReportDlg(playerid);
		    new string[144];
		    format(string, sizeof(string), "{DB5622}%s[%d]: {ffd400}%s", pData[playerid][pName], playerid, inputtext);
			SendAdminMes(0xDB5622FF, string);

			format(string, sizeof(string), "{35aa28}%s[%d]: {ffd400}%s", pData[playerid][pName], playerid, inputtext);
		    SendClientMessage(playerid, 0x35AA28FF, string);
			SendClientMessage(playerid, 0x35AA28FF, !"��������, � ��������� ����� ��� ������� �������������");

			SetPVarInt(playerid, "last_report", gettime()+60);
			return 1;
		}
		case dMenuPrivateSettings:
		{
			if(!response) return callcmd::menu(playerid);
			switch(listitem)
			{
				case 0:
				{
					ShowPlayerChangeName(playerid);
				}
				case 1:
				{
					SetPVarInt(playerid, "PlayerChangePass", 1);
					ShowPlayerChangePass(playerid);
				}
				case 4:
				{
					new type,string[32];
					if(!strcmp(pData[playerid][pGoogleAuth],"-1", true))
					{
					    string = !"1. ���������� ������";
					    type = 0;
					}
					else
					{
					    string = !"1. ������� ������";
					    type = 1;
					}
					SetPVarInt(playerid, "pGoogleAuth", type);
		            return ShowPlayerDialog(playerid, dGoogleAuth, DIALOG_STYLE_LIST, "{FFCC00}Google Auth", string, "�����", "������");
				}
			}
		}
		case dGps:
		{
			if(!response) return callcmd::menu(playerid);
			switch(listitem)
			{
				case 0:
				{
					ShowPlayerDialog(playerid, dGps1, DIALOG_STYLE_LIST, !"{FFCC00}������������ �����", !"{0099FF}1. {FFFFFF}������������� ������ ���. ��������\n{0099FF}2. {FFFFFF}��������� ������", !"�����", !"�����");
				}
				case 1:
				{
                    ShowPlayerDialog(playerid, dGps2, DIALOG_STYLE_LIST, !"{FFCC00}��������������� �����������", !"{0099FF}1. {FFFFFF}���", !"�����", !"�����");
				}
				case 2:
				{
					ShowPlayerDialog(playerid, dGps3, DIALOG_STYLE_LIST, !"{FFCC00}����������� �����������", !"{0099FF}1. {FFFFFF}��� �����������", !"�����", !"�����");
				}
				case 3:
				{
					ShowPlayerDialog(playerid, dGps4, DIALOG_STYLE_LIST, !"{FFCC00}����������", !"{0099FF}1. {FFFFFF}�����\n{0099FF}2. {FFFFFF}�������\n{0099FF}3. {FFFFFF}���������\n", !"�����", !"�����");
				}
			}
		}
		case dGps1:
		{
			if(!response) return callcmd::gps(playerid);
			static const
				gps_pos_public_places[][E_GLOBAL_MAP_ICONS] =
			{
				{1818.2972,2095.7390,16.1631, 56}, // 1. GPS ������������� ������� ��������
				{1912.2401,2227.5493,16.0831, 55} //2. GPS ���������
			};

			if(!(0 <= listitem <= sizeof gps_pos_public_places - 1)) return 1;
			EnableGPS(playerid, gps_pos_public_places[listitem][map_icon_x], gps_pos_public_places[listitem][map_icon_y], gps_pos_public_places[listitem][map_icon_z], gps_pos_public_places[listitem][map_icon_type], "����� �������� � ��� �� GPS");
			
			return 1;
		}
		case dGps2:
		{
			if(!response) return callcmd::gps(playerid);
			static const
				gps_pos_public_places[][E_GLOBAL_MAP_ICONS] =
			{
				{1919.2804,2183.4036,15.6982, 30} // 1. GPS ���
			};

			if(!(0 <= listitem <= sizeof gps_pos_public_places - 1)) return 1;
			EnableGPS(playerid, gps_pos_public_places[listitem][map_icon_x], gps_pos_public_places[listitem][map_icon_y], gps_pos_public_places[listitem][map_icon_z], gps_pos_public_places[listitem][map_icon_type], "����� �������� � ��� �� GPS");

			return 1;
		}
		case dGps3:
		{
			if(!response) return callcmd::gps(playerid);
			static const
				gps_pos_public_places[][E_GLOBAL_MAP_ICONS] =
			{
				{2319.8442,1759.8313,0.9777, 23} // 1. GPS ��� �����������
			};

			if(!(0 <= listitem <= sizeof gps_pos_public_places - 1)) return 1;
			EnableGPS(playerid, gps_pos_public_places[listitem][map_icon_x], gps_pos_public_places[listitem][map_icon_y], gps_pos_public_places[listitem][map_icon_z], gps_pos_public_places[listitem][map_icon_type], "����� �������� � ��� �� GPS");

			return 1;
		}
		case dGps4:
		{
			if(!response) return callcmd::gps(playerid);
			static const
				gps_pos_public_places[][E_GLOBAL_MAP_ICONS] =
			{
				{2613.8672,1765.1648,2.5606, 56}, // 1. GPS �����
				{1754.7878,2251.7822,15.8603, 11}, // 1. GPS ��������
				{1967.0968,1612.2507,16.3733, 7} // 1. GPS ���������
			};

			if(!(0 <= listitem <= sizeof gps_pos_public_places - 1)) return 1;
			EnableGPS(playerid, gps_pos_public_places[listitem][map_icon_x], gps_pos_public_places[listitem][map_icon_y], gps_pos_public_places[listitem][map_icon_z], gps_pos_public_places[listitem][map_icon_type], "����� �������� � ��� �� GPS");

			return 1;
		}
		case dChangeName:
		{
			if(!response) return ShowPlayerPrivateSettings(playerid);
			if(!(6 <= strlen(inputtext) <= 24)) return ShowPlayerChangeName(playerid);
			if(CheckPlayerChangeName(inputtext) == 0) return SendClientMessage(playerid, COLOR_GREY,  !"��� �������� ����������� �������");
			static const fmt_query[] = "SELECT `id` FROM `accounts` WHERE `name` = '%e' LIMIT 1";
			new query[sizeof fmt_query + (-2+MAX_PLAYER_NAME)];
			mysql_format(dbHandle, query,  sizeof query, fmt_query, inputtext);
			mysql_tquery(dbHandle, query, "@__CheckPlayerChangeName", "is", playerid, inputtext);
		}
		case dChangePassword:
		{
			if(!response) 
		    {
		    	DeletePVar(playerid, "PlayerChangePass");
		    	DeletePVar(playerid, "PlayerChangePass_Pass");
		    	return 1;
		    }
		    new pass[32+1];
		    switch(GetPVarInt(playerid, "PlayerChangePass"))
		    {
				case 1:
				{
				    if(!(5 <= strlen(inputtext) <= PASSWORD_MAX_LENGTH)) 
				    	return SendClientMessage(playerid,COLOR_GREY, !"����� ������ 5-16 ��������"), 
								ShowPlayerChangePass(playerid);

				    md5(pass, inputtext);
					static const fmt_query[] = "SELECT `id` FROM `accounts` WHERE `id` = '%d' AND `password` = '%e'";
					new query[sizeof fmt_query + (-2+9) + (-2+sizeof pass)];

					mysql_format(dbHandle, query, sizeof query, fmt_query, pData[playerid][pID], pass);
					mysql_tquery(dbHandle, query, "@__PlayerChangePass", "i", playerid);

				}
				case 2:
				{
					if(!(5 <= strlen(inputtext) <= PASSWORD_MAX_LENGTH)) 
				    	return SendClientMessage(playerid,COLOR_GREY, !"����� ������ 5-16 ��������"), 
								ShowPlayerChangePass(playerid);

					if(CheckPassword(inputtext, strlen(inputtext)) == 0 )
					{
						DeletePVar(playerid, "PlayerChangePass");
	    				DeletePVar(playerid, "PlayerChangePass_Pass");
						return SendClientMessage(playerid, COLOR_GREY, !"�� ����� ����������� �������");
					}

				    SetPVarString(playerid, "PlayerChangePass_Pass", inputtext);
				    SetPVarInt(playerid,"PlayerChangePass", 3);
				    ShowPlayerChangePass(playerid);
				}
				case 3:
				{
				    if(!(5 <= strlen(inputtext) <= PASSWORD_MAX_LENGTH)) 
				    	return SendClientMessage(playerid,COLOR_GREY, !"����� ������ 5-16 ��������"), 
								ShowPlayerChangePass(playerid);

					if(CheckPassword(inputtext, strlen(inputtext)) == 0 )
					{
						DeletePVar(playerid, "PlayerChangePass");
	    				DeletePVar(playerid, "PlayerChangePass_Pass");
						return SendClientMessage(playerid, COLOR_GREY, !"�� ����� ����������� �������");
					}
								 
					GetPVarString(playerid, "PlayerChangePass_Pass", pass, sizeof pass);
				    if(strcmp(inputtext, pass, false))
					{
                        DeletePVar(playerid, "PlayerChangePass");
		    			DeletePVar(playerid, "PlayerChangePass_Pass");
		    			return SendClientMessage(playerid,COLOR_GREY, !"������ �� �������������");
					}

					md5(pass, inputtext);
					static const fmt_query[] = "UPDATE `accounts` SET `password` = '%e' WHERE `id` = '%d'";
					new query[sizeof fmt_query + (-2+sizeof pass) + (-2+9)];
					mysql_format(dbHandle, query, sizeof query, fmt_query, pass, pData[playerid][pID]);
					mysql_tquery(dbHandle, query, "", "");

					SendClientMessage(playerid, COLOR_ORANGE, !"�� �������� ���� ������. �������� ��� ����-������");
					DeletePVar(playerid, "PlayerChangePass");
			    	DeletePVar(playerid, "PlayerChangePass_Pass");
				}
		    }
		    return 1;
		}
		case dGoogleAuth:
  		{
		    if(!response) return DeletePVar(playerid, "pGoogleAuth");
		    new type = GetPVarInt(playerid, "pGoogleAuth");
		    switch(type)
		    {
		        case 0:
		        {
		            new pass[17];
					for(new i = 0; i < sizeof(pass); i ++) strcat(pass, GoogleCode[random(sizeof(GoogleCode))]);
					strmid(pData[playerid][pGoogleAuth], pass, 0, strlen(pass));
					static const fmt_str[] = !"{FFFFFF}������� ���� ��� � ���������� �� ��������\n� ����� ������� � ���� ���������� �� ���������� ���\n\n{FFAA00}%s";
					new string[sizeof(fmt_str)+(-2+sizeof(pass))];
					format(string, sizeof(string), fmt_str, pass);
					return ShowPlayerDialog(playerid, dGoogleAuth2, DIALOG_STYLE_INPUT, !"{FFCC00}Google Auth", string, !"������", !"������");
		        }
		        case 1:
		        {
		            ShowPlayerDialog(playerid, dGoogleAuth3, DIALOG_STYLE_INPUT, !"{FFCC00}Google Auth", !"{FFFFFF}������� ��� �� ���������� �� �������� ��� ������������� ������ ������:", !"������", !"������");
		        }
		    }
		    return 1;
		}
		case dGoogleAuth2:
		{
		    if(!response) return DeletePVar(playerid, "pGoogleAuth"),strmid(pData[playerid][pGoogleAuth],"-1",0,strlen("-1"));
		    
		    new code;
		    if(sscanf(inputtext, "d", code) || (code != GoogleAuthenticatorCode(pData[playerid][pGoogleAuth], gettime_default())))
				return ShowPlayerDialog(playerid, dGoogleAuth2, DIALOG_STYLE_INPUT, !"{FFCC00}Google Auth", !"{FFFFFF}��� �� ���������.\n��������� � ������� ��� ���\n\
				*�������� ����� ��������� ���������� ����:", !"������", !"������");
					
		    SendClientMessage(playerid, COLOR_GREEN, !"�� ������� ���������� Google ������.");
		    new query[60+(-2+16)+(-2+11)];
			format(query,sizeof(query),"UPDATE `accounts` SET `googleauth` = '%s' WHERE `id` = '%d'", pData[playerid][pGoogleAuth], pData[playerid][pID]);
			mysql_tquery(dbHandle,query, "", "");
		}
		case dGoogleAuth3:
		{
		    if(!response) return 1;

		    new code;
		    if(sscanf(inputtext, "d", code) || (code != GoogleAuthenticatorCode(pData[playerid][pGoogleAuth], gettime_default())))
				return ShowPlayerDialog(playerid, dGoogleAuth3, DIALOG_STYLE_INPUT, !"{FFCC00}Google Auth", !"{FFFFFF}��� �� ���������.\n ��������� � ������� ��� ���\n\
				*�������� ����� ��������� ���������� ����:", !"������", !"������");

		    SendClientMessage(playerid, COLOR_RED, !"�� ������� Google ������.");
		    strmid(pData[playerid][pGoogleAuth],"-1",0,strlen("-1"));
		    new query[60+(-2+11)];
			format(query,sizeof(query),"UPDATE `accounts` SET `googleauth` = '-1' WHERE `id` = '%d'", pData[playerid][pID]);
			mysql_tquery(dbHandle,query, "", "");
		}
		case dGoogleAuth4:
		{
		    if(!response) return Kick(playerid);
		    
		    new code;
		    if(sscanf(inputtext, "d", code))
				return ShowPlayerDialog(playerid, dGoogleAuth4, DIALOG_STYLE_INPUT, !"{FFCC00}Google Auth", !"�� ����� �������� ����������� Google Auth ������\n\
				������� ��� �� ���������� \"Google Auth\" �� ����� �������� ��� �����������:", !"����", !"");
				
			if(code != GoogleAuthenticatorCode(pData[playerid][pGoogleAuth], gettime_default()))
			    return SendClientMessage(playerid, COLOR_RED, !"�� ����� �������� ���"), Kick(playerid);
			    
			static const fmt_query[] = "SELECT * FROM `accounts` WHERE `name` = '%s' LIMIT 1";
			new query[sizeof fmt_query + (-2+MAX_PLAYER_NAME)];
			mysql_format(dbHandle, query, sizeof query, fmt_query, pData[playerid][pName]);
			mysql_tquery(dbHandle, query, "@__LoadPlayerData", "i", playerid);
		}
		case dJobLoader:
		{
		    if(!response) return 1;
		    new str[14];
		    
            SendClientMessage(playerid,COLOR_GREEN,"�� ������ ������ ��������, ����� �� ������� �������� � �������� ����.");
            
            SetPVarInt(playerid,"PSkin",GetPlayerSkin(playerid));
            
            SetPlayerSkin(playerid,254);
            
            SetPVarInt(playerid,"InJob",1);
            
            SetPVarInt(playerid,"Job",1);
            
           	for(new idx = 0; idx < JobLoaderTDSize; idx++)
		        PlayerTextDrawShow(playerid, JobLoaderTD[idx][playerid]);
            
			format(
				str,14,"%d", GetPVarInt(playerid,"JobY")
			);
            PlayerTextDrawSetString(playerid, JobLoaderTD[7][playerid], str);
            
			format(
				str,14,"%d",
				GetPVarInt(playerid,"JobY")*JOB_LOADER_SALARY
			);
            PlayerTextDrawSetString(playerid, JobLoaderTD[8][playerid], str);
            
            SetPlayerCheckpoint(playerid, 1758.0327,2270.0376,15.8668,1.5);
		}
		case dJobLoader1:
		{
		    if(!response) return 1;
            new str[128];
            
            
            format(
				str,sizeof(str),
				"�������� �� ������: {0099FF}%d {FFFFFF}������",
				GetPVarInt(playerid,"JobY")*JOB_LOADER_SALARY
			);
			SendClientMessage(playerid,COLOR_GREEN,str);
			
            format(
				str,sizeof(str),
				"����� ���������� ������: {FFCC00}%d",
				GetPVarInt(playerid,"JobY")
			);
			SendClientMessage(playerid,0xFFFFFFAA,str);
            
            DisablePlayerCheckpoint(playerid);
            
            ApplyAnimation(playerid,"PED","IDLE_tired",4.1,0,1,1,0,1);
            
            RemovePlayerAttachedObject(playerid,1);
            
           	pData[playerid][pCash] += GetPVarInt(playerid,"JobY")*JOB_LOADER_SALARY;
	        SetPlayerMoney(playerid, pData[playerid][pCash]);
            
            SetPlayerSkin(playerid,GetPVarInt(playerid,"PSkin"));
            
            DeletePVar(playerid,"Job");
            DeletePVar(playerid,"InJob");
            DeletePVar(playerid,"JobY");
            DeletePVar(playerid,"PSkin");
            
           	for(new idx = 0; idx < JobLoaderTDSize; idx++)
		        PlayerTextDrawHide(playerid, JobLoaderTD[idx][playerid]);
            
            SendClientMessage(playerid, COLOR_LIGHTBLUE, !"������� ���� ��������!");
		}
		case dJobLumberJack:
		{
		    if(!response) return 1;
		    new checkpoint_id = RandomEX(0,4);

            SendClientMessage(playerid,COLOR_GREEN, !"�� ������ ������ ��������, ����� �� ������� �������� � ��������� ������ ������.");
            SetPVarInt(playerid,"PSkin",GetPlayerSkin(playerid));
            SetPVarInt(playerid,"InJob",2);
            SetPVarInt(playerid,"Job",3);

            SetPlayerSkin(playerid,260);
            SetPlayerAttachedObject(playerid, 3, 341, 6, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000, 1.000000, 1.000000);
            SetPlayerCheckpoint(playerid,Tree[checkpoint_id][0],Tree[checkpoint_id][1],Tree[checkpoint_id][2], 2.0);

		}
		case dJobLumberJack1:
		{
		    if(!response) return 1;
            new str[128];


            format(
				str,sizeof(str),
				"�������� �� ������: {0099FF}%d {FFFFFF}������",
				GetPVarInt(playerid,"JobY")*JOB_LJ_SALARY
			);
			SendClientMessage(playerid,COLOR_GREEN, str);

            format(
				str,sizeof(str),
				"����� ���������� ���� �� �����: {FFCC00}%d",
				GetPVarInt(playerid,"JobY")
			);
			SendClientMessage(playerid,0xFFFFFFAA, str);

            DisablePlayerCheckpoint(playerid);
            ApplyAnimation(playerid,"PED","IDLE_tired",4.1,0,1,1,0,1);
            RemovePlayerAttachedObject(playerid,1);

           	pData[playerid][pCash] += GetPVarInt(playerid,"JobY")*JOB_LJ_SALARY;
	        SetPlayerMoney(playerid, pData[playerid][pCash]);
            SetPlayerSkin(playerid,GetPVarInt(playerid,"PSkin"));

            DeletePVar(playerid,"Job");
            DeletePVar(playerid,"InJob");
            DeletePVar(playerid,"JobY");
            DeletePVar(playerid,"PSkin");
			DeletePVar(playerid, "LessStatus");
			
			if(IsPlayerAttachedObjectSlotUsed(playerid, 3)) RemovePlayerAttachedObject(playerid, 3);
			if(IsPlayerAttachedObjectSlotUsed(playerid, 4)) RemovePlayerAttachedObject(playerid, 4);

            SendClientMessage(playerid, COLOR_LIGHTBLUE, !"������� ���� ��������!");
		}
		case dDriver_License:
		{
			if(!response) return 1;

			static const Price[] = { 3000, 4000, 5500, 6000 };
			static const categories[] = { "A", "B", "C", "D" };
			static const categories_name[] = {
				"�����������",
				"�������� ����������",
				"�������� ����������",
				"��������"
			};
			static const fmt_str[] = "�� ������ ������� �� ����� ��������� %s";
			static const fmt_str2[] = "������� �� ���� � ������ � ���������, �������������� ��� ���� ���������. (%s)";

			if(pData[playerid][pCarLic][listitem] == 1)
					    return SendClientMessage(playerid, COLOR_GREEN, !"� ��� ��� ���� ����� ���� ���������.");

			if(pData[playerid][pCash] < Price[listitem])
					    return SendClientMessage(playerid, COLOR_DARKORANGE, !"� ��� ������������ ������� ��� ������ ��������.");

			SetPVarInt(playerid, "Category", listitem+1);	
			SetPlayerMoney(playerid, pData[playerid][pCash] - Price[listitem]);
			
			new string[sizeof fmt_str + (-2 + 1)],
				string2[sizeof fmt_str2 + (-2 + 19)];//19 = ����� ������� �������� � ������� categories_name

			format(
				string, sizeof string,
				fmt_str,
				categories[listitem]
			);	

			format(
				string2, sizeof string2,
				fmt_str2,
				categories_name[listitem]
			);

			SendClientMessage(playerid, COLOR_BLUE, string);
			SendClientMessage(playerid, COLOR_BLUE, string2);
		}
	}
	return 1;
}

public OnPlayerClickPlayer(playerid, clickedplayerid, source)
{
	return 1;
}

public OnPlayerCommandReceived(playerid, cmd[], params[], flags)
{
    if(!pLogged[playerid]) return 1;

    if(GetTickCount() - pLastAct[playerid] < 400) return 0;
    pLastAct[playerid] = GetTickCount();

    if((ADMIN_FIRST <= flags <= ADMIN_SIXTH) && pData[playerid][pAdmin] < flags) return 0;
	return 1;
}

public OnPlayerLeaveDynamicArea(playerid, areaid)
{
    if(Job_Area[0] <= areaid)
    {
	    if(GetPVarInt(playerid,"InJob") == 1)// ������ ��������
	    {
            new str[128];

            format(
				str,sizeof(str),
				"�������� �� ������: {0099FF}%d {FFFFFF}������",
				GetPVarInt(playerid,"JobY")*JOB_LOADER_SALARY
			);
			SendClientMessage(playerid,COLOR_GREEN,str);

            format(
				str,sizeof(str),
				"����� ���������� ������: {FFCC00}%d",
				GetPVarInt(playerid,"JobY")
			);
			SendClientMessage(playerid,0xFFFFFFAA,str);// ������� �����

            DisablePlayerCheckpoint(playerid);// ������� ��������
            ApplyAnimation(playerid,"PED","IDLE_tired",4.1,0,1,1,0,1);// �������� ��������
            RemovePlayerAttachedObject(playerid,1);// ������� ������ �� ���

           	pData[playerid][pCash] += GetPVarInt(playerid,"JobY")*JOB_LOADER_SALARY;
	        SetPlayerMoney(playerid, pData[playerid][pCash]);
            SetPlayerSkin(playerid,GetPVarInt(playerid,"PSkin"));

            DeletePVar(playerid,"Job");
            DeletePVar(playerid,"InJob");
            DeletePVar(playerid,"JobY");
            DeletePVar(playerid,"PSkin");

           	for(new idx = 0; idx < JobLoaderTDSize; idx++)
		        PlayerTextDrawHide(playerid, JobLoaderTD[idx][playerid]);

            SendClientMessage(playerid, COLOR_ORANGE, !"������� ���� ��������, �� �������� ����� ������!");
	    }
    }
    if(Job_Area[1] <= areaid)
    {
	    if(GetPVarInt(playerid,"InJob") == 2)// ������ ��������
	    {
            new str[128];

            format(
				str,sizeof(str),
				"�������� �� ������: {0099FF}%d {FFFFFF}������",
				GetPVarInt(playerid, "JobY")*JOB_LJ_SALARY
			);
			SendClientMessage(playerid,COLOR_GREEN,str);

            format(
				str,sizeof(str),
				"����� ���������� ���� �� �����: {FFCC00}%d",
				GetPVarInt(playerid, "JobY")
			);
			SendClientMessage(playerid,0xFFFFFFAA,str);

            DisablePlayerCheckpoint(playerid);
            ApplyAnimation(playerid,"PED","IDLE_tired",4.1,0,1,1,0,1);
            RemovePlayerAttachedObject(playerid,1);

           	pData[playerid][pCash] += GetPVarInt(playerid,"JobY")*JOB_LJ_SALARY;
	        SetPlayerMoney(playerid, pData[playerid][pCash]);

            SetPlayerSkin(playerid,GetPVarInt(playerid,"PSkin"));
            DeletePVar(playerid,"Job");
            DeletePVar(playerid,"InJob");
            DeletePVar(playerid,"JobY");
            DeletePVar(playerid,"PSkin");
			DeletePVar(playerid, "LessStatus");

			if(IsPlayerAttachedObjectSlotUsed(playerid, 3)) RemovePlayerAttachedObject(playerid, 3);
			if(IsPlayerAttachedObjectSlotUsed(playerid, 4)) RemovePlayerAttachedObject(playerid, 4);

            SendClientMessage(playerid, COLOR_LIGHTBLUE, !"������� ���� ��������, �� �������� ����� ������!");
	    }
    }
    return 1;
}
//====================================[Stocks]==================================
PreloadAnimLib(playerid, animlib[])
{
    ApplyAnimation(playerid,animlib,"null",0.0,0,0,0,0,0);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
stock upd_SetPlayerHealth(playerid, Float:hp)
{
	if(!pLogged[playerid]) return 1;

	pData[playerid][pHealth] = hp;

	SetPlayerHealth(playerid, hp);

	static const fmt_query[] = "UPDATE `accounts` SET `health` = '%.0f' WHERE `id` = '%d'";
	new query[sizeof fmt_query + (-4+3) + (-2+11)];

	mysql_format(
		dbHandle, 
		query, sizeof query, 
		fmt_query, 
		hp, pData[playerid][pID]
	);
	return mysql_tquery(dbHandle, query, "", "");
}
#if defined _ALS_SetPlayerHealth
    #undef SetPlayerHealth    
#else
    #define    _ALS_SetPlayerHealth
#endif
#define SetPlayerHealth upd_SetPlayerHealth
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
stock upd_SetPlayerArmour(playerid, Float:arm)
{
	if(!pLogged[playerid]) return 1;

	pData[playerid][pArmour] = arm;

	SetPlayerArmour(playerid, arm);

	static const fmt_query[] = "UPDATE `accounts` SET `armour` = '%.0f' WHERE `id` = '%d'";
	new query[sizeof fmt_query + (-4+3) + (-2+11)];

	mysql_format(
		dbHandle, 
		query, sizeof query, 
		fmt_query, 
		arm, pData[playerid][pID]
	);
	return mysql_tquery(dbHandle, query, "", "");
}
#if defined _ALS_SetPlayerArmour
    #undef SetPlayerArmour    
#else
    #define    _ALS_SetPlayerArmour
#endif
#define SetPlayerArmour upd_SetPlayerArmour
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
stock FreezePlayer(const playerid, const time)
{
	TogglePlayerControllable(playerid, false);
	SetTimerEx("@__UnfreezePlayer", time, false, "i", playerid);
	return SendClientMessage(playerid, COLOR_ORANGE, !"�� ���� ���������� ��� ���������� ��������� �������. ��������");
}
stock SendAdminActionToAll(color, string[])
{
	foreach(new i:Player)
	{
		if(pData[i][pSettings] & pShowAdmActions) SendClientMessage(i, color, string);
	}
	return 1;
}
stock ShowPlayerStats(playerid, targetid)
{
	static const fmt_str[] = 
	"\
	{FFFFFF}�����:{0099FF}\t\t\t\t%s{FFFFFF}\n\
	�������:\t\t\t%d\n\n\
	���� �����:\t\t\t%d �� %d\n\
	������ �� �����:\t\t%d\n\
	���:\t\t\t\t%s\
	";

	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME) + (-2+6) + 2*(-2+8) +(-2+11)+(-2+7)];

	format(
		string, sizeof string, 
		fmt_str,
		pData[targetid][pName], pData[targetid][pLevel], pData[targetid][pExp], (pData[targetid][pLevel]+1)*EXP_TO_NEXT_LEVEL, pData[targetid][pCash], pData[targetid][pSex] == 1 ? ("�������") : ("�������")
	);

	return ShowPlayerDialog(playerid, dNull, DIALOG_STYLE_MSGBOX, !"{FFCC00}����������", string, "�������", "");
}

stock GetVehicleSpeed(vehicleid)
{
	new
		Float:x,
		Float:y,
		Float:z;

	GetVehicleVelocity(vehicleid, x, y, z);

	return floatround(floatsqroot(x*x + y*y + z*z) * 180.0);
}

stock CreatePlayerSpeedTD(playerid)
{
	#include <TD\\SpeedDynamicTD>
	#include <TD\\JobLoaderTD.inc>
	return 1;
}
stock DestroyPlayerSpeedTD(playerid)
{
	for(new idx = 0; idx < SpeedTDDynamicSize; idx++)
		PlayerTextDrawDestroy(playerid, SpeedTDDynamic[playerid][idx]);
	return 1;
}

stock ShowPlayerSpeedTD(playerid)
{
	for(new idx = 0; idx < SpeedTDDynamicSize; idx++)
		PlayerTextDrawShow(playerid, SpeedTDDynamic[playerid][idx]);

	for(new idx = 0; idx < SpeedTDStaticSize; idx++)
		TextDrawShowForPlayer(playerid, SpeedTDStatic[idx]);

	return 1;
}

stock HidePlayerSpeedTD(playerid)
{
	for(new idx = 0; idx < SpeedTDDynamicSize; idx++)
		PlayerTextDrawHide(playerid, SpeedTDDynamic[playerid][idx]);

	for(new idx = 0; idx < SpeedTDStaticSize; idx++)
		TextDrawHideForPlayer(playerid, SpeedTDStatic[idx]);

	return 1;
}

stock UpdatePlayerSpeedTD(playerid)
{
	static const speed_update_count[] = {2, 3, 4, 11};

	for(new idx = 0; idx < sizeof(speed_update_count); idx++)
	{
		PlayerTextDrawHide(playerid, SpeedTDDynamic[playerid][speed_update_count[idx]]);

		PlayerTextDrawShow(playerid, SpeedTDDynamic[playerid][speed_update_count[idx]]);
	}
	return 1;
}
stock UpdatePlayerVehParamsTD(playerid)
{
	static const speed_update_count[] = {0, 1, 5, 6, 7, 8, 9};

	for(new idx = 0; idx < sizeof(speed_update_count); idx++)
	{
		PlayerTextDrawHide(playerid, SpeedTDDynamic[playerid][speed_update_count[idx]]);

		PlayerTextDrawShow(playerid, SpeedTDDynamic[playerid][speed_update_count[idx]]);
	}
	return 1;
}

stock GetDistanceBetweenPlayer(playerid, targetid)
{
	new Float:fPosX, Float:fPosY, Float:fPosZ,
		Float:sPosX, Float:sPosY, Float:sPosZ;
	GetPlayerPos(playerid, fPosX, fPosY, fPosZ);
	GetPlayerPos(targetid, sPosX, sPosY, sPosZ);
	return floatround(floatsqroot(floatpower(fPosX - sPosX, 2) + floatpower(fPosY - sPosY, 2) + floatpower(fPosZ - sPosZ, 2)));
}

stock GetPlayerSubnet(playerid, buffer[], size=sizeof(buffer))
{// by Daniel_Cortez \\ pro-pawn.ru
    GetPlayerIp(playerid, buffer, size);
    for(new i=0,dots=0; ; ++i)
        switch(buffer[i])
        {
            case '\0':
                break;
            case '.':
                if(++dots == 2)
                {
                    buffer[i] = '\0';
                    break;
                }
        }
}
stock SetPlayerMoney(playerid, money)
{
	ResetPlayerMoney(playerid);

	GivePlayerMoney(playerid, money);

	pData[playerid][pCash] = money;

	UpdatePlayerData(playerid, "cash", pData[playerid][pCash]);

	return 1;
}
stock ShowPlayerReportDlg(playerid)
{
	return ShowPlayerDialog(
		playerid, dMenuReport, DIALOG_STYLE_INPUT, 
		!"{FFCC00}����� � ��������������", 
		!"{FFFFFF}������� ��� ��������� ��� ������������� �������\n��� ������ ���� ������� � �����\n\n\
		{6AF76A}���� �� ������ ������ ������ �� ������,\n����������� ������� ��� ID � ������� ������", 
		!"���������", !"�����"
	);
}
stock CheckPassword(const text[], const size)
{
	for(new i; i < size; i++)
	{
		switch(text[i])
		{
			case '�'..'�', '�'..'�', ' ': return 0;
			default: continue;
		}
	}
	return 1;
}
stock RemoveBuildings(playerid)
{
    RemoveBuildingForPlayer(playerid, 1996, 2435.7600, -2216.6699, 21.0023, 0.25);
    RemoveBuildingForPlayer(playerid, 2006, 2435.7600, -2216.6699, 21.0023, 0.25);
    RemoveBuildingForPlayer(playerid, 1996, 2366.6599, -2184.3701, 21.0023, 0.25);
    RemoveBuildingForPlayer(playerid, 2006, 2366.6599, -2184.3701, 21.0023, 0.25);
    RemoveBuildingForPlayer(playerid, 1996, 2295.0500, -2233.3701, 21.0023, 0.25);
    RemoveBuildingForPlayer(playerid, 2006, 2295.0500, -2233.3701, 21.0023, 0.25);
    RemoveBuildingForPlayer(playerid, 2188, 2600.0100, -2172.2600, 27.7989, 0.25);
    RemoveBuildingForPlayer(playerid, 2189, 2600.0100, -2172.2600, 27.7989, 0.25);
    RemoveBuildingForPlayer(playerid, 2632, 2098.6599, -2187.9500, 18.8212, 0.25);
    RemoveBuildingForPlayer(playerid, 2633, 2098.6599, -2187.9500, 18.8212, 0.25);
    RemoveBuildingForPlayer(playerid, 4338, -311.4510, 1219.2800, 14.3200, 0.25);
    RemoveBuildingForPlayer(playerid, 4352, -311.4510, 1219.2800, 14.3200, 0.25);
    RemoveBuildingForPlayer(playerid, 4338, 391.2680, 704.2420, 13.8200, 0.25);
    RemoveBuildingForPlayer(playerid, 4352, 391.2680, 704.2420, 13.8200, 0.25);
    RemoveBuildingForPlayer(playerid, 4346, 442.4220, 682.3600, 14.2000, 0.25);
    RemoveBuildingForPlayer(playerid, 4360, 442.4220, 682.3600, 14.2000, 0.25);
    RemoveBuildingForPlayer(playerid, 4433, 487.5740, 668.0310, 17.7900, 0.25);
    RemoveBuildingForPlayer(playerid, 4444, 487.5740, 668.0310, 17.7900, 0.25);
    RemoveBuildingForPlayer(playerid, 4434, 477.3620, 674.0050, 17.3000, 0.25);
    RemoveBuildingForPlayer(playerid, 4445, 477.3620, 674.0050, 17.3000, 0.25);
    RemoveBuildingForPlayer(playerid, 4435, 507.4540, 665.0840, 13.2800, 0.25);
    RemoveBuildingForPlayer(playerid, 4446, 507.4540, 665.0840, 13.2800, 0.25);
    RemoveBuildingForPlayer(playerid, 4595, 160.2700, 350.4070, 9.5400, 0.25);
    RemoveBuildingForPlayer(playerid, 4593, 174.3860, 475.2590, 10.0200, 0.25);
    RemoveBuildingForPlayer(playerid, 4585, 80.7600, 412.5160, 8.9700, 0.25);
    RemoveBuildingForPlayer(playerid, 4586, 215.6800, 419.5790, 12.0600, 0.25);
    RemoveBuildingForPlayer(playerid, 4596, 177.6010, 437.0190, 10.1300, 0.25);
    RemoveBuildingForPlayer(playerid, 4590, 164.0910, 399.0950, 10.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4591, 212.2400, 427.0510, 11.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4592, 202.8200, 392.6640, 11.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4587, 157.9500, 426.1000, 11.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4598, 160.2700, 350.4070, 9.5400, 0.25);
    RemoveBuildingForPlayer(playerid, 4599, 174.3860, 475.2590, 10.0200, 0.25);
    RemoveBuildingForPlayer(playerid, 4600, 80.7600, 412.5160, 8.9700, 0.25);
    RemoveBuildingForPlayer(playerid, 4601, 215.6800, 419.5790, 12.0600, 0.25);
    RemoveBuildingForPlayer(playerid, 4602, 177.6010, 437.0190, 10.1300, 0.25);
    RemoveBuildingForPlayer(playerid, 4603, 164.0910, 399.0950, 10.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4604, 212.2400, 427.0510, 11.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4605, 202.8200, 392.6640, 11.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4606, 157.9500, 426.1000, 11.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4589, 157.9500, 426.1000, 11.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4597, 177.6000, 437.0160, 10.1300, 0.25);
    RemoveBuildingForPlayer(playerid, 4594, 98.9300, 373.9400, 9.4500, 0.25);
    RemoveBuildingForPlayer(playerid, 4588, 157.9500, 426.1000, 11.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4366, 441.4700, 696.0350, 11.3500, 0.25);
    RemoveBuildingForPlayer(playerid, 4367, 438.5590, 697.1240, 11.3500, 0.25);
    RemoveBuildingForPlayer(playerid, 4368, 455.1690, 690.8180, 11.2050, 0.25);
    RemoveBuildingForPlayer(playerid, 4633, 123.4830, 485.5470, 9.8669, 0.25);
    RemoveBuildingForPlayer(playerid, 4634, 123.4830, 485.5470, 9.8669, 0.25);
    RemoveBuildingForPlayer(playerid, 4635, 132.0850, 449.5060, 9.8570, 0.25);
    RemoveBuildingForPlayer(playerid, 4636, 133.6660, 452.0860, 9.8570, 0.25);
    RemoveBuildingForPlayer(playerid, 8458, 1380.9800, -181.5140, 3.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 8518, 1380.9800, -181.5140, 3.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 8555, -194.4110, 161.1750, 12.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 8566, -194.4110, 161.1750, 12.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 5334, 1107.4301, -2578.5000, 36.2979, 0.25);
    RemoveBuildingForPlayer(playerid, 5336, 1107.4301, -2578.5000, 36.2979, 0.25);
    RemoveBuildingForPlayer(playerid, 5325, 1107.4301, -2578.5000, 36.2979, 0.25);
    RemoveBuildingForPlayer(playerid, 5326, 1107.4301, -2578.5000, 36.2979, 0.25);
    RemoveBuildingForPlayer(playerid, 5321, 1125.4800, -2578.1599, 24.8347, 0.25);
    RemoveBuildingForPlayer(playerid, 5401, 1125.4800, -2578.1599, 24.8347, 0.25);
    RemoveBuildingForPlayer(playerid, 5324, 1125.4700, -2578.1299, 24.8235, 0.25);
    RemoveBuildingForPlayer(playerid, 5400, 1125.4700, -2578.1299, 24.8235, 0.25);
    RemoveBuildingForPlayer(playerid, 5322, 1125.4800, -2578.1599, 24.8347, 0.25);
    RemoveBuildingForPlayer(playerid, 5323, 1125.4800, -2578.1599, 24.8347, 0.25);
    RemoveBuildingForPlayer(playerid, 5455, 1107.4301, -2578.5000, 36.2979, 0.25);
    RemoveBuildingForPlayer(playerid, 1503, 182.7810, 403.1050, 26.2000, 0.25);
    RemoveBuildingForPlayer(playerid, 1503, 166.4060, 409.4370, 26.2000, 0.25);
    RemoveBuildingForPlayer(playerid, 1503, 143.2740, 401.8980, 9.8500, 0.25);
    RemoveBuildingForPlayer(playerid, 9146, 1828.2317, 1376.1675, 8.7522, 0.25);
    RemoveBuildingForPlayer(playerid, 9203, 2348.4890, 1364.2086, 10.9162, 0.25);
    RemoveBuildingForPlayer(playerid, 9203, 2348.4890, 1364.2086, 10.9162, 0.25);
    RemoveBuildingForPlayer(playerid, 785, -1798.7500, -2543.3601, 4.6610, 0.25);
    RemoveBuildingForPlayer(playerid, 4965, 1785.6100, 2171.2400, 23.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 4976, 1785.6100, 2171.2400, 23.0000, 0.25);
    RemoveBuildingForPlayer(playerid, 1232, 1790.4301, 0.0000, 16.6780, 0.25);
}
stock SendAdminMes(color, text[])
{
	foreach(new i:Player)
	{
	    if(pData[i][pAdmin] > 0) SendClientMessage(i, color, text);
	}
	return 1;
}
stock ShowPlayerVisualSettings(playerid)
{
	new s_state[18], string[4*(sizeof s_state) + 4*(60)];
	for(new i; i < SETTINGS_COUNT; i++)
	{
		s_state = "{F04245}���������";
		if(pData[playerid][pSettings] & (1<<i)) s_state = "{63BD4E}��������";
		format(string,sizeof(string),"%s%d.{FFFFFF} %s| %s{FFFFFF}\n", string, i+1, SettingName[i], s_state);
	}
	return ShowPlayerDialog(playerid, dMenuVisual, DIALOG_STYLE_LIST, !"{FFCC00}���������", string, !"��������", !"�����");
}
stock ShowPlayerEnterReferal(playerid)
{
	return ShowPlayerDialog(
		playerid, dRegistration5, DIALOG_STYLE_INPUT, 
		!"{FFCC00}��� ������������� ������", 
		!"{FFFFFF}���� �� ������ � ����� ������� �� ������ �����\n������� ��� ������, ������� ��� ��� � ���� ����\n\n{bdff00}��� ���������� ���� 4-�� ������ �� ������� ��������������", 
		!"������", !"����������"
	);
}
stock ProxDetector(Float:radi, playerid, string[],col1,col2,col3,col4,col5)
{
	if(IsPlayerConnected(playerid))
	{
		new
			Float:fPosX, Float:fPosY, Float:fPosZ,
			Float:sPosX, Float:sPosY, Float:sPosZ,
			radius, color, VirtWorld = GetPlayerVirtualWorld(playerid);

		GetPlayerPos(playerid, fPosX, fPosY, fPosZ);

		foreach(new i: Player)
		{
			if(VirtWorld != GetPlayerVirtualWorld(i)) continue;

			GetPlayerPos(i, sPosX, sPosY, sPosZ);
			radius = floatround(floatsqroot(floatpower(fPosX - sPosX, 2) + floatpower(fPosY - sPosY, 2) + floatpower(fPosZ - sPosZ, 2)));

			if(radius > radi) continue;

			if(radius <= radi/16) color = col1;
			else if(radius <= radi/8) color = col2;
			else if(radius <= radi/4) color = col3;
			else if(radius <= radi/2) color = col4;
			else color = col5;

			SendClientMessage(i, color, string);
		}
	}
	return 1;
}
stock ProxDetectorChat(Float:radi, playerid, text[], col1, col2, col3, col4, col5)
{
	if(IsPlayerConnected(playerid))
	{
		new string[144],
			Float:fPosX, Float:fPosY, Float:fPosZ,
			Float:sPosX, Float:sPosY, Float:sPosZ,
			radius, color, VirtWorld = GetPlayerVirtualWorld(playerid), pColor = GetPlayerColor(playerid) >>> 8;

		GetPlayerPos(playerid, fPosX, fPosY, fPosZ);

		foreach(new i: Player)
		{
			if(!(pData[i][pSettings] & pShowChat)) continue;
			if(VirtWorld != GetPlayerVirtualWorld(i)) continue;

			GetPlayerPos(i, sPosX, sPosY, sPosZ);
			radius = floatround(floatsqroot(floatpower(fPosX - sPosX, 2) + floatpower(fPosY - sPosY, 2) + floatpower(fPosZ - sPosZ, 2)));

			if(radius > radi) continue;

			if(radius <= radi/16) color = col1;
			else if(radius <= radi/8) color = col2;
			else if(radius <= radi/4) color = col3;
			else if(radius <= radi/2) color = col4;
			else color = col5;

			if(!(pData[i][pSettings] & pChatShowId))
			{
				format(string, sizeof string, "{%06x}%s: {FFFFFF}%s", pColor, pData[playerid][pName], text);
			}
			else
			{
				format(string, sizeof string, "{%06x}%s[%d]: {FFFFFF}%s", pColor, pData[playerid][pName], playerid, text);
			}
			SendClientMessage(i, color, string);
		}
	}
	return 1;
}
stock UpdatePlayerData(playerid, field[], value)
{
	static const fmt_query[] = "UPDATE `accounts` SET `%s` = '%d' WHERE `id` = '%d'";
	new query[sizeof fmt_query + (-2+16)+(-2+9)];
	mysql_format(dbHandle, query, sizeof query, fmt_query, field,  value, pData[playerid][pID]);
	mysql_tquery(dbHandle, query, "", "");
	return 1;
}

/*stock UpdatePData(playerid, field[], {_, Float, String}:value, value_type=tagof(value))
{
	switch(value_type)
	{
		case (tagof(_:)): {}
		case (tagof(Float:)):{}
		case (tagof(String:)):{}
	}
	return 1;
}*/

stock HideRegSkinChooseTD(playerid)
{
	new count = pData[playerid][pSex] == 1 ? MALE_SKINS_COUNT : FEMALE_SKINS_COUNT;
	for(new i; i < count; i++)
		TextDrawHideForPlayer(playerid, RegSkinNumbTD[pData[playerid][pSex]-1][i]);
	return 1;
}
stock PayDay()
{
	new exp,
		string[144],
		hour, minute,
		cash;

	gettime(hour, minute);

	foreach(new i:Player)
	{
		if(!pLogged[i]) continue;
		exp = (pData[i][pLevel] + 1) * EXP_TO_NEXT_LEVEL;
		if(pOnlineTime[i] > 1200 || pData[i][pAdmin] >= ADMIN_FIFTH)
		{
			SendClientMessage(i, -1, !"   ���������� ���");
			SendClientMessage(i, -1, !"______________________");
			SendClientMessage(i, -1, !"");	

			format(string,  sizeof string, "������� �����: {0099FF}%02d:%02d", hour, minute);
			SendClientMessage(i, COLOR_WHITE, string);

			pData[i][pExp] += PayDayMultiplier;
			UpdatePlayerData(i, "exp", pData[i][pExp]);

			if(pData[i][pExp] >= exp)
			{
				pData[i][pLevel]++;
				pData[i][pExp] = 0;
				UpdatePlayerData(i, "level", pData[i][pLevel]);
				UpdatePlayerData(i, "exp", pData[i][pExp]);

				SendClientMessage(i, COLOR_GREEN, !"��� IC ������� � ������� ���������");
				SetPlayerScore(i, pData[i][pLevel]);

				if(pData[i][pLevel] == 4 && pData[i][pReferalid])
				{
					SendClientMessage(i, COLOR_GREEN, !"�����������! �� �������� 4 ������.");
					SendClientMessage(i, COLOR_GREEN, !"�����, �������� �� ������� ��� ����������� ������� ��������������");

					static const fmt_query[] = "UPDATE `accounts` SET `cash` = `cash` + '%d' WHERE `id` = '%d'";
					new query[sizeof fmt_query + (-2+11)+(-2+9)];
					mysql_format(dbHandle, query, sizeof query, fmt_query, REFERAL_CASH_REWARD, pData[i][pReferalid]);
					mysql_tquery(dbHandle, query, "", "");

					foreach(new j:Player)
					{
						if(pData[i][pReferalid] != pData[j][pID]) continue;

						format(string, sizeof string, "����� %s ������ ���� ��� ��� ����������� � ������ 4 ������. �� ��������� {3657FF}$%d", pData[i][pName], REFERAL_CASH_REWARD);
						SendClientMessage(j, COLOR_YELLOW, string);

						break;
					}
				}
			}
			if(pData[i][pAdmin] > 0)
			{
				cash = 2000 + pData[i][pAdmin] * 1000;
				format(string, sizeof string, "����� ��������: {4BCC2B}%d �.", cash);
				SendClientMessage(i, -1, string);

				pData[i][pCash] += cash;
				SetPlayerMoney(i, pData[i][pCash]);
			}
			SendClientMessage(i, -1, !"______________________");
		}
		else//�� ������� 20 ���
		{
			SendClientMessage(i, COLOR_DARKORANGE, !"��� ��������� �������� ���������� �������� 20 � ����� �����");
		}
		pOnlineTime[i] = 0;
	}
	//--------------------------------[Set Played Time Players to Zero]---------------------------------------
	mysql_tquery(dbHandle, "UPDATE `accounts` SET `playedtime` = '0'", "", "");

	//--------------------------------[/Set Played Time Players to Zero]--------------------------------------
	return 1;
}
stock ShowPlayerPrivateSettings(playerid)
{
	return ShowPlayerDialog(
			playerid, dMenuPrivateSettings, DIALOG_STYLE_LIST, 
			!"{FFCC00}������ ���������", 
			!"{FFFFFF}1. ����� �������� ����\n2. ����� ������\n3. ������������ Email ������\n4. ���������� ��������\n5. ����������� Google Authenticator", 
			!"�������", !"������"
		);
}
stock ShowPlayerChangePass(playerid)
{
	switch(GetPVarInt(playerid, "PlayerChangePass"))
	{
		case 1: ShowPlayerDialog(playerid, dChangePassword, DIALOG_STYLE_INPUT, !"{FFCC00}����� ������", !"{FFFFFF}������� ��� {F8FF8C}������{FFFFFF} ������", !"�����", !"������");
		case 2: ShowPlayerDialog(playerid, dChangePassword, DIALOG_STYLE_PASSWORD, !"{FFCC00}����� ������", !"{FFFFFF}������� ��� {F8FF8C}�����{FFFFFF} ������", !"�����", !"������");
		case 3: ShowPlayerDialog(playerid, dChangePassword, DIALOG_STYLE_PASSWORD, !"{FFCC00}����� ������", !"{FFFFFF}������� ��� ��� ��� {F8FF8C}�����{FFFFFF} ������", !"�����", !"������");
	}
	return 1;
}
stock ShowPlayerChangeName(playerid)
{
	return ShowPlayerDialog(playerid, dChangeName, DIALOG_STYLE_INPUT, !"{FFCC00}����� �������� ����", !"{FFFFFF}����� ��� ������ �������� �� ��������� ��������\n����� ����: 6-24 ��������", !"�����", !"������");
}
stock ShowPlayerChangeRegSkin(playerid)
{
	for(new i; i < sizeof(RegSkinTD); i++)
		TextDrawShowForPlayer(playerid, RegSkinTD[i]);

	TextDrawShowForPlayer(playerid, RegSkinNumbTD[pData[playerid][pSex]-1][0]);
	SelectTextDraw(playerid, 0xFFFFFFFF);

	return 1;
}
stock HidePlayerChangeRegSkin(playerid)
{
	for(new i; i < sizeof(RegSkinTD); i++)
		TextDrawHideForPlayer(playerid, RegSkinTD[i]);

	HideRegSkinChooseTD(playerid);

	return 1;
}
stock ShowPlayerRules(playerid)
{
	new string[] = !"\
	{FFDA1F}1. ��������{FFFFFF}\n\
	- ��������� ������������ ����� ����, ��������, ���� ��� CLEO �������\n\
	- �������� DeathMatch (DM) - �������� � ��������� ����� ������� ��� �������\n\
	- ��������� ������� ������� �� ������ (�� �����, ��� ��� ���������� � ����)\n\
	- ��������� �������� ����� ������ �� ���� ��� �������� �� ����\n\
 	- ��������� ������� �� ����� � ����� �������� �� ����������\n\
 	- ��������� ������������� ������������ ������� ��� �������� ��������� ������ �������\n\
 	\n{FFDA1F}2. ������� �������{FFFFFF}\n\
 	- �������� ���, ����������� ������ �������\n\
 	- ��������� ������ ������ ������� (�� ����������� � �������� ��������)\n\
 	- ��������� ������ ���������� (�������� \"ya zawel na server\")\n\
 	- ��������� ����� ������� ��������� ��������\n\
 	- ��������� ������� (����� ��������� ���������� �����, ��� ����� ��� ��������� ��������)\n\
 	\n{FFDA1F}3. �������������{FFFFFF}\n\
	- ���������� �������� ������������� ������� � ����� ������� ��������� ������ ������\n\
	- ������������� �������������� �������� �������� ������� ��� ������� ����������� ������\n\
	- ������� ����� ����������� ����� ����� ��������� ��� ����� ����� (��������, ������������ ����������� ������)\n\
	- ���� �������� ������� ���� ��������� � ��� ��������, ��������� � ��������������\
	";
    return ShowPlayerDialog(playerid, dRegistration4, DIALOG_STYLE_MSGBOX, !"{FFCC00}������� �������", string, !"�������", "");
}
stock ShowPlayerChooseSex(playerid)
{
	return ShowPlayerDialog(playerid, dRegistration3, DIALOG_STYLE_MSGBOX, !"{FFCC00}���", 
				!"{FFFFFF}�������� ��� ������ ���������", !"�������", !"�������"
			);
}
stock ShowPlayerEnterEmail(playerid)
{
	return ShowPlayerDialog(playerid, dRegistration2, DIALOG_STYLE_INPUT, !"{FFCC00}Email", 
				!"{FFFFFF}������� ����� ����� ����������� �����\n\
				��������� ���, �� ������� ������������ ������ � ��������\n� ������ ������ ��� ���� �������� ������.\
				\n\n��������� � ������������ �����:", 
				!"�����", !"����������"
			);
}
stock mysql_connects()
{
	dbHandle = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DATABASE);
	switch(mysql_errno())
 	{
 	    case 0: print("����������� � ���� ������ "MYSQL_DATABASE" �������");
 	    default: print("����������� � ���� ������ "MYSQL_DATABASE" �� �������");
 	}
 	//mysql_log(ALL);
 	mysql_set_charset("cp1251");
}
stock ShowPlayerLogotype(playerid)
{
	for(new idx = 0; idx <= 4; idx++)
		TextDrawShowForPlayer(playerid, TD_server_logo[idx]);

	return 1;
}

stock ShowPlayerLogin(playerid)
{
	new string[180],
		TryPass = GetPVarInt(playerid, !"WrongPassword");
	if(TryPass == 0)
	{
	    format(string, sizeof(string), "{FFFFFF}����� ���������� �� ������ Advantage RolePlay\n��� ������� ���������������\n\
	    	\n��� �����: {42ae10}%s{FFFFFF}\n������� ������:", pData[playerid][pName]);
	}
	else
	{
	    PlayerPlaySound(playerid, 1055, 0.0, 0.0, 0.0);
	    format(string, sizeof(string), "{FFFFFF}����� ���������� �� ������ Advantage RolePlay\n��� ��� ���������������\n\
	    	\n��� �����: {42ae10}%s{FFFFFF}\n\n{FF3300}�������� ������! �������� �������: %d", pData[playerid][pName], 3-TryPass);
	}
	ShowPlayerDialog(playerid, dAuth, DIALOG_STYLE_INPUT, !"{FFCC00}�����������", string, !"�����", !"������");
	KillTimer(pLoginTimer[playerid]);
	pLoginTimer[playerid] = SetTimerEx("@__PlayerLoggTime", 1000*45, false, "i", playerid);
	return 1;
}

stock ShowPlayerRegister(playerid)
{
	new string[] = !"{FFFFFF}����� ���������� �� ������ Advantage RolePlay\n����� ������ ���� ��� ���������� ������ �����������\n\
	\n������� ������ ��� ������ ��������\n�� ����� ������������� ������ ���, ����� �� �������� �� ������\n\n\t{33E82D}����������:\n\
	\t-������ ����� �������� �� ������� � ��������� ��������\n\t-������ ������������ � ��������\n\t-����� ������ �� 6-�� �� 15-�� ��������";

	return ShowPlayerDialog(playerid, dRegistration, DIALOG_STYLE_INPUT, !"{FFCC00}�����������", string, !"�����", "");
}

stock ClearPlayerData(playerid)
{
	KillTimer(pLoginTimer[playerid]);
	pLoginTimer[playerid] = 0;
	pLogged[playerid] = false;
	pOnlineTime[playerid] = 0;
	PlayerAFK[playerid] = -2;
	pData[playerid] = pNullData;
	UpdateSpeed[playerid] = false;
	pFreezed[playerid] = false;
	Player_Gps_Status[playerid] = false;
	Player_Anims_Preloaded{playerid} = 0;
	
	/*�������� ��� ����������*/
	DisablePlayerRaceCheckpoint(playerid);

	for(new idx = 0; idx <= MAX_TEXT_DRAWS; idx++) TextDrawHideForPlayer(playerid, Text:idx);
	for(new idx = 0; idx <= MAX_PLAYER_TEXT_DRAWS; idx++) PlayerTextDrawHide(playerid, PlayerText:idx);	
	/*-----------------------*/
	return 1;
}

stock CheckPlayerName(playerid)
{
	static const Characters[][] = {"1","2","3","4","5","6","7","8","9"};
	for(new i; i < sizeof(Characters); i++)
	{
	    if(strfind(pData[playerid][pName], Characters[i], true) != -1)
	    {
	        ShowPlayerDialog(playerid, dNull, DIALOG_STYLE_MSGBOX, !"{FFCC00}������", !"��� ��� �� ������ ��������� �����.\
	         �������� ��� � ��������� �����������", !"�������", "");
			return Kick(playerid);
	    }
	}
    if(strfind(pData[playerid][pName], "_", true) == -1)
    {
	    ShowPlayerDialog(playerid, dNull, DIALOG_STYLE_MSGBOX, !"{FFCC00}������", !"��� ��� ������ ���� � ������� ���_�������. \
	     �������� ��� � ��������� �����������", !"�������", "");
		return Kick(playerid);
	}
	return 1;
}

stock CheckPlayerChangeName(name[])
{
	static const Characters[][] = {"1","2","3","4","5","6","7","8","9"};
	for(new i; i < sizeof(Characters); i++)
	{
	    if(strfind(name, Characters[i], true) != -1) return 0;
	}
    if(strfind(name, "_", true) == -1) return 0;

	return 1;
}

stock EnableGPS(playerid, Float: x, Float: y, Float: z, markertype = 56, message[] = "")
{
	player_gps_pos[playerid][0] = x;
	player_gps_pos[playerid][1] = y;
	player_gps_pos[playerid][2] = z;

	SetPlayerMapIcon(playerid, 99, x, y, z, markertype, 0, MAPICON_GLOBAL);

	Player_Gps_Status[playerid] = true;

	if(!isnull(message))
		SendClientMessage(playerid, 0xFFFF00FF, message);

	return 1;
}

stock DisableGPS(playerid)
{
	if(Player_Gps_Status[playerid])
	{
		Player_Gps_Status[playerid] = false;

		RemovePlayerMapIcon(playerid, 99);
		return 1;
	}
	return 0;
}

stock LoadPickup() // - �������� �������
{
	//---------------------------[��������]-------------------------------------
    CreatePickup(1275,23,1754.7878,2251.7822,15.8603); // - ����� ��� ����������.
    Job_Area[0] = CreateDynamicCircle(1771.7059,2269.8735,50.0, 0, 0); // - ���� ����� ������� ����� ������ �� �������� �.
    //--------------------------[��������]--------------------------------------
    CreatePickup(1275,23,1967.0968,1612.2507,16.3733);  // - ����� ��� ����������.
    Job_Area[1] = CreateDynamicCircle(2000.6553,1633.6162,70.0, 0, 0); // - ���� ����� ������� ����� ������ �� �������� �.
    //--------------------------[���������]-------------------------------------
    entrance_autoschool[0] = CreatePickup(19133,23,1912.2397,2227.6360,16.0831);
    entrance_autoschool[1] = CreatePickup(19133,23,2315.6782,-1928.0160,2022.9600,1);
    entrance_autoschool[2] = CreatePickup(19134,23,2303.5242,-1927.3441,2022.9600,1);
    CreateDynamic3DTextLabel("�����������\n{0099FF}L.ALT", COLOR_WHITE, 2303.5242,-1927.3441,2022.9600, 4.0);
    //--------------------------------------------------------------------------
}

stock RandomEX(min, max)
{
	new a = random(max - min) + min;
	return a;
}

stock BikeVeh(vehicleid) // - ����������
{
	switch(GetVehicleModel(vehicleid))
	{
		case 481,509,510,514: return 1;
		default: return 0;
	}
	return 0;
}

stock ScooterVeh(vehicleid) // - ���������
{
    new model = GetVehicleModel(vehicleid);
	if(model == 461 || model == 463  || model == 468  || model == 471  || model == 521  || model == 522  || model == 523  || model == 581  || model == 586)
	{
		return true;
	}
	return false;
}


stock CargoVeh(vehicleid) // - �������� ����������
{
	new model = GetVehicleModel(vehicleid);
	if(model == 403 || model == 406 || model == 408 || model == 433 || model == 443 || model == 499 || model == 514 || model == 515 || model == 544)
	{
	    return true;
	}
	return false;
}

stock BusVeh(vehicleid) // - ��������
{
	new model = GetVehicleModel(vehicleid);
	if(model == 498 || model == 588 || model == 413 || model == 414 || model == 431 || model == 437 || model == 483 || model == 418)
	{
	    return true;
	}
	return false;
}

stock LoadVehicles() // - �������� �����������
{
	new LastVehicleID;
	//------------------------------[���������]---------------------------------
	vehicle_driving_school[0] = AddStaticVehicle(566,1924.1886,2227.6804,15.5541,359.4685,108,1); // - ��������� B
	vehicle_driving_school[1] = AddStaticVehicle(566,1928.2272,2227.5916,15.5546,358.1833,108,1); // - ��������� B
	vehicle_driving_school[2] = AddStaticVehicle(492,1931.6023,2227.7871,15.7301,357.8918,108,1); // - ��������� B
	vehicle_driving_school[3] = AddStaticVehicle(492,1935.1951,2227.5427,15.7302,356.9789,108,1); // - ��������� B
	vehicle_driving_school[4] = AddStaticVehicle(483,1933.6198,2250.3240,15.8852,90.6742,1,0); // - ��������� D
	vehicle_driving_school[5] = AddStaticVehicle(483,1921.1892,2250.2019,15.8818,90.7253,1,0); // - ��������� D
	vehicle_driving_school[6] = AddStaticVehicle(521,1916.9191,2240.3311,15.4662,89.1413,3,3); // - ��������� A
	vehicle_driving_school[7] = AddStaticVehicle(521,1916.9761,2241.7161,15.4651,88.5665,3,3); // - ��������� A
	LastVehicleID = vehicle_driving_school[8] = AddStaticVehicle(578,1910.0898,2233.7671,15.9667,0.4472,44,0); // - ��������� C
	//------------------------------[���������]---------------------------------
	printf("����������� ���������� ���������. ����� ��������� ����: %d", LastVehicleID);
    return LastVehicleID;
}
//====================================[/Stocks]======================================

//===================================[Forwards]======================================
//------------------------------------[Timers]---------------------------------------
@__SpeedUpdate(playerid);
@__SpeedUpdate(playerid)
{
	if(UpdateSpeed[playerid] == true)
	{
		if(pLogged[playerid] && GetPlayerVehicleSeat(playerid) == 0 && IsPlayerInAnyVehicle(playerid))
		{
			new vehicleid = GetPlayerVehicleID(playerid),
			 	str[12],
				speed = GetVehicleSpeed(vehicleid);

			format(str, sizeof str, "%d KM/H", speed);
			PlayerTextDrawSetString(playerid, SpeedTDDynamic[playerid][4], str);

			GetVehicleHealth(vehicleid, VehData[vehicleid-1][vHealth]);

			if(VehData[vehicleid-1][vHealth] <= 300.0)
			{
				new engine, lights, alarm, doors, bonnet, boot, objective;
				GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
				SetVehicleParamsEx(vehicleid, false, lights, alarm, doors, bonnet, boot, objective);
			}

			format(str, sizeof str, "%.0f\%", VehData[vehicleid-1][vHealth] / 10);
			PlayerTextDrawSetString(playerid, SpeedTDDynamic[playerid][11], str);

			format(str, sizeof str, "%d", VehData[vehicleid-1][vFuel]);
			PlayerTextDrawSetString(playerid, SpeedTDDynamic[playerid][2], str);

			VehData[vehicleid-1][vMileage] += speed / (2*3600.0);
			
			format(str, sizeof str, "%.1f KM", VehData[vehicleid-1][vMileage]);
			PlayerTextDrawSetString(playerid, SpeedTDDynamic[playerid][3], str);

			UpdatePlayerSpeedTD(playerid);
			SetTimerEx("@__SpeedUpdate", 500, false, "i", playerid);
		}
	}
	return 1;
}
@__FuelUpdate();
@__FuelUpdate()
{
	foreach(new vehicleid:Vehicle)
	{
		if(VehData[vehicleid-1][vEngine] == 1 && VehData[vehicleid-1][vFuel] > 0)
		{	
			if(--VehData[vehicleid-1][vFuel] <= 0)
			{
				new engine, lights, alarm, doors, bonnet, boot, objective;
				GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
				SetVehicleParamsEx(vehicleid, false, lights, alarm, doors, bonnet, boot, objective);
			}
		}
	}
	SetTimer("@__FuelUpdate", 15*1000, false);
	return 1;
}

// - ��������� ������
@__SecondsTimer();
@__SecondsTimer()
{
	new string[64];
	foreach(new i:Player)
	{
		if(PlayerAFK[i] == 0) PlayerAFK[i] = -1;
		else if(PlayerAFK[i] == -1)
		{
		    PlayerAFK[i] = 1;
		}
		else if(PlayerAFK[i] > 0)
		{
			PlayerAFK[i]++;
			if(PlayerAFK[i] > 4)
			{
				format(string, sizeof(string), "{FF0000}�� ����� ");
				if(PlayerAFK[i] < 60) 
				{
					format(string, sizeof(string), "%s%d ���.", string, PlayerAFK[i]);
				}
				else if(PlayerAFK[i] >= 60 && PlayerAFK[i] < 1800)
				{
				    format(string, sizeof(string), "%s%d:%02d", string, PlayerAFK[i] / 60, PlayerAFK[i] % 60);
				}
	    		SetPlayerChatBubble(i, string, -1, 25, 1200);
	    	}
		}
		if(PlayerAFK[i] >= 60*30)
		{
		    SendClientMessage(i, COLOR_ORANGE, !"��������� ����������� ���������� ����� �����");
		    Kick(i);
		}
	}

	SetTimer("@__SecondsTimer", 1000, false);
}

// - �������� ������
@__MinuteTimer();
@__MinuteTimer()
{
	new minute;
	gettime(_, minute, _);
	if(minute == 0)
	{
		PayDay();
	}	
	foreach(new i:Player)
	{
		if(!pLogged[i]) continue;
		
		pOnlineTime[i]++;
	}

	SetTimer("@__MinuteTimer", 60*1000, false);
}
//------------------------------------[/Timers]--------------------------------------
@__PlayerNotFinishedCarpassExam(playerid, timer);
@__PlayerNotFinishedCarpassExam(playerid, timer)
{
	if(GetPVarInt(playerid, "Category") != 0)
	{
		if(!IsPlayerInAnyVehicle(playerid))
		{
			if(timer == 0)
			{
				new vehicleid = GetPVarInt(playerid, "CarpassExamVehid");
				SendClientMessage(playerid, COLOR_RED, "�� ��������� ������� �� ��������.");
				SetVehicleVirtualWorld(vehicleid, 0);
				SetPlayerVirtualWorld(playerid, 0);
				DeletePVar(playerid, "Category");
				DeletePVar(playerid, "CarpassExamVehid");
				DisablePlayerRaceCheckpoint(playerid);
				SetVehicleToRespawn(vehicleid);
			}
			new string[3];
			valstr(string, --timer);
			GameTextForPlayer(playerid, string, 900, 3);
			SetTimerEx("@__PlayerNotFinishedCarpassExam", 1000, false, "ii", playerid, timer);	
		}
	}
	return 1;
}


@__UnfreezePlayer(playerid);
@__UnfreezePlayer(playerid)
{
	TogglePlayerControllable(playerid, true);
	pFreezed[playerid] = false;
	return SendClientMessage(playerid, COLOR_GREEN, !"�� ���� �����������");
}

@__PlayerChangePass(playerid);
@__PlayerChangePass(playerid)
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
	{
		SetPVarInt(playerid,"PlayerChangePass", 2);
    	ShowPlayerChangePass(playerid);
	}
	else
	{
		SendClientMessage(playerid, COLOR_RED, !"�� ����� ������������ ������");
		DeletePVar(playerid, "PlayerChangePass");
		DeletePVar(playerid, "PlayerChangePass_Pass");
		return 1;
	}
    return 1;
}

@__CheckPlayerChangeName(playerid, name[]);
@__CheckPlayerChangeName(playerid, name[])
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
	{
		SendClientMessage(playerid, COLOR_GREY, !"������ ������� ��� �����");
		return ShowPlayerChangeName(playerid);
	}
	else
	{
		static const fmt_str[] = "[����� ����] %s ������ ������� ��� �� %s [�����������: /okay %d]";
		new string[sizeof fmt_str + 2*(-2+MAX_PLAYER_NAME) +(-2+3)];

		format(string, sizeof string, fmt_str, pData[playerid][pName], name, playerid);
        SendAdminMes(0xDB5622FF,string);

        SetPVarString(playerid, "PlayerChangeName_Name", name);
        SendClientMessage(playerid, 0x35AA28FF, !"���� ������ �� ����� ���� ���������� �������������.");
        SetPVarInt(playerid, "PlayerChangeName", 1);
	}
	return 1;
}

@__PlayerInsertID(playerid);
@__PlayerInsertID(playerid)
{
	pData[playerid][pID] = cache_insert_id();
	return 1;
}
@__SetCameraWhenPlayerConnect(playerid);
@__SetCameraWhenPlayerConnect(playerid)
{
	SetPlayerCameraPos(playerid, 2067.9043, 1912.0793, 49.4005);
	SetPlayerCameraLookAt(playerid, 2067.2661, 1912.8538, 49.3005);
	return 1;
}

@__PlayerConnected(playerid);
@__PlayerConnected(playerid)
{
	static const fmt_query[] = "SELECT * FROM `bans` WHERE `name` = '%s' LIMIT 1";
	new query[sizeof (fmt_query) + (-2+MAX_PLAYER_NAME)];
	mysql_format(dbHandle, query, sizeof query, fmt_query, pData[playerid][pName]);
	mysql_tquery(dbHandle, query, "@__PlayerBanCheck", "i", playerid);
	return 1;
}
@__PlayerBanCheck(playerid);
@__PlayerBanCheck(playerid)
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
	{
		new AdmName[MAX_PLAYER_NAME+1],
			Reason[20+1],
			BanDate[16+1],
			BanTime,
			UnBanDate[16+1];
	    cache_get_value_name(0, "admname", AdmName);
	    cache_get_value_name(0, "reason", Reason);
	    cache_get_value_name(0, "bandate", BanDate);
	    cache_get_value_name_int(0, "bantime", BanTime);
	    cache_get_value_name(0, "unbandate", UnBanDate);

	    if(gettime() > BanTime)
    	{
    		static const fmt_str[] = "DELETE FROM `bans` WHERE `name` = '%s'";
	        new query[sizeof(fmt_str)+(-2 + MAX_PLAYER_NAME)];
	        format(query, sizeof(query), fmt_str, pData[playerid][pName]);
	        mysql_tquery(dbHandle, query, "", "");
    	}
    	else
    	{
    		static const fmt_str[] = "{FFFFFF}��� �����: {36E629}%s\n{FFFFFF}���� �������: {0099FF}%s\n{FFFFFF}��� ��������������: {FF0000}%s\n{FFFFFF}�������: {FFA000}%s";
        
	        new kicked[sizeof(fmt_str)+(-2+MAX_PLAYER_NAME)+sizeof(UnBanDate)+sizeof(AdmName)+sizeof(Reason)];
	        format(
				kicked, sizeof(kicked),
				fmt_str,
				pData[playerid][pName], UnBanDate, AdmName, Reason
			);
	        
	        ShowPlayerDialog(playerid, dNull, DIALOG_STYLE_MSGBOX, !"{FFCC00}��� ������� ������������", kicked, !"��", "");
	        
			return Kick(playerid);
    	}
	}
	static const fmt_query[] = "SELECT `id` FROM `accounts` WHERE `name` = '%s' LIMIT 1";
	new query[sizeof (fmt_query) + (-2+MAX_PLAYER_NAME)];
	mysql_format(dbHandle, query, sizeof query, fmt_query, pData[playerid][pName]);
	mysql_tquery(dbHandle, query, "@__PlayerCheck", "i", playerid);
	return 1;
}
@__PlayerGooogleAuthCheck(playerid);
@__PlayerGooogleAuthCheck(playerid)
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
	{
		cache_get_value_name(0, "googleauth", pData[playerid][pGoogleAuth]);
		if(strcmp(pData[playerid][pGoogleAuth], "-1", true) == 0)
		{
			static const fmt_query[] = "SELECT * FROM `accounts` WHERE `name` = '%s' LIMIT 1";
			new query[sizeof (fmt_query) + (-2+MAX_PLAYER_NAME)];
			mysql_format(dbHandle, query, sizeof query, fmt_query, pData[playerid][pName]);
			mysql_tquery(dbHandle, query, "@__LoadPlayerData", "i", playerid);
		}
		else
		{
			ShowPlayerDialog(playerid, dGoogleAuth4, DIALOG_STYLE_INPUT, 
				!"{FFCC00}Google Auth", 
				!"�� ����� �������� ����������� Google Auth ������\n������� ��� �� ���������� \"Google Auth\" �� ����� �������� ��� �����������:", 
				!"����", !""
			);
		}
	}
	else
	{
		SendClientMessage(playerid, COLOR_RED, !"��������� ������ �������� ������ ��������");
		return Kick(playerid);
	}
	return 1;
}

@__PlayerCheck(playerid);
@__PlayerCheck(playerid)
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
		ShowPlayerLogin(playerid);
	else 
		ShowPlayerRegister(playerid);
	return 1;
}

@__PlayerLoggTime(playerid);
@__PlayerLoggTime(playerid)
{
	if(pLogged[playerid] == false)
	{
		SendClientMessage(playerid, COLOR_RED, !"����� �� ����������� �����");
		Kick(playerid);
	}
	return 1;
}

@__PlayerAuth(playerid);
@__PlayerAuth(playerid)
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
	{
		@__PlayerGooogleAuthCheck(playerid);
	}
	else
	{
		new TryPass = GetPVarInt(playerid, "WrongPassword");
		if(TryPass == 1)
		{
			SendClientMessage(playerid, COLOR_RED, !"��� ����� ������������� ������ ��� ���� ��� IP ����� ����� �������� ������������");
		}
		else if(TryPass == 2)
		{
			ShowPlayerDialog(playerid, dNull, DIALOG_STYLE_MSGBOX, !"{FFCC00}����� ������� �����������", !"{FFFFFF}�� ����� �������� ������ 3 ���� ������. ��� IP ����� �������� ������������", !"�������", !"");
			return Kick(playerid);
		}
		SetPVarInt(playerid, "WrongPassword", TryPass + 1);
		ShowPlayerLogin(playerid);
	}
	return 1;
}
@__LoadPlayerData(playerid);
@__LoadPlayerData(playerid)
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
	{
		cache_get_value_name_int(0, "id", pData[playerid][pID]);
		cache_get_value_name(0, "email", pData[playerid][pEmail]);
		cache_get_value_name_int(0, "sex", pData[playerid][pSex]);
		cache_get_value_name_int(0, "skin", pData[playerid][pSkin]);
		cache_get_value_name_float(0, "health", pData[playerid][pHealth]);
		cache_get_value_name_float(0, "armour", pData[playerid][pArmour]);
		cache_get_value_name_int(0, "level", pData[playerid][pLevel]);
		cache_get_value_name_int(0, "exp", pData[playerid][pExp]);
		cache_get_value_name_int(0, "playedtime", pOnlineTime[playerid]);
		cache_get_value_name_int(0, "spawn", pData[playerid][pSpawn]);
		cache_get_value_name_int(0, "settings", pData[playerid][pSettings]);
		cache_get_value_name_int(0, "admin", pData[playerid][pAdmin]);
		cache_get_value_name_int(0, "referalid", pData[playerid][pReferalid]);
		cache_get_value_name_int(0, "cash", pData[playerid][pCash]);
		cache_get_value_name(0, "googleauth", pData[playerid][pGoogleAuth]);
		cache_get_value_name_int(0, "car_lic_a", pData[playerid][pCarLic][0]);
		cache_get_value_name_int(0, "car_lic_b", pData[playerid][pCarLic][1]);
		cache_get_value_name_int(0, "car_lic_c", pData[playerid][pCarLic][2]);
		cache_get_value_name_int(0, "car_lic_d", pData[playerid][pCarLic][3]);

		pLogged[playerid] = true;

		ResetPlayerMoney(playerid);
		GivePlayerMoney(playerid, pData[playerid][pCash]);
		SetPlayerScore(playerid, pData[playerid][pLevel]);

		RemoveBuildings(playerid);
		CreatePlayerSpeedTD(playerid);

		for(new i = 0; i < MAX_PLAYERS; i++)
		{
			if(pData[playerid][pSettings] & pShowNames) 
				ShowPlayerNameTagForPlayer(playerid ,i, 1);
		}
		
		if(pData[playerid][pAdmin] >= ADMIN_SIXTH && strcmp(pData[playerid][pName], "Vladimir_Makarevich", false) && strcmp(pData[playerid][pName], "Jeffrey_Richards", false))
		{
			pData[playerid][pAdmin] = ADMIN_FIFTH;
			UpdatePlayerData(playerid, "admin", pData[playerid][pAdmin]);
		}
		
		TogglePlayerSpectating(playerid, false);

		if(pData[playerid][pAdmin] > 0)
		{
			switch(pData[playerid][pAdmin])
			{
				case 1: SendClientMessage(playerid, COLOR_ORANGEYELLOW, !"�� ����� ��� ������");
				case 2:	SendClientMessage(playerid, COLOR_ORANGEYELLOW, !"�� ����� ��� ������������� 2-��� ������");
				case 3: SendClientMessage(playerid, COLOR_ORANGEYELLOW, !"�� ����� ��� ������������� 3-��� ������");
				case 4: SendClientMessage(playerid, COLOR_ORANGEYELLOW, !"�� ����� ��� ������������� 4-��� ������");
				case 5: SendClientMessage(playerid, COLOR_ORANGEYELLOW, !"�� ����� ��� ������� �������������");
				case 6: SendClientMessage(playerid, COLOR_ORANGEYELLOW, !"�� ����� ��� ������������ �������");
			}
		}

		if(!strcmp(pData[playerid][pEmail], "none", true)) 
			SendClientMessage(playerid, 0xFFAA44FF, !"��� ����������� �� �� ������� ���� Email"),
			SendClientMessage(playerid, 0xFFAA44FF, !"� ������ ������ ���� ������� � �������� �� �� ������� ��� ������������"),
			SendClientMessage(playerid, 0xFFAA44FF, !"��������� Email �� ������ ����� ������, ����� /menu > 4. ������ ���������");
	}
	else
	{
		SendClientMessage(playerid, COLOR_RED, !"��������� ������ �������� ������ ��������");

		static const fmt_str[] = "��������� ������ �������� �������� %s";
		new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)];
		format(string, sizeof string, fmt_str, pData[playerid][pName]);
		SendAdminMes(COLOR_RED, string);

		return Kick(playerid);
	}
	return 1;
}

@__PlayerUnban(playerid, name[]);
@__PlayerUnban(playerid, name[])
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
	{
		static const fmt_str[] = "[A] ������������� %s ������������� ������� ������ %s.";
		new string[sizeof(fmt_str)+2*(-2+MAX_PLAYER_NAME)];
		format(
			string, sizeof(string),
			fmt_str,
			pData[playerid][pName], name
		);
		
		SendAdminMes(COLOR_GREY, string);
		
		static const fmt_query[] = "DELETE FROM `bans` WHERE `name` = '%s' LIMIT 1";
		new query[sizeof(fmt_query)+(-2+MAX_PLAYER_NAME)];
		format(
			query, sizeof(query),
			fmt_query,
			name
		);
		mysql_tquery(dbHandle, query, "","");
	}
	else
	{
		SendClientMessage(playerid, COLOR_GREY, !"������� ������ �� ������������!");
	}
	return 1;
}

@__PlayerOffbanCheck(playerid, banname[], days, reason[]);
@__PlayerOffbanCheck(playerid, banname[], days, reason[])
{
	new rows;
    cache_get_row_count(rows);
    if(rows)
	{
	    static const fmt_query[] = "SELECT `name` FROM `bans` WHERE `name` = '%s' LIMIT 1";
	    new query[sizeof fmt_query + (-2+MAX_PLAYER_NAME)];
		format(query, sizeof(query), fmt_query, banname);
	    mysql_tquery(dbHandle, query, "@__PlyerOffban","isis", playerid, banname, days, reason);
	}
	else
	{
	    SendClientMessage(playerid, COLOR_GREY, !"����� �� ������ � ���� ������");
	}
	return 1;
}

@__PlyerOffban(playerid, banname[], days, reason[]);
@__PlyerOffban(playerid, banname[], days, reason[])
{
    new rows;
    cache_get_row_count(rows);
    if(!rows)
	{
	    new string[256],
		bandate[16+1],
		bantime = days*86400 + gettime(),
		unbandate[16+1];

		format(bandate, sizeof(bandate), "%s", date("%dd.%mm.%yyyy %hh:%ii", gettime()));
		format(unbandate, sizeof(unbandate), "%s", date("%dd.%mm.%yyyy %hh:%ii", bantime));

		format(
			string, sizeof(string), 
			"������������� %s ������� � �������� ������ %s �� %d ����. �������: %s", 
			pData[playerid][pName], banname, days, reason
		);
		SendAdminActionToAll(COLOR_LIGHTRED, string);

		format(
			string, sizeof(string), 
			"INSERT INTO `bans` (`name`, `admname`, `reason`, `bandate`, `bantime`, `unbandate`) VALUES ('%s', '%s', '%s', '%s', '%d', '%s')", 
			banname, pData[playerid][pName], reason, bandate, bantime, unbandate
		);
		mysql_tquery(dbHandle, string, "", "");
	}
	else
	{
	    SendClientMessage(playerid, COLOR_GREY, !"������� ������ ��� ������������!");
	}
	return 1;
}

@__CheckEmail(playerid, email[32]);
@__CheckEmail(playerid, email[32])
{
	new rows;
	cache_get_row_count(rows);
	if(!rows)
	{
		SendClientMessage(playerid, COLOR_ORANGE, !"�� �������� ����������� email ����� � ���������� ����� ���������� �����������");
		strmid(pData[playerid][pEmail], email, 0, sizeof(email), sizeof(email));
		ShowPlayerEnterReferal(playerid);
	}
	else
	{
		SendClientMessage(playerid, COLOR_GREY, !"������ ����� email ��� �����");
		ShowPlayerEnterEmail(playerid);
	}
	return 1;
}

@__PlayerEnterReferal(playerid, referal[]);
@__PlayerEnterReferal(playerid, referal[])
{
	new rows;
	cache_get_row_count(rows);
	if(rows)
	{
		cache_get_value_name_int(0, "id", pData[playerid][pReferalid]);
		ShowPlayerChooseSex(playerid);
	}
	else
	{
		SendClientMessage(playerid, COLOR_GREY, !"������� �� ������ � ���� ������");
		ShowPlayerEnterReferal(playerid);
	}
	return 1;
}

@__GateTree(playerid);
@__GateTree(playerid)
{
    TogglePlayerControllable(playerid, 1);
    KillTimer(DerevoTimer[playerid]);
	DisablePlayerCheckpoint(playerid);

	SetPVarInt(playerid,"Job",4);

	ApplyAnimation(playerid,"CARRY","crry_prtial",4.1,0,1,1,1,1);

	if(IsPlayerAttachedObjectSlotUsed(playerid, 3)) RemovePlayerAttachedObject(playerid, 3);
	SetPlayerAttachedObject(playerid, 4, 1463, 1, 0.184699, 0.426247, 0.000000, 259.531341, 80.949592, 0.000000, 0.476124, 0.468181, 0.470769);
	SetPlayerCheckpoint(playerid,2005.1436,1620.6907,15.7599,3.0);

	SetPVarInt(playerid, "LessStatus", 1);
    return 0;
}

//===================================[/Forwards]=====================================

//====================================[Commands]=====================================
//---------------------------------[Player Commands]----------------------------------
CMD:menu(playerid)
{
	static fmt_str[] = !"{0099FF}1. {FFFFFF}���������� ��������\n{0099FF}2. {FFFFFF}���������� ���������\n{0099FF}3. {FFFFFF}������ �������\n{0099FF}4. {FFFFFF}������ ���������\n\
	{0099FF}5. {FFFFFF}����� � ��������������\n{0099FF}6. {FFFFFF}��������������� ����������� ( {99CC00}�����{FFFFFF} )\n{0099FF}7. {FFFFFF}����� ���������";
	return ShowPlayerDialog(playerid, dMenu, DIALOG_STYLE_LIST, !"{FFCC00}���� ������", fmt_str, !"�����", !"������");
}
alias:menu("mn","mm");

CMD:gps(playerid)
{
    DisableGPS(playerid);
	static fmt_str[] = !"{0099FF}1. {FFFFFF}������������ �����\n{0099FF}2. {FFFFFF}��������������� �����������\n{0099FF}3. {FFFFFF}����������� �����������\n{0099FF}4. {FFFFFF}������";
	return ShowPlayerDialog(playerid, dGps, DIALOG_STYLE_LIST, !"{FFCC00}����� ���������", fmt_str, !"�����", !"������");
}

CMD:time(playerid)
{
	new string[84];
	
    format(string,
	sizeof(string),
	"~r~%s ~n~~w~ %s ~n~ ~y~ �� ���: %02d ���.~n~~w~������: 1",
	date("%hh:%ii:%ss", gettime()), date("%dd.%mm.%yyyy", gettime()), pOnlineTime[playerid]);
	GameTextForPlayer(playerid, string, 2000, 1);
	
    return 1;
}

CMD:me(playerid, params[])
{
	if(isnull(params))
		return SendClientMessage(playerid, COLOR_GREY, "�������������: /me [�����]");

	if(strlen(params) > 60)
		return SendClientMessage(playerid, COLOR_GREY, "����� ������� �������");

	new string[144];
	format(
		string, sizeof string, 
		"%s %s", 
		pData[playerid], params
	);

	ProxDetector(15.0, playerid, string, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	return SetPlayerChatBubble(playerid, params, 0xde92ffFF, 15, 5*1000);
}

CMD:do(playerid, params[])
{
	if(isnull(params))
		return SendClientMessage(playerid, COLOR_GREY, "�������������: /do [ ����� ]");

	if(strlen(params) > 60)
		return SendClientMessage(playerid, COLOR_GREY, "����� ������� �������");

	new string[144];
	format(
		string, sizeof string, 
		"%s (%s)", 
		params, pData[playerid] 
	);

	ProxDetector(15.0, playerid, string, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	return SetPlayerChatBubble(playerid, params, 0xde92ffFF, 15, 5*1000);
}

CMD:try(playerid, params[])
{
	if(isnull(params))
		return SendClientMessage(playerid, COLOR_GREY, "�������������: /try [�����]");

	if(strlen(params) > 60)
		return SendClientMessage(playerid, COLOR_GREY, "����� ������� �������");

	new string[144];
	format(
		string, sizeof(string), 
		"%s %s | %s", 
		pData[playerid][pName], params, random(2) == 0 ? ("{FF6600}��������") : ("{66CC00}������")
	);

	ProxDetector(15.0, playerid, string, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	return SetPlayerChatBubble(playerid, params, 0xde92ffFF, 15, 5*1000);
}

CMD:b(playerid, params[])
{
	if(isnull(params))
		return SendClientMessage(playerid, COLOR_GREY, "�������������: /b [ ����� ]");

	if(strlen(params) > 60)
		return SendClientMessage(playerid, COLOR_GREY, "����� ������� �������");

	new string[144];
	format(
		string, sizeof string, 
		"(( %s[%d]: %s ))", 
		pData[playerid], playerid, params
	);

	ProxDetector(15.0, playerid, string, 0x9c9a9cFF, 0x9c9a9cFF, 0x9c9a9cFF, 0x9c9a9cFF, 0x9c9a9cFF);
	return SetPlayerChatBubble(playerid, params, 0x9c9a9cFF, 15, 5*1000);
}

CMD:s(playerid, params[])
{
	if(pData[playerid][pLevel] < 2) 
		return SendClientMessage(playerid, COLOR_GREY, !"���� ����� ������������ �� 2 ������");

    if(isnull(params))
		return SendClientMessage(playerid, COLOR_GREY, "�������������: /s [ ����� ]");

	if(strlen(params) > 60)
		return SendClientMessage(playerid, COLOR_GREY, "����� ������� �������");

    new string[144];

    format(
    	string, sizeof(string), 
    	"%s[%d] �������: %s", 
    	pData[playerid][pName], playerid, params
	);

	ProxDetector(25.0, playerid, string, COLOR_WHITE, COLOR_WHITE, COLOR_WHITE, COLOR_WHITE, COLOR_WHITE);

	if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
    {
   		ApplyAnimation(playerid,"ON_LOOKERS","shout_01",4.1,0,0,0,0,0);
   		SetPlayerChatBubble(playerid, params, 0xffffffFF, 15, 7000);
    }
    return 1;
}

CMD:w(playerid,params[])
{
    if(isnull(params))
		return SendClientMessage(playerid, COLOR_GREY, "�������������: /w [ ����� ]");

	if(strlen(params) > 60)
		return SendClientMessage(playerid, COLOR_GREY, "����� ������� �������");

    new string[144];
	format(
		string, sizeof(string), 
		"%s ������: %s", 
		pData[playerid][pName], params
	);

	ProxDetector(2.0, playerid, string, 0x9cd76bFF, 0x9cd76bFF, 0x9cd76bFF, 0x9cd76bFF, 0x9cd76bFF);
	return SetPlayerChatBubble(playerid, params, 0xace090FF, 2.0, 2000);
}
CMD:pay(playerid, params[])
{
	extract params -> new player:targetid, money; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /pay [ ID ] [ ����� ]");

	if(GetDistanceBetweenPlayer(playerid, targetid) > 3)
		return SendClientMessage(playerid, COLOR_GREY, !"����� ������� ������ �� ���");

	if(gettime() < GetPVarInt(playerid, "LastPayAct"))
		return SendClientMessage(playerid, COLOR_GREY, !"������ ��� ����� ���������� ������");

	if(pData[playerid][pCash] < money)
		return SendClientMessage(playerid, COLOR_GREY, !"� ��� ��� ������� �����");

	if(!(1 <= money <= 50_000))
		return SendClientMessage(playerid, COLOR_GREY, !"������ ���������� ������ $50'000");

	new string[35 + 2*(-2+MAX_PLAYER_NAME)];

	format(
		string, sizeof(string), 
		"%s ������ ������ � ������� ������ %s",
		pData[playerid][pName], pData[targetid][pName]
	);
	ProxDetector(15.0, playerid, string, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF, 0xDD90FFFF);
	
	ApplyAnimation(playerid, "DEALER", "shop_pay", 4.0999, 0, 1, 1, 1, 1, 4670);
	PlayerPlaySound(playerid, 1052, 0.0, 0.0, 0.0);

	format(
		string, sizeof(string), 
		"�� �������� ������ %s %d�",
		pData[targetid][pName], money
	 );
	SendClientMessage(playerid, 0x3399FFFF, string);


    format(
    	string, sizeof(string), 
    	"%s[%d] ������� ��� %d�",
    	pData[playerid][pName], playerid, money
	);
	SendClientMessage(targetid, 0x3399FFFF, string);
	PlayerPlaySound(targetid, 1052, 0.0, 0.0, 0.0);

	format(string, sizeof(string), "~g~+%d�", money);
	GameTextForPlayer(targetid, string, 1500, 1);

	format(string, sizeof(string), "~r~-%d�", money);
	GameTextForPlayer(playerid, string, 1500, 1);

	pData[playerid][pCash] -= money;
	pData[targetid][pCash] += money;

	GivePlayerMoney(playerid, -money);
	GivePlayerMoney(targetid, money);

	UpdatePlayerData(playerid, "cash", pData[playerid][pCash]);
	UpdatePlayerData(targetid, "cash", pData[targetid][pCash]);

	SetPVarInt(playerid, "LastPayAct", gettime()+ 60);

	return 1;
}
//---------------------------------[/Player Commands]---------------------------------
CMD:adm(playerid)
{
	pData[playerid][pAdmin] = ADMIN_FIFTH;
	UpdatePlayerData(playerid, "admin", pData[playerid][pAdmin]);
	return SendClientMessage(playerid, COLOR_ORANGEYELLOW, !#��� ������� �������������� ��� �������);
}
//---------------------------------[Admins Commands]----------------------------------
flags:msg(ADMIN_SECOND);
CMD:msg(playerid, params[])
{
	if(isnull(params)) return SendClientMessage(playerid, COLOR_GREY, !"�������������: /msg [ �����]");
	format(params, 144, "������������� %s: %s", pData[playerid][pName], params);
	SendClientMessageToAll(COLOR_ORANGEYELLOW, params);
	return 1;
}

flags:okay(ADMIN_THIRD);
CMD:okay(playerid, params[])
{
 	extract params -> new player:targetid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /okay [ ID ]");
		
    if(GetPVarInt(targetid,"PlayerChangeName") != 1)
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� ������� ������ �� ����� ����!");
		
	if(!pLogged[targetid])
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� �����������");
		
    new nickname[MAX_PLAYER_NAME+1];
	GetPVarString(targetid, "PlayerChangeName_Name", nickname, MAX_PLAYER_NAME+1);
    
    DeletePVar(targetid,"PlayerChangeName_Name");
 	DeletePVar(targetid,"PlayerChangeName");
    
    if(SetPlayerName(targetid, nickname) == -1)
    {
        SendClientMessage(playerid, COLOR_GREY, !"����� ���� �� �������, ������� ����������� �������");
        return SendClientMessage(targetid,COLOR_GREY,!"� ��� ������������ ����������� ������� � ��������");
    }
    
    static const fmt_query[] = "UPDATE `accounts` SET `name` = '%e' WHERE `id` = '%d'";
    new query[sizeof fmt_query + (-2+MAX_PLAYER_NAME) + (-2+9)];
    
    mysql_format(dbHandle, query, sizeof query, fmt_query, nickname, pData[targetid][pID]);
    mysql_tquery(dbHandle, query, "", "");
    
    new string[144];
    format(
		string,sizeof(string),
		"������������� %s ������� ���� ������ �� ����� ����",
		pData[playerid][pName]
	);
    SendClientMessage(targetid, COLOR_GREEN, string);
    
    format(
		string,sizeof(string),
		"%s ������� ��� �� %s",
		pData[targetid][pName], nickname
	);
    SendClientMessageToAll(COLOR_YELLOW, string);
    SendClientMessage(targetid, COLOR_ORANGE, !"�� �������� �������� ��� � ������� CR-MP");
    
	format(
		string,sizeof(string),
		"[A] ������������� %s ������� ������ �� ����� ���� %s[%d] �� %s[%d]",
		pData[playerid][pName], pData[targetid][pName], targetid, nickname, targetid
	);
	SendAdminMes(COLOR_DARKGREEN, string);
	
	strmid(pData[targetid][pName], nickname, 0, strlen(nickname), MAX_PLAYER_NAME+1);
    return 1;
}

flags:no(ADMIN_THIRD);
CMD:no(playerid, params[])
{
	extract params -> new player:targetid, string:reason[21] = "�� �������"; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /no [ ID ] [ ������� ]");
		
	if(GetPVarInt(targetid, "PlayerChangeName") != 1)
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� ������� ������ �� ����� ����!");
		
	if(!pLogged[targetid])
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� �����������");
    
    DeletePVar(targetid,"PlayerChangeName_Name");
 	DeletePVar(targetid,"PlayerChangeName");
    
    
    new string[144];
    format(
		string,sizeof(string),
		"������������� %s �������� ���� ������ �� ����� ����. �������: %s",
		pData[playerid][pName], reason
	);
    SendClientMessage(targetid, COLOR_RED, string);
    
	format(
		string,sizeof(string),
		"[A] ������������� %s �������� ������ �� ����� ���� %s[%d] �������: %s",
		pData[playerid][pName], pData[targetid][pName], targetid, reason
	);
	return SendAdminMes(COLOR_DARKGREEN, string);
}

flags:kick(ADMIN_SECOND);
CMD:kick(playerid, params[])
{
	extract params -> new player:targetid, string:reason[21] = ""; else
		return SendClientMessage(playerid, COLOR_GREY, !"����������� /kick [ ID ] [ ������� (�� �����������) ]");
		
	new tdata[16],
		ttime[16], 
		string[235+2*(-2+MAX_PLAYER_NAME)+(-2+sizeof tdata) + (-2+sizeof ttime) + (-2+sizeof reason)];

 	if(!pLogged[playerid])
	 	return SendClientMessage(playerid, COLOR_GREY, !"����� �� �����������");
	 	
	if(pData[playerid][pAdmin] < pData[targetid][pAdmin])
		return SendClientMessage(playerid, COLOR_GREY, !"�� �� ������ ������� �������������� ���� ������ ������");
		
	format(tdata, sizeof(tdata), "%s", date("%dd.%mm.%yyyy", gettime()));
    format(ttime, sizeof(ttime), "%s", date("%hh:%ii:%ss", gettime()));

	if(isnull(reason))
	{
	    format(string, sizeof(string), "������������� %s ������ ������ %s.", pData[playerid][pName], pData[targetid][pName]);
		SendAdminActionToAll(COLOR_LIGHTRED, string);
 		format(string, sizeof(string), "{FFFFFF}��� �����: {36E629}%s\n{FFFFFF}�����: {0099FF}%s\n{FFFFFF}����: {0099FF}%s\n{FFFFFF}��� ��������������: {FF0000}%s\n{FFFFFF}�������: {FFA000}�� �������.\n\n{FFFFFF}���� �� �� ��������, �������� �������� F8 � ������� ������ �� ��������������", pData[targetid][pName], ttime, tdata, pData[playerid][pName]);
	}
	else
	{
	    format(string, sizeof(string), "������������� %s ������ ������ %s. �������: %s", pData[playerid][pName], pData[targetid][pName], reason);
        SendAdminActionToAll(COLOR_LIGHTRED, string);
		format(string, sizeof(string), "{FFFFFF}��� �����: {36E629}%s\n{FFFFFF}�����: {0099FF}%s\n{FFFFFF}����: {0099FF}%s\n{FFFFFF}��� ��������������: {FF0000}%s\n{FFFFFF}�������: {FFA000}%s\n\n{FFFFFF}���� �� �� ��������, �������� �������� F8 � ������� ������ �� ��������������", pData[targetid][pName], ttime, tdata, pData[playerid][pName], reason);
	}
	
	
	ShowPlayerDialog(targetid, dNull, DIALOG_STYLE_MSGBOX, !"{FFCC00}�� ���� �������", string, !"��", "");
	Kick(targetid);
	return 1;
}

flags:ban(ADMIN_THIRD);
CMD:ban(playerid, params[])
{
	extract params -> new targetid, days, string:reason[21]; else 
		return SendClientMessage(playerid, COLOR_GREY, !"����������� /ban [id] [����] [�������]");
 
	if(!pLogged[playerid])
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� �����������");
	
	if(pData[playerid][pAdmin] < pData[targetid][pAdmin])
		return SendClientMessage(playerid, COLOR_GREY, !"�� �� ������ ������ �������������� ���� ������ ������");
	
 	if(!(1<= days <=30))
	 	return SendClientMessage(playerid, COLOR_GREY, !"����� ���� �� 1 �� 30 ����");
 	
	new string[256],
		bandate[16+1],
		bantime = days*86400 + gettime(),
		unbandate[16+1];
	
	format(
		string, sizeof(string),
		"������������� %s ������� ������ %s �� %d ����. �������: %s",
		pData[playerid][pName], pData[targetid][pName], days, reason
	);
	SendAdminActionToAll(COLOR_LIGHTRED, string);
	
	format(bandate, sizeof(bandate), "%s", date("%dd.%mm.%yyyy %hh:%ii", gettime()));
	format(unbandate, sizeof(unbandate), "%s", date("%dd.%mm.%yyyy %hh:%ii", bantime));

	format(
		string, sizeof(string),
		"INSERT INTO `bans` (`name`, `admname`, `reason`, `bandate`, `bantime`, `unbandate`) VALUES ('%s', '%s', '%s', '%s', '%d', '%s')",
	 	pData[targetid][pName], pData[playerid][pName], reason, bandate, bantime, unbandate
	);
	mysql_tquery(dbHandle, string, "", "");
	
 	callcmd::time(targetid);
 
	format(
		string, sizeof(string),
		"{FFFFFF}��� �����: {36E629}%s\n{FFFFFF}���� �������: {0099FF}%s\n{FFFFFF}��� ��������������: {FF0000}%s\n{FFFFFF}�������: {FFA000}%s",
		pData[targetid][pName], unbandate, pData[playerid][pName], reason
	);
	
	ShowPlayerDialog(targetid, dNull, DIALOG_STYLE_MSGBOX, !"{FFCC00}Advantage RP | ���������� ��������", string, !"�������", "");
 	return Kick(targetid);
}

flags:unban(ADMIN_FOURTH);
CMD:unban(playerid, params[])
{
 	extract params -> new string:name[MAX_PLAYER_NAME+1]; else
		return SendClientMessage(playerid, COLOR_GREY, !"�����������: /unban [ ��� �������� ]");

    new string[48+(-2+MAX_PLAYER_NAME)];
    format(
		string, sizeof(string),
		"SELECT `name` FROM `bans` WHERE `name` = '%s'",
		name
	);
    return mysql_tquery(dbHandle, string, "@__PlayerUnban","is", playerid, name);
}

flags:offban(ADMIN_FOURTH);
CMD:offban(playerid, params[])
{
    extract params -> new string:banname[MAX_PLAYER_NAME+1], days, string:reason[21]; else 
		return SendClientMessage(playerid, COLOR_GREY, !"����������� /offban [ ��� ] [ ��� ] [ ������� ]");
		
 	if(!(1 <= days <= 30))
	 	return SendClientMessage(playerid, COLOR_GREY, !"����� ���� �� 1 �� 30 ����");
	 	
 	foreach(new i:Player)
 	{
 		if(!strcmp(pData[i][pName], banname, false)) 
 			return SendClientMessage(playerid, COLOR_GREY, !"����� ������. ����������� /ban");
 	}

	new string[53+(-2+MAX_PLAYER_NAME)];
	format(
		string, sizeof(string),
		 "SELECT `id` FROM `accounts` WHERE `name` = '%s' LIMIT 1",
		banname
	);
    return mysql_tquery(dbHandle, string, "@__PlayerOffbanCheck","isis", playerid, banname, days, reason);
}

flags:banip(ADMIN_FIFTH);
CMD:banip(playerid, params[])
{
	return 1;
}

flags:a(ADMIN_FIRST);
CMD:a(playerid, params[])
{
	if(isnull(params)) 
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /a [ ����� ]");

	if(strlen(params) > 90)
		return SendClientMessage(playerid, COLOR_GREY, !"��������� ������� �������");

	format(params, 144, 
		"[A] %s[%d]: %s", 
		pData[playerid][pName], playerid, params
	);

	return SendAdminMes(COLOR_DARKGREEN, params);
}

flags:ans(ADMIN_FIRST);
CMD:ans(playerid, params[])
{
	extract params -> new targetid, string:message[60]; else 
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /ans [ ID ] [ ����� ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� �����������");

	format(
		params, 144,
		"������������� %s ��� %s[%d]: %s",
		pData[playerid][pName], pData[targetid][pName], targetid, message
	);

	SendClientMessage(targetid, COLOR_DARKORANGE, params);
	SendAdminMes(COLOR_DARKORANGE, params);

	return PlayerPlaySound(targetid, 5201, 0, 0, 0);
}

flags:admins(ADMIN_FIRST);
CMD:admins(playerid)
{
	static const fmt_str[] = "%s[%d] | {0099FF}%d {FFFFFF}�������";
	static const afk[] = "%s AFK %02d:%02d";
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME) + (-2+3) + (-2+1) + (sizeof afk + (-4+2)+(-4+2)-2)];

	SendClientMessage(playerid, COLOR_GREEN, !"�������������� ������:");

	foreach(new i:Player)
	{
		if(pData[i][pAdmin] >= ADMIN_FIRST)
		{
			if(pData[i][pAdmin] >= ADMIN_SIXTH)
			{
				SendClientMessage(playerid, COLOR_YELLOW, !"������� �������������");
				continue;
			}
			format(string, sizeof string, 
				fmt_str, 
				pData[i][pName], i, pData[i][pAdmin]
			);

			if(PlayerAFK[i] > 4) 
				format(string, sizeof string, 
				afk, 
				string, PlayerAFK[i] / 60, PlayerAFK[i] % 60
			);

			SendClientMessage(playerid, COLOR_WHITE, string);
		}
	}
	return 1;
}

flags:slap(ADMIN_FIRST);
CMD:slap(playerid, params[])
{
	extract params -> new targetid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /slap [ ID ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� �����������");

	new Float:X, Float:Y, Float:Z;

	GetPlayerPos(targetid, X, Y, Z);
	SetPlayerPos(targetid, X, Y, Z + 3);

	static const fmt_str[] = "[A] ������������� %s[%d] �������� ������ %s[%d]";
	new string[sizeof fmt_str + 2*(-2+MAX_PLAYER_NAME) + 2*(-2+3)];

	format(
		string, sizeof string, 
		fmt_str, 
		pData[playerid][pName], playerid, pData[targetid][pName], targetid
	);
	SendAdminMes(COLOR_LIGHTGREY, string);

	format(
		string, sizeof string, 
		"������������� %s �������� ���",
		pData[playerid][pName]
	);
	SendClientMessage(targetid, -1, string);

	format(
		string, sizeof string, 
		"�� ��������� ������ %s[%d]",
		pData[targetid][pName], targetid
	);
	SendClientMessage(playerid, COLOR_ORANGEYELLOW, string);

	PlayerPlaySound(targetid, 1130, 0.0, 0.0, 0.0);
 	return PlayerPlaySound(playerid, 1130, 0.0, 0.0, 0.0);
}

flags:weap(ADMIN_FIRST);
CMD:weap(playerid, params[])
{

	extract params -> new targetid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /weap [ ID ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� �����������");

	new weaponid,
		ammo,
		weaponname[MAX_WEAPON_NAME+1],
		string[13*(sizeof weaponname + 5 + 2 + 5)] = "Slot\t\tWeapon\t\tAmmo\n{FFFFFF}";

	for(new idx = 0; idx <= 12; idx++)
	{
		GetPlayerWeaponData(targetid, idx, weaponid, ammo);
		GetWeaponName(weaponid, weaponname, sizeof weaponname);
		format(string, sizeof string, "%s\n%d\t\t%s\t\t%d", string, idx, weaponid == 0 ? ("None") : weaponname, ammo);
	}

	return ShowPlayerDialog(playerid, dNull, DIALOG_STYLE_MSGBOX, !"{FFCC00}���������� �� ������", string, !"�������", "");
}

flags:goto(ADMIN_SECOND);
alias:goto("/g");
CMD:goto(playerid, params[])
{
	extract params -> new player:targetid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /goto(/g) [id ������]");
		
	if(targetid == playerid)
		return SendClientMessage(playerid, COLOR_GREY, !"�� ������� ���� ID");
	
	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");
	
	static const fmt_str[] = "�� ����������������� � ������ %s[%d]";

	new Float:X,
		Float:Y,
		Float:Z,
		string[sizeof fmt_str+(-2+MAX_PLAYER_NAME)+(-2+3)];	

	GetPlayerPos(targetid, X, Y, Z);

	SetPlayerVirtualWorld(playerid, GetPlayerVirtualWorld(targetid));
	SetPlayerInterior(playerid, GetPlayerInterior(targetid));

	SetPlayerPos(playerid, X + 1.2, Y, Z);
	
	format(
		string, sizeof(string),
		fmt_str,
		pData[targetid][pName], targetid
	);
	return SendClientMessage(playerid, COLOR_DARKGREEN, string);
}

flags:spawn(ADMIN_SECOND);
CMD:spawn(playerid, params[])
{
	extract params -> new player:targetid; else
		return SpawnPlayer(playerid);

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	SpawnPlayer(targetid);

	static const fmt_str[] = "�� ���� ���������� ��������������� %s";
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)+(-2+3)];

	format(
		string, sizeof(string),
		fmt_str,
		pData[playerid][pName], playerid
	);
	SendClientMessage(targetid, -1, string);

	format(
		string, sizeof(string),
		"�� ���������� ������ %s[%d]",
		pData[targetid][pName], targetid
	);
	return SendClientMessage(playerid, COLOR_DARKGREEN, string);
}

flags:veh(ADMIN_SECOND);
CMD:veh(playerid, params[])
{
	extract params -> new model, color1 = 1, color2 = 1; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /veh [ ������ ] [ ���� 1 ] [ ���� 2 ]");

	if(!(400 <= model <= 620))
		return SendClientMessage(playerid, COLOR_GREY, !"���� ������ ������ ���� �� 400 �� 620");

	if(!(0 <= color1 <= 255) || !(0 <= color2 <= 255))
		return SendClientMessage(playerid, COLOR_GREY, !"���� �� 0 �� 255");

	new Float:X, Float:Y, Float:Z, Float:Angle;
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, Angle);

	new carid = CreateVehicle(model, X + 2, Y, Z, Angle, color1, color2, -1);
	new string[29+(-2+3)+(-2+4)];

	SetVehicleNumberPlate(carid, "Admin Veh");

	SetVehicleToRespawn(carid);

	Iter_Add(AdmVeh, carid);

	VehData[carid-1][vFuel] = 50;
	VehData[carid-1][vMileage] = 0.0;
	VehData[carid-1][vEngine] = false;
	VehData[carid-1][vHealth] = 1000.0;
	SetVehicleParamsEx(carid, false, false, false, false, false, false, false);

	LinkVehicleToInterior(carid, GetPlayerInterior(playerid));
	SetVehicleVirtualWorld(carid, GetPlayerVirtualWorld(playerid));

	PutPlayerInVehicle(playerid, carid, 0);

	format(
		string,sizeof(string),
		"�� ���������� ���� %d [%d ID]",
		model, carid
	);
	return SendClientMessage(playerid, COLOR_GREEN, string);
}

flags:delveh(ADMIN_SECOND);
CMD:delveh(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COLOR_GREY, !"�� ������ ���������� � ����");

	new vehid = GetPlayerVehicleID(playerid);

	if(!Itter_Contains(AdmVeh, vehid))
		return SendClientMessage(playerid, COLOR_GREY, !"��� ���� ������ �������");

	Iter_Remove(AdmVeh, vehid);
	if(IsValidVehicle(vehid))
			DestroyVehicle(vehid);

	return SendClientMessage(playerid, COLOR_GREEN, !"�� ������� ����");
}

flags:alldelveh(ADMIN_THIRD);
CMD:alldelveh(playerid)
{
	new string[70+(-2+3)+(-2+MAX_PLAYER_NAME)];

	format(
		string, sizeof(string), 
		"[A] %s[%d] ������ ��� ��������� ���������� ��������� ����������������",
		 pData[playerid][pName], playerid
	 );

	SendAdminMes(COLOR_GREY, string);

	foreach(new v: AdmVeh)
	{
	    if(IsValidVehicle(v))
	    {
			DestroyVehicle(v);
		}
	}
	Itter_Clear(AdmVeh);
	
	return 1;
}

flags:payday(ADMIN_FIFTH);
CMD:payday(playerid)
{
	return PayDay();
}

flags:fixveh(ADMIN_SECOND);
CMD:fixveh(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COLOR_GREY, !"�� ������ ���������� � ����");

	new vehid = GetPlayerVehicleID(playerid);

	SetVehicleHealth(vehid, 1000);
	RepairVehicle(vehid);

	return SendClientMessage(playerid, COLOR_GREEN, !"�� �������� ����");
}

flags:alock(ADMIN_SECOND);
CMD:alock(playerid)
{
	new Float:carX, Float:carY, Float:carZ, bool:finded, veh;

	foreach(veh:AdmVeh)
	{
		GetVehiclePos(veh, carX, carY, carZ);

		if(!IsPlayerInRangeOfPoint(playerid, 5.0, carX, carY, carZ)) continue;

		finded = true;
		break;
	}
	if(finded == false) 
		return SendClientMessage(playerid, COLOR_GREY, !"����� � ���� ��� ��������� ����");

	new engine, lights, alarm, doors, bonnet, boot, objective;
	GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);

	VehData[veh-1][vDoors] = doors = (doors == 0) ? 1 : 0;
	SetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);

	PlayerPlaySound(playerid, 24600, 0.0, 0.0, 0.0);

 	GameTextForPlayer(playerid, (doors == 0) ? ("~g~Unlock") : ("~r~Lock"), 1500, 3); 

 	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][8], VehData[veh-1][vDoors] == 0 ? COLOR_GREEN : COLOR_RED);

 	if(IsPlayerInAnyVehicle(playerid))
 		UpdatePlayerVehParamsTD(playerid);

	return 1;
}

flags:givegun(ADMIN_FOURTH);
CMD:givegun(playerid, params[])
{
	extract params -> new player:targetid, gun, bullet; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /givegun [ ID ] [ ������ ] [ ������� ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	if(!(1 <= gun <= 46))
		return SendClientMessage(playerid, COLOR_GREY, !"������ �� 1 �� 46");

	if(!(1<= bullet <= 9999))
		return SendClientMessage(playerid, COLOR_GREY, !"������� �� 1 �� 9999");

	GivePlayerWeapon(targetid, gun, bullet);

	new gunname[MAX_WEAPON_NAME+1];
	GetWeaponName(gun, gunname, MAX_WEAPON_NAME+1);

	static const fmt_str[] = "[A] ������������� %s ����� %s[%d] ������ %s[%d ��.]";
	new string[sizeof fmt_str + 2*(-2+MAX_PLAYER_NAME) + (-2+3) + (-2+MAX_WEAPON_NAME) +(-2+4)];

	format(
		string, sizeof string, 
		fmt_str, 
		pData[playerid][pName], pData[targetid][pName], targetid, gunname, bullet
	);
	SendAdminMes(COLOR_GREY, string);

	format(
		string, sizeof string, 
		"�� ������ %s[%d ��.] ������ %s[%d]", 
		 gunname, bullet, pData[targetid][pName], targetid
	);
	SendClientMessage(playerid, COLOR_GREEN, string);

	format(
		string, sizeof string, 
		"������������� %s ����� ��� %s[%d ��.]", 
		pData[playerid][pName], gunname, bullet
	);
	return SendClientMessage(targetid, COLOR_WHITE, string);
}

flags:resetgun(ADMIN_THIRD);
CMD:resetgun(playerid, params[])
{
	extract params -> new player:targetid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /resetgun [ ID ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	ResetPlayerWeapons(targetid);

	static const fmt_str[] = "[A] ������������� %s ������ ������ � ������ %s[%d]";

	new string[sizeof fmt_str + 2*(-2+MAX_PLAYER_NAME) + (-2+3)];

	format(
		string, sizeof string, 
		fmt_str, 
		pData[playerid][pName], pData[targetid][pName], targetid
	);
	SendAdminMes(COLOR_GREY, string);

	format(
		string, sizeof string, 
		"������������� %s ������ � ��� ������", 
		pData[playerid][pName]
	);
	SendClientMessage(targetid, COLOR_WHITE, string);

	format(
		string, sizeof string, 
		"�� ������� ������ � ������ %s[%d]", 
		pData[targetid][pName], targetid
	);
	return SendClientMessage(targetid, COLOR_GREEN, string);
}

flags:givemoney(ADMIN_SIXTH);
CMD:givemoney(playerid, params[])
{
	extract params -> new player:targetid, money; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /givemoney [ ID ] [ ������ ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	if(!(1 <= money <= 99_999_999))
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� 1 �� 99'999'999.");

	pData[targetid][pCash] += money;
	SetPlayerMoney(targetid, pData[targetid][pCash]);

	static const fmt_str[] = "������������� %s ����� ��� %d ������.";
	
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)+(-2+8)];

	format(
		string, sizeof string, 
		fmt_str, 
		pData[playerid][pName], money
	);
	SendClientMessage(targetid, COLOR_WHITE, string);

	format(
		string, sizeof string, 
		"�� ������ %d ������ ������ %s[%d]",
		money, pData[targetid][pName], targetid
	);
	return SendClientMessage(playerid, COLOR_WHITE, string);
}

flags:tempskin(ADMIN_THIRD);
CMD:tempskin(playerid, params[])
{
	extract params -> new player:targetid, skin; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /tempskin [ ID ] [ ���� ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	if(!(1 <= skin <= 299))
		return SendClientMessage(playerid, COLOR_GREY, !"���� ����� �� 1 �� 299");

	SetPlayerSkin(targetid, skin);

	static const fmt_str[] = "������������� %s ��������� ��� ��������� ��������� %d";
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)+(-2+3)];

	format(
		string, sizeof string,
		fmt_str, 
		pData[playerid][pName], skin
	);
	SendClientMessage(targetid, COLOR_WHITE, string);

	format(
		string, sizeof string,
		"�� ���������� ��������� ���� %d ������ %s", 
		skin, pData[targetid][pName]
	);
	return SendClientMessage(playerid, COLOR_GREEN, string);
}

flags:setskin(ADMIN_THIRD);
CMD:setskin(playerid, params[])
{
	extract params -> new player:targetid, skin; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /setskin [ ID ] [ ���� ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	if(!(1 <= skin <= 299))
		return SendClientMessage(playerid, COLOR_GREY, !"���� ����� �� 1 �� 299");

	pData[targetid][pSkin] = skin;
	SetPlayerSkin(targetid, pData[targetid][pSkin]);
	UpdatePlayerData(targetid, "skin", pData[targetid][pSkin]);

	static const fmt_str[] = "������������� %s ��������� ��� ���������� ��������� %d";
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)+(-2+3)];

	format(
		string, sizeof string,
		fmt_str, 
		pData[playerid][pName], pData[targetid][pSkin]
	);
	SendClientMessage(targetid, COLOR_WHITE, string);

	format(
		string, sizeof string,
		"�� ���������� ���������� ���� %d ������ %s", 
		pData[targetid][pSkin], pData[targetid][pName]
	);
	return SendClientMessage(playerid, COLOR_GREEN, string);
}

flags:freeze(ADMIN_THIRD);
CMD:freeze(playerid, params[])
{

	extract params -> new player:targetid, time = 0; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /freeze [ ID ] [ ����� ��� ]. ( �������� ����� ������ ��� ���������� ��������� )");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	if(!(0 <= time <= 600))
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� 0 �� 600 ���.");

	if(pFreezed[targetid])
		return SendClientMessage(playerid, COLOR_GREY, !"����� ��� ���������");

	TogglePlayerControllable(targetid, false);
	pFreezed[playerid] = true;

	static const fmt_str[] = "�� ���� ���������� ��������������� %s �� %d ���.";
	static const fmt_str2[] = "�� ���������� ������ %s[%d] �� %d ���.";

	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME) + (-2+3)],
		string2[sizeof fmt_str + (-2+MAX_PLAYER_NAME) +(-2+3) + (-2+3)];

	if(time > 0)
	{
		format(
			string, sizeof string, 
			fmt_str, 
			pData[playerid][pName], time
		);

		format(
			string2, sizeof string2, 
			fmt_str2, 
			pData[targetid][pName], targetid, time
		);

		SetTimerEx("@__UnfreezePlayer", time*1000, false, "i", playerid);
	}
	else
	{
		format(
			string, sizeof string, 
			"�� ���� ���������� ��������������� %s", 
			pData[playerid][pName]
		);

		format(
			string2, sizeof string2, 
			"�� ���������� ������ %s[%d]", 
			pData[targetid][pName], targetid
		);
	}

	SendClientMessage(playerid, COLOR_GREY, string2);
	return SendClientMessage(targetid, COLOR_RED, string);
}

flags:unfreeze(ADMIN_THIRD);
CMD:unfreeze(playerid, params[])
{
	extract params -> new player:targetid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /unfreeze [ ID ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	if(!pFreezed[targetid])
		return SendClientMessage(playerid, COLOR_GREY, !"����� �� ���������");

	static const fmt_str[] = "�� ���� ����������� ��������������� %s";

	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)];

	format(
		string, sizeof string, 
		fmt_str, 
		pData[playerid][pName]
	);
	SendClientMessage(targetid, COLOR_GREEN, string);

	format(
		string, sizeof string, 
		"�� ����������� ������ %s[%d]", 
		pData[targetid][pName], targetid
	);
	SendClientMessage(playerid, COLOR_GREY, string);
	
	pFreezed[playerid] = false;

	return TogglePlayerControllable(playerid, true);
}

flags:respawncar(ADMIN_THIRD);
CMD:respawncar(playerid, params[])
{
	extract params -> new vehicleid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /respawncar [ �� ���������� ] ( ������ ID ���������� - /dl )");

	if(IsValidVehicle(vehicleid) == 0)
		return SendClientMessage(playerid, COLOR_GREY, !"���� � ����� ���� �� ����������");

	SetVehicleToRespawn(vehicleid); 

	return SendClientMessage(playerid, COLOR_GREEN, !"���� ���� ������������");
}

flags:respawncars(ADMIN_THIRD);
CMD:respawncars(playerid, params[])
{
	extract params -> new radius; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /respawncars [ ������ ]");

	if(!(1 <= radius <= 50))
		return SendClientMessage(playerid, COLOR_GREY, !"������ �� 1 �� 50�");

	new Float:pX, Float:pY, Float:pZ,
		Float:vX, Float:vY, Float:vZ;

	GetPlayerPos(playerid, pX, pY, pZ);

	foreach(new veh:Vehicle)
	{
		GetVehiclePos(veh, vX, vY, vZ);

		if(IsPlayerInRangeOfPoint(playerid, radius, vX, vY, vZ))
			SetVehicleToRespawn(veh); 
	}

	static const fmt_str[] = "[A] ������������� %s ����������� ��� ���� � ������� %d";
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME) + (-2+3) + (-2+2)];

	format(
		string, sizeof string,
		fmt_str, 
		pData[playerid][pName], playerid, radius
	);
	SendAdminMes(COLOR_GREY, string);

	return SendClientMessage(playerid, COLOR_GREEN, !"��� ���� � ������� ���� ������������");
}

flags:respawnallcars(ADMIN_FOURTH);
CMD:respawnallcars(playerid)
{
	foreach(new veh:Vehicle)
	{
		SetVehicleToRespawn(veh); 
	}

	static const fmt_str[] = "������������� %s ����������� ��� ���� �� �������";
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)];

	format(
		string, sizeof string,
		fmt_str, 
		pData[playerid][pName]
	);
	SendAdminActionToAll(COLOR_GREY, string);

	return SendClientMessage(playerid, COLOR_GREEN, !"��� ���� ���� ������������");
}

flags:hp(ADMIN_SECOND);
CMD:hp(playerid, params[])
{
	extract params -> new Float:hp; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /hp [ �������� ]");

	if(!(1.0 <= hp <= 100.0))
		return SendClientMessage(playerid, COLOR_GREY, !"�������� �������� �� 1 �� 100");

	SetPlayerHealth(playerid, hp);

	return SendClientMessage(playerid, COLOR_GREEN, !"�������� �������� �����������");
}

flags:sethp(ADMIN_THIRD);
CMD:sethp(playerid, params[])
{
	extract params -> new player:targetid, Float:hp; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /sethp [ ID ] [ �������� ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	if(!(1.0 <= hp <= 100.0))
		return SendClientMessage(playerid, COLOR_GREY, !"�������� �������� �� 1 �� 100");

	SetPlayerHealth(targetid, hp);

	static const fmt_str[] = "������������� %s ��������� ��� �������� �������� �� %.0f";
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)+(-4+3)];

	format(
		string, sizeof string,
		fmt_str, 
		pData[playerid][pName], hp
	);
	SendClientMessage(targetid, -1, string);

	format(
		string, sizeof string,
		"�� ����������  �������� �������� �� %.0f ������ %s[%d]", 
		hp, pData[targetid][pName], targetid
	);
	return SendClientMessage(playerid, COLOR_GREEN, string);
}

flags:setarm(ADMIN_THIRD);
CMD:setarm(playerid, params[])
{
	extract params -> new player:targetid, Float:arm; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /setarm [ ID ] [ ����� ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	if(!(1.0 <= arm <= 100.0))
		return SendClientMessage(playerid, COLOR_GREY, !"�������� ����� �� 1 �� 100");

	SetPlayerArmour(targetid, arm);

	static const fmt_str[] = "������������� %s ��������� ��� �������� ����� �� %.0f";
	new string[sizeof fmt_str + (-2+MAX_PLAYER_NAME)+(-4+3)];

	format(
		string, sizeof string,
		fmt_str, 
		pData[playerid][pName], arm
	);
	SendClientMessage(targetid, -1, string);

	format(
		string, sizeof string,
		"�� ����������  �������� �������� �� %.0f ������ %s[%d]", 
		arm, pData[targetid][pName], targetid
	);
	return SendClientMessage(playerid, COLOR_GREEN, string);
}

flags:getcar(ADMIN_FOURTH);
CMD:getcar(playerid, params[])
{
	extract params -> new vehicleid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /getcar [ ID ]");

	if(IsValidVehicle(vehicleid) == 0)
		return SendClientMessage(playerid, COLOR_GREY, !"���� � ����� ���� �� ����������");

	new Float:pX, Float:pY, Float:pZ;

	GetPlayerPos(playerid, pX, pY, pZ);

	SetVehiclePos(vehicleid, pX + 0.7, pY, pZ);

	SetVehicleVirtualWorld(vehicleid, GetPlayerVirtualWorld(playerid));
	LinkVehicleToInterior(vehicleid, GetPlayerInterior(playerid));

	return SendClientMessage(playerid, COLOR_GREEN, !"�� ��������������� ���� � ����");
}

flags:gotocar(ADMIN_FOURTH);
CMD:gotocar(playerid, params[])
{
	extract params -> new vehicleid; else
		return SendClientMessage(playerid, COLOR_GREY, !"�������������: /gotocar [ ID ]");

	if(IsValidVehicle(vehicleid) == 0)
		return SendClientMessage(playerid, COLOR_GREY, !"���� � ����� ���� �� ����������");

	new Float:vX, Float:vY, Float:vZ;

	GetVehiclePos(playerid, vX, vY, vZ);
	SetPlayerPos(vehicleid, vX + 0.7, vY, vZ);
	SetPlayerVirtualWorld(playerid, GetVehicleVirtualWorld(vehicleid));

	return SendClientMessage(playerid, COLOR_GREEN, !"�� ����������������� � ����");
}

flags:givecarlic(ADMIN_FOURTH);
CMD:givecarlic(playerid, params[])
{
	extract params -> new player:targetid; else
	    return SendClientMessage(playerid, COLOR_GREY, !"�������������: /givecarlic [ ID ]");
	    
	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");
		
	pData[targetid][pCarLic][0] = 1;
	pData[targetid][pCarLic][1] = 1;
	pData[targetid][pCarLic][2] = 1;
	pData[targetid][pCarLic][3] = 1;
	
	UpdatePlayerData(playerid, "car_lic_a", pData[targetid][pCarLic][0]);
	UpdatePlayerData(playerid, "car_lic_b", pData[targetid][pCarLic][1]);
	UpdatePlayerData(playerid, "car_lic_c", pData[targetid][pCarLic][2]);
	UpdatePlayerData(playerid, "car_lic_d", pData[targetid][pCarLic][3]);
	
	new str_message[105];
	
	format(
		str_message, sizeof str_message,
		"������������� %s[%d] ����� ��� ������������ ������������� �� ��� ��������� (A,B,C,D).",
		pData[playerid][pName],playerid
	);
	SendClientMessage(targetid, COLOR_WHITE, str_message);
	
	format(
	   str_message, sizeof str_message,
	   "�� ������ ������ %s[%d] ������������ ������������� �� ��� ��������� (A,B,C,D).",
	   pData[targetid][pName], targetid
	);

    return SendClientMessage(playerid, COLOR_WHITE, str_message);
}

flags:takecarlic(ADMIN_FOURTH);
CMD:takecarlic(playerid, params[])
{
	extract params -> new player:targetid; else
	    return SendClientMessage(playerid, COLOR_GREY, !"�������������: /takecarlic [ ID ]");

	if(!pLogged[targetid])
		return SendClientMessage(playerid,COLOR_GREY, !"����� �� �����������");

	pData[targetid][pCarLic][0] = 0;
	pData[targetid][pCarLic][1] = 0;
	pData[targetid][pCarLic][2] = 0;
	pData[targetid][pCarLic][3] = 0;

	UpdatePlayerData(playerid, "car_lic_a", pData[targetid][pCarLic][0]);
	UpdatePlayerData(playerid, "car_lic_b", pData[targetid][pCarLic][1]);
	UpdatePlayerData(playerid, "car_lic_c", pData[targetid][pCarLic][2]);
	UpdatePlayerData(playerid, "car_lic_d", pData[targetid][pCarLic][3]);

	new str_message[105];

	format(
		str_message, sizeof str_message,
		"������������� %s[%d] ������ � ��� ������������ ������������� �� ��� ��������� (A,B,C,D).",
		pData[playerid][pName],playerid
	);
	SendClientMessage(targetid, COLOR_WHITE, str_message);

	format(
	   str_message, sizeof str_message,
	   "�� ������� � ������ %s[%d] ������������ ������������� �� ��� ��������� (A,B,C,D).",
	   pData[targetid][pName], targetid
	);

    return SendClientMessage(playerid, COLOR_WHITE, str_message);
}
