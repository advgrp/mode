-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Май 03 2017 г., 12:01
-- Версия сервера: 5.5.55-0+deb8u1
-- Версия PHP: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `gs3469`
--

-- --------------------------------------------------------

--
-- Структура таблицы `accounts`
--

CREATE TABLE `accounts` (
  `id` int(9) NOT NULL,
  `name` varchar(24) NOT NULL,
  `password` varchar(32) CHARACTER SET cp1251 COLLATE cp1251_bin NOT NULL,
  `email` varchar(32) NOT NULL DEFAULT 'none',
  `regip` varchar(16) NOT NULL,
  `regdata` varchar(12) NOT NULL,
  `sex` int(1) NOT NULL,
  `skin` int(3) NOT NULL,
  `health` int(3) NOT NULL DEFAULT '100',
  `armour` int(3) NOT NULL DEFAULT '0',
  `level` int(9) NOT NULL DEFAULT '1',
  `exp` int(11) NOT NULL DEFAULT '0',
  `playedtime` int(2) NOT NULL DEFAULT '0',
  `spawn` int(1) NOT NULL DEFAULT '1',
  `settings` int(2) NOT NULL DEFAULT '15',
  `admin` int(1) NOT NULL DEFAULT '0',
  `referalid` int(11) NOT NULL DEFAULT '0',
  `cash` int(11) NOT NULL DEFAULT '2000',
  `googleauth` varchar(16) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `password`, `email`, `regip`, `regdata`, `sex`, `skin`, `health`, `armour`, `level`, `exp`, `playedtime`, `spawn`, `settings`, `admin`, `referalid`, `cash`, `googleauth`) VALUES
(1, 'Ivan_Ivanov', 'b9519de4b87d90902f9532dd3674745b', 'none', '185.84.188.173', '26.04.2017', 1, 299, 100, 0, 2, 9, 0, 1, 15, 6, 0, 123000, '-1'),
(2, 'Andrey_Alex', 'cbd2df1416c8f6e787b38484f4e560f8', 'none', '93.77.172.122', '26.04.2017', 1, 24, 100, 0, 1, 0, 0, 1, 15, 5, 0, 2000, '-1'),
(3, 'Andrey_Kekhter', 'e10adc3949ba59abbe56e057f20f883e', 'none', '217.118.79.25', '26.04.2017', 1, 292, 100, 0, 2, 1, 0, 1, 15, 5, 1, 37150, '-1'),
(4, 'Martin_Ackles', 'e53aaa772440670f9a73f1582b610d97', 'synbulatov@mail.ru', '62.133.162.160', '26.04.2017', 1, 20, 100, 0, 1, 0, 0, 1, 15, 5, 0, 2000, '-1'),
(5, 'Alexey_Kulik', 'cfff10064b90d4f9527909eb6b6bc860', 'a2222222@gmail.com', '178.120.56.21', '26.04.2017', 1, 294, 100, 0, 1, 0, 0, 1, 15, 5, 0, 2200, '-1'),
(6, 'Nikita_Sever', 'a7bce2f55575e734eff385fbf60ea89c', 'gocs_csgo@mail.ru', '192.166.117.243', '26.04.2017', 1, 115, 100, 0, 1, 1, 0, 1, 15, 5, 0, 9000, '-1'),
(7, 'Evgeny_Makarov', '8ca66bb49b9b327069fe134e9978d6fd', 'ifhfas212@mail.ru', '194.8.85.233', '26.04.2017', 1, 24, 100, 0, 1, 0, 0, 1, 15, 5, 0, 2150, '-1'),
(8, 'Nick_Name', 'e10adc3949ba59abbe56e057f20f883e', 'none', '217.118.79.19', '26.04.2017', 1, 14, 100, 0, 1, 0, 0, 1, 15, 5, 0, 2000, '-1'),
(9, 'Vladimir_Makarevich', 'b9519de4b87d90902f9532dd3674745b', 'none', '185.84.189.114', '28.04.2017', 1, 299, 100, 0, 2, 3, 0, 1, 15, 5, 0, 56800, '-1'),
(10, 'Adlan_Eblanov', 'e1216a75b881f85acf6cc78baa8335bc', 'none', '146.158.86.96', '28.04.2017', 1, 17, 100, 0, 1, 1, 0, 1, 15, 5, 0, 9050, '-1'),
(11, 'Vyacheslav_Molotov', 'fdb15d3d7d61c835ba627fc8083229c8', 'fhfeh@gmail.com', '37.20.144.62', '28.04.2017', 1, 299, 1, 0, 1, 0, 0, 1, 15, 5, 0, 2650, '-1'),
(12, 'Vitaliy_Anc', '4297f44b13955235245b2497399d7a93', 'mac@mail.ru', '85.26.232.11', '28.04.2017', 1, 14, 100, 0, 1, 0, 0, 1, 15, 5, 0, 2550, '-1'),
(13, 'Villren_Dilarensi', '6a125dafae79670fb3fb4883b49e8b47', 'none', '146.158.86.96', '29.04.2017', 1, 14, 100, 0, 1, 0, 0, 1, 15, 5, 0, 2350, '-1');

-- --------------------------------------------------------

--
-- Структура таблицы `bans`
--

CREATE TABLE `bans` (
  `name` varchar(24) NOT NULL,
  `admname` varchar(24) NOT NULL,
  `reason` varchar(20) NOT NULL,
  `bandate` varchar(16) NOT NULL,
  `bantime` int(12) NOT NULL,
  `unbandate` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
