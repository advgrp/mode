#if defined _fix_time
	#endinput
#endif

#define _fix_time

#if !defined _fix_gettime_count
#define _fix_gettime_count 0
#endif

stock gettime_default(&hour=0,&minute=0,&second=0) return gettime(hour, minute, second);

stock _fix___gettime(&hour=0,&minute=0,&second=0)
{
	return (gettime(hour, minute, second) + (_fix_gettime_count));
}

#if defined _ALS_gettime
    #undef    gettime
#else
    #define    _ALS_gettime
#endif

#if !defined _fix_gettime_count
	#define _fix_gettime_count 0
#endif
#define gettime _fix___gettime


