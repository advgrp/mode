	SpeedTDDynamic[playerid][0] = CreatePlayerTextDraw(playerid, 488.500000, 378.875000, "ld_beat:left");
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][0], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, SpeedTDDynamic[playerid][0], 22.000000, 16.625000);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][0], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][0], -1);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][0], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][0], 1);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][0], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][0], 4);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][0], 1);

	SpeedTDDynamic[playerid][1] = CreatePlayerTextDraw(playerid, 604.000000, 379.000000, "ld_beat:right");
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][1], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, SpeedTDDynamic[playerid][1], 22.000000, 16.625000);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][1], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][1], -1);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][1], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][1], 1);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][1], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][1], 4);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][1], 1);

	SpeedTDDynamic[playerid][2] = CreatePlayerTextDraw(playerid, 502.500000, 395.062500, "60");//fuel
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][2], 0.229999, 1.534373);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][2], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][2], -1);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][2], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][2], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][2], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][2], 2);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][2], 1);

	SpeedTDDynamic[playerid][3] = CreatePlayerTextDraw(playerid, 503.500000, 411.250000, "0.0 KM");//mileage
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][3], 0.277498, 1.206248);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][3], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][3], -1);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][3], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][3], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][3], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][3], 2);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][3], 1);

	SpeedTDDynamic[playerid][4] = CreatePlayerTextDraw(playerid, 536.000000, 377.562500, "0 KM/H");//speed
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][4], 0.263500, 1.726873);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][4], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][4], -1);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][4], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][4], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][4], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][4], 2);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][4], 1);

	SpeedTDDynamic[playerid][5] = CreatePlayerTextDraw(playerid, 562.000000, 417.812500, "D");
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][5], 0.432498, 2.041872);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][5], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][5], 10092458);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][5], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][5], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][5], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][5], 2);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][5], 1);

	SpeedTDDynamic[playerid][6] = CreatePlayerTextDraw(playerid, 554.500000, 418.250000, "=");
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][6], 0.450498, 1.503749);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][6], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][6], 10092458);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][6], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][6], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][6], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][6], 1);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][6], 1);

	SpeedTDDynamic[playerid][7] = CreatePlayerTextDraw(playerid, 554.399597, 423.581420, "=");
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][7], 0.450498, 1.503749);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][7], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][7], 10092458);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][7], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][7], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][7], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][7], 1);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][7], 1);

	SpeedTDDynamic[playerid][8] = CreatePlayerTextDraw(playerid, 580.000000, 426.125000, "LD_SPAC:white");//lock
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][8], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, SpeedTDDynamic[playerid][8], 9.500000, 7.875000);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][8], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][8], -16776961);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][8], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][8], 0);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][8], 4);

	SpeedTDDynamic[playerid][9] = CreatePlayerTextDraw(playerid, 599.400207, 421.268737, "engine");
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][9], 0.198998, 1.302495);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][9], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][9], -1);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][9], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][9], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][9], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][9], 2);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][9], 1);

	SpeedTDDynamic[playerid][10] = CreatePlayerTextDraw(playerid, 534.000000, 399.437500, "");//vehname
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][10], 0.172499, 0.935000);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][10], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][10], -1);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][10], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][10], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][10], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][10], 2);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][10], 1);

	SpeedTDDynamic[playerid][11] = CreatePlayerTextDraw(playerid, 502.500000, 423.937500, "100%");
	PlayerTextDrawLetterSize(playerid, SpeedTDDynamic[playerid][11], 0.316000, 1.236873);
	PlayerTextDrawAlignment(playerid, SpeedTDDynamic[playerid][11], 1);
	PlayerTextDrawColor(playerid, SpeedTDDynamic[playerid][11], -1);
	PlayerTextDrawSetShadow(playerid, SpeedTDDynamic[playerid][11], 0);
	PlayerTextDrawSetOutline(playerid, SpeedTDDynamic[playerid][11], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedTDDynamic[playerid][11], 51);
	PlayerTextDrawFont(playerid, SpeedTDDynamic[playerid][11], 2);
	PlayerTextDrawSetProportional(playerid, SpeedTDDynamic[playerid][11], 1);