    JobLoaderTD[0][playerid] = CreatePlayerTextDraw(playerid, 632.099975, 181.750000, "usebox");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[0][playerid], 0.000000, 7.827495);
    PlayerTextDrawTextSize(playerid, JobLoaderTD[0][playerid], 530.000000, 0.000000);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[0][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[0][playerid], 0);
    PlayerTextDrawUseBox(playerid, JobLoaderTD[0][playerid], true);
    PlayerTextDrawBoxColor(playerid, JobLoaderTD[0][playerid], 102);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[0][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[0][playerid], 0);
    PlayerTextDrawFont(playerid, JobLoaderTD[0][playerid], 0);

    JobLoaderTD[1][playerid] = CreatePlayerTextDraw(playerid, 625.000000, 185.687500, "usebox");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[1][playerid], 0.000000, 1.287500);
    PlayerTextDrawTextSize(playerid, JobLoaderTD[1][playerid], 538.500000, 0.000000);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[1][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[1][playerid], 10092543);
    PlayerTextDrawUseBox(playerid, JobLoaderTD[1][playerid], true);
    PlayerTextDrawBoxColor(playerid, JobLoaderTD[1][playerid], 10092543);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[1][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[1][playerid], 0);
    PlayerTextDrawFont(playerid, JobLoaderTD[1][playerid], 0);

    JobLoaderTD[2][playerid] = CreatePlayerTextDraw(playerid, 554.650024, 183.749969, "STATS");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[2][playerid], 0.426550, 1.432000);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[2][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[2][playerid], -1);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[2][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[2][playerid], 0);
    PlayerTextDrawBackgroundColor(playerid, JobLoaderTD[2][playerid], 51);
    PlayerTextDrawFont(playerid, JobLoaderTD[2][playerid], 2);
    PlayerTextDrawSetProportional(playerid, JobLoaderTD[2][playerid], 1);

    JobLoaderTD[3][playerid] = CreatePlayerTextDraw(playerid, 537.700134, 211.006256, "Перенесено");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[3][playerid], 0.153299, 0.839622);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[3][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[3][playerid], -1);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[3][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[3][playerid], 0);
    PlayerTextDrawBackgroundColor(playerid, JobLoaderTD[3][playerid], 51);
    PlayerTextDrawFont(playerid, JobLoaderTD[3][playerid], 2);
    PlayerTextDrawSetProportional(playerid, JobLoaderTD[3][playerid], 1);

    JobLoaderTD[4][playerid] = CreatePlayerTextDraw(playerid, 537.600036, 225.043762, "Заработано");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[4][playerid], 0.146697, 0.805935);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[4][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[4][playerid], -1);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[4][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[4][playerid], 0);
    PlayerTextDrawBackgroundColor(playerid, JobLoaderTD[4][playerid], 51);
    PlayerTextDrawFont(playerid, JobLoaderTD[4][playerid], 2);
    PlayerTextDrawSetProportional(playerid, JobLoaderTD[4][playerid], 1);

    JobLoaderTD[5][playerid] = CreatePlayerTextDraw(playerid, 581.100280, 214.549987, "LD_SPAC:white");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[5][playerid], 0.000000, 0.000000);
    PlayerTextDrawTextSize(playerid, JobLoaderTD[5][playerid], 11.000000, 0.437500);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[5][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[5][playerid], -1);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[5][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[5][playerid], 0);
    PlayerTextDrawFont(playerid, JobLoaderTD[5][playerid], 4);

    JobLoaderTD[6][playerid] = CreatePlayerTextDraw(playerid, 581.400024, 228.937591, "LD_SPAC:white");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[6][playerid], 0.000000, 0.000000);
    PlayerTextDrawTextSize(playerid, JobLoaderTD[6][playerid], 11.000000, 0.437500);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[6][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[6][playerid], -1);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[6][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[6][playerid], 0);
    PlayerTextDrawFont(playerid, JobLoaderTD[6][playerid], 4);

    JobLoaderTD[7][playerid] = CreatePlayerTextDraw(playerid, 600.500671, 209.562591, "650");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[7][playerid], 0.254299, 1.011564);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[7][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[7][playerid], -5963521);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[7][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[7][playerid], 0);
    PlayerTextDrawBackgroundColor(playerid, JobLoaderTD[7][playerid], 51);
    PlayerTextDrawFont(playerid, JobLoaderTD[7][playerid], 2);
    PlayerTextDrawSetProportional(playerid, JobLoaderTD[7][playerid], 1);

    JobLoaderTD[8][playerid] = CreatePlayerTextDraw(playerid, 600.750244, 222.637588, "0");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[8][playerid], 0.171498, 1.095126);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[8][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[8][playerid], 8388863);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[8][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[8][playerid], 0);
    PlayerTextDrawBackgroundColor(playerid, JobLoaderTD[8][playerid], 51);
    PlayerTextDrawFont(playerid, JobLoaderTD[8][playerid], 2);
    PlayerTextDrawSetProportional(playerid, JobLoaderTD[8][playerid], 1);

    JobLoaderTD[9][playerid] = CreatePlayerTextDraw(playerid, 593.900024, 224.612503, "hud:radar_cash");
    PlayerTextDrawLetterSize(playerid, JobLoaderTD[9][playerid], 0.000000, 0.000000);
    PlayerTextDrawTextSize(playerid, JobLoaderTD[9][playerid], 5.949997, 7.612504);
    PlayerTextDrawAlignment(playerid, JobLoaderTD[9][playerid], 1);
    PlayerTextDrawColor(playerid, JobLoaderTD[9][playerid], -1);
    PlayerTextDrawSetShadow(playerid, JobLoaderTD[9][playerid], 0);
    PlayerTextDrawSetOutline(playerid, JobLoaderTD[9][playerid], 0);
    PlayerTextDrawFont(playerid, JobLoaderTD[9][playerid], 4);