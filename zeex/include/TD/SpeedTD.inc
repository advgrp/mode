	SpeedTDStatic[0] = TextDrawCreate(635.000000, 377.750000, "usebox");
	TextDrawLetterSize(SpeedTDStatic[0], 0.000000, 6.634722);
	TextDrawTextSize(SpeedTDStatic[0], 479.500000, 0.000000);
	TextDrawAlignment(SpeedTDStatic[0], 1);
	TextDrawColor(SpeedTDStatic[0], 102);
	TextDrawUseBox(SpeedTDStatic[0], true);
	TextDrawBoxColor(SpeedTDStatic[0], 336860415);
	TextDrawSetShadow(SpeedTDStatic[0], 0);
	TextDrawSetOutline(SpeedTDStatic[0], 0);
	TextDrawFont(SpeedTDStatic[0], 0);

	SpeedTDStatic[1] = TextDrawCreate(487.500000, 396.812500, "hud:radar_centre");
	TextDrawLetterSize(SpeedTDStatic[1], 0.449999, 1.600000);
	TextDrawTextSize(SpeedTDStatic[1], 14.000000, 10.500000);
	TextDrawAlignment(SpeedTDStatic[1], 1);
	TextDrawColor(SpeedTDStatic[1], -1);
	TextDrawSetShadow(SpeedTDStatic[1], 0);
	TextDrawSetOutline(SpeedTDStatic[1], 1);
	TextDrawBackgroundColor(SpeedTDStatic[1], 51);
	TextDrawFont(SpeedTDStatic[1], 4);
	TextDrawSetProportional(SpeedTDStatic[1], 1);

	SpeedTDStatic[2] = TextDrawCreate(489.500000, 411.250000, "hud:radar_impound");
	TextDrawLetterSize(SpeedTDStatic[2], 0.449999, 1.600000);
	TextDrawTextSize(SpeedTDStatic[2], 10.500000, 10.937500);
	TextDrawAlignment(SpeedTDStatic[2], 1);
	TextDrawColor(SpeedTDStatic[2], -1);
	TextDrawSetShadow(SpeedTDStatic[2], 0);
	TextDrawSetOutline(SpeedTDStatic[2], 1);
	TextDrawBackgroundColor(SpeedTDStatic[2], 51);
	TextDrawFont(SpeedTDStatic[2], 4);
	TextDrawSetProportional(SpeedTDStatic[2], 1);

	SpeedTDStatic[3] = TextDrawCreate(517.000000, 397.250000, "L");
	TextDrawLetterSize(SpeedTDStatic[3], 0.270498, 1.206248);
	TextDrawAlignment(SpeedTDStatic[3], 1);
	TextDrawColor(SpeedTDStatic[3], -1);
	TextDrawSetShadow(SpeedTDStatic[3], 0);
	TextDrawSetOutline(SpeedTDStatic[3], 0);
	TextDrawBackgroundColor(SpeedTDStatic[3], 51);
	TextDrawFont(SpeedTDStatic[3], 2);
	TextDrawSetProportional(SpeedTDStatic[3], 1);

	SpeedTDStatic[4] = TextDrawCreate(588.000000, 421.437500, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[4], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[4], 1.000000, 4.375000);
	TextDrawAlignment(SpeedTDStatic[4], 1);
	TextDrawColor(SpeedTDStatic[4], -1);
	TextDrawSetShadow(SpeedTDStatic[4], 0);
	TextDrawSetOutline(SpeedTDStatic[4], 0);
	TextDrawFont(SpeedTDStatic[4], 4);

	SpeedTDStatic[5] = TextDrawCreate(580.500000, 421.312500, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[5], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[5], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[5], 1);
	TextDrawColor(SpeedTDStatic[5], -1);
	TextDrawSetShadow(SpeedTDStatic[5], 0);
	TextDrawSetOutline(SpeedTDStatic[5], 0);
	TextDrawFont(SpeedTDStatic[5], 4);

	SpeedTDStatic[6] = TextDrawCreate(581.000000, 420.125000, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[6], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[6], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[6], 1);
	TextDrawColor(SpeedTDStatic[6], -1);
	TextDrawSetShadow(SpeedTDStatic[6], 0);
	TextDrawSetOutline(SpeedTDStatic[6], 0);
	TextDrawFont(SpeedTDStatic[6], 4);

	SpeedTDStatic[7] = TextDrawCreate(582.000000, 419.812500, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[7], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[7], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[7], 1);
	TextDrawColor(SpeedTDStatic[7], -1);
	TextDrawSetShadow(SpeedTDStatic[7], 0);
	TextDrawSetOutline(SpeedTDStatic[7], 0);
	TextDrawFont(SpeedTDStatic[7], 4);

	SpeedTDStatic[8] = TextDrawCreate(583.000000, 419.937500, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[8], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[8], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[8], 1);
	TextDrawColor(SpeedTDStatic[8], -1);
	TextDrawSetShadow(SpeedTDStatic[8], 0);
	TextDrawSetOutline(SpeedTDStatic[8], 0);
	TextDrawFont(SpeedTDStatic[8], 4);

	SpeedTDStatic[9] = TextDrawCreate(585.000000, 419.750000, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[9], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[9], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[9], 1);
	TextDrawColor(SpeedTDStatic[9], -1);
	TextDrawSetShadow(SpeedTDStatic[9], 0);
	TextDrawSetOutline(SpeedTDStatic[9], 0);
	TextDrawFont(SpeedTDStatic[9], 4);

	SpeedTDStatic[10] = TextDrawCreate(586.500000, 420.437500, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[10], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[10], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[10], 1);
	TextDrawColor(SpeedTDStatic[10], -1);
	TextDrawSetShadow(SpeedTDStatic[10], 0);
	TextDrawSetOutline(SpeedTDStatic[10], 0);
	TextDrawFont(SpeedTDStatic[10], 4);

	SpeedTDStatic[11] = TextDrawCreate(585.500000, 419.687500, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[11], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[11], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[11], 1);
	TextDrawColor(SpeedTDStatic[11], -1);
	TextDrawSetShadow(SpeedTDStatic[11], 0);
	TextDrawSetOutline(SpeedTDStatic[11], 0);
	TextDrawFont(SpeedTDStatic[11], 4);

	SpeedTDStatic[12] = TextDrawCreate(631.000000, 399.625000, "usebox");
	TextDrawLetterSize(SpeedTDStatic[12], 0.000000, 1.190276);
	TextDrawTextSize(SpeedTDStatic[12], 529.500000, 0.000000);
	TextDrawAlignment(SpeedTDStatic[12], 1);
	TextDrawColor(SpeedTDStatic[12], 0);
	TextDrawUseBox(SpeedTDStatic[12], true);
	TextDrawBoxColor(SpeedTDStatic[12], 255);
	TextDrawSetShadow(SpeedTDStatic[12], 0);
	TextDrawSetOutline(SpeedTDStatic[12], 0);
	TextDrawFont(SpeedTDStatic[12], 0);

	SpeedTDStatic[13] = TextDrawCreate(490.000000, 426.125000, "hud:radar_modGarage");
	TextDrawLetterSize(SpeedTDStatic[13], 0.449999, 1.600000);
	TextDrawTextSize(SpeedTDStatic[13], 9.500000, 9.625000);
	TextDrawAlignment(SpeedTDStatic[13], 1);
	TextDrawColor(SpeedTDStatic[13], -1);
	TextDrawSetShadow(SpeedTDStatic[13], 0);
	TextDrawSetOutline(SpeedTDStatic[13], 1);
	TextDrawBackgroundColor(SpeedTDStatic[13], 51);
	TextDrawFont(SpeedTDStatic[13], 4);
	TextDrawSetProportional(SpeedTDStatic[13], 1);

	SpeedTDStatic[14] = TextDrawCreate(580.000000, 421.750000, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[14], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[14], 1.000000, 4.375000);
	TextDrawAlignment(SpeedTDStatic[14], 1);
	TextDrawColor(SpeedTDStatic[14], -1);
	TextDrawSetShadow(SpeedTDStatic[14], 0);
	TextDrawSetOutline(SpeedTDStatic[14], 0);
	TextDrawFont(SpeedTDStatic[14], 4);

	SpeedTDStatic[15] = TextDrawCreate(584.000000, 419.625000, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[15], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[15], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[15], 1);
	TextDrawColor(SpeedTDStatic[15], -1);
	TextDrawSetShadow(SpeedTDStatic[15], 0);
	TextDrawSetOutline(SpeedTDStatic[15], 0);
	TextDrawFont(SpeedTDStatic[15], 4);

	SpeedTDStatic[16] = TextDrawCreate(587.500000, 420.750000, "LD_SPAC:white");
	TextDrawLetterSize(SpeedTDStatic[16], 0.000000, 0.000000);
	TextDrawTextSize(SpeedTDStatic[16], 1.000000, 0.875000);
	TextDrawAlignment(SpeedTDStatic[16], 1);
	TextDrawColor(SpeedTDStatic[16], -1);
	TextDrawSetShadow(SpeedTDStatic[16], 0);
	TextDrawSetOutline(SpeedTDStatic[16], 0);
	TextDrawFont(SpeedTDStatic[16], 4);

	SpeedTDStatic[17] = TextDrawCreate(631.500000, 423.687500, "usebox");
	TextDrawLetterSize(SpeedTDStatic[17], 0.000000, 1.044443);
	TextDrawTextSize(SpeedTDStatic[17], 593.500000, 0.000000);
	TextDrawAlignment(SpeedTDStatic[17], 1);
	TextDrawColor(SpeedTDStatic[17], 0);
	TextDrawUseBox(SpeedTDStatic[17], true);
	TextDrawBoxColor(SpeedTDStatic[17], 255);
	TextDrawSetShadow(SpeedTDStatic[17], 0);
	TextDrawSetOutline(SpeedTDStatic[17], 0);
	TextDrawFont(SpeedTDStatic[17], 0);