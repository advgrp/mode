    TD_server_logo[0] = TextDrawCreate(547.300231, 14.918751, "LD_BEAT:chit");
    TextDrawLetterSize(TD_server_logo[0], 0.000000, 0.000000);
    TextDrawTextSize(TD_server_logo[0], 32.549999, 25.462501);
    TextDrawAlignment(TD_server_logo[0], 1);
    TextDrawColor(TD_server_logo[0], 911736720);
    TextDrawSetShadow(TD_server_logo[0], 0);
    TextDrawSetOutline(TD_server_logo[0], 0);
    TextDrawFont(TD_server_logo[0], 4);

    TD_server_logo[1] = TextDrawCreate(557.649963, 18.637508, "A");
    TextDrawLetterSize(TD_server_logo[1], 0.449999, 1.600000);
    TextDrawAlignment(TD_server_logo[1], 1);
    TextDrawColor(TD_server_logo[1], -1);
    TextDrawSetShadow(TD_server_logo[1], 0);
    TextDrawSetOutline(TD_server_logo[1], 0);
    TextDrawBackgroundColor(TD_server_logo[1], 51);
    TextDrawFont(TD_server_logo[1], 2);
    TextDrawSetProportional(TD_server_logo[1], 1);

    TD_server_logo[2] = TextDrawCreate(576.649963, 16.931253, "dvantage");
    TextDrawLetterSize(TD_server_logo[2], 0.243449, 1.023814);
    TextDrawAlignment(TD_server_logo[2], 1);
    TextDrawColor(TD_server_logo[2], -1);
    TextDrawSetShadow(TD_server_logo[2], 0);
    TextDrawSetOutline(TD_server_logo[2], 0);
    TextDrawBackgroundColor(TD_server_logo[2], 51);
    TextDrawFont(TD_server_logo[2], 2);
    TextDrawSetProportional(TD_server_logo[2], 1);

    TD_server_logo[3] = TextDrawCreate(631.600280, 28.099998, "usebox");
    TextDrawLetterSize(TD_server_logo[3], 0.000000, 0.701110);
    TextDrawTextSize(TD_server_logo[3], 574.149902, 0.000000);
    TextDrawAlignment(TD_server_logo[3], 1);
    TextDrawColor(TD_server_logo[3], 0);
    TextDrawUseBox(TD_server_logo[3], true);
    TextDrawBoxColor(TD_server_logo[3], 102);
    TextDrawSetShadow(TD_server_logo[3], 0);
    TextDrawSetOutline(TD_server_logo[3], 0);
    TextDrawFont(TD_server_logo[3], 0);

    TD_server_logo[4] = TextDrawCreate(578.100280, 26.337501, "RolePlay");
    TextDrawLetterSize(TD_server_logo[4], 0.247750, 0.944625);
    TextDrawAlignment(TD_server_logo[4], 1);
    TextDrawColor(TD_server_logo[4], -1);
    TextDrawSetShadow(TD_server_logo[4], 1);
    TextDrawSetOutline(TD_server_logo[4], 0);
    TextDrawBackgroundColor(TD_server_logo[4], 51);
    TextDrawFont(TD_server_logo[4], 2);
    TextDrawSetProportional(TD_server_logo[4], 1);
