	RegSkinTD[0] = TextDrawCreate(119.500000, 212.666656, "usebox");
	TextDrawLetterSize(RegSkinTD[0], 0.000000, 12.007406);
	TextDrawTextSize(RegSkinTD[0], 5.500000, 0.000000);
	TextDrawAlignment(RegSkinTD[0], 1);
	TextDrawColor(RegSkinTD[0], 0);
	TextDrawUseBox(RegSkinTD[0], true);
	TextDrawBoxColor(RegSkinTD[0], 102);
	TextDrawSetShadow(RegSkinTD[0], 0);
	TextDrawSetOutline(RegSkinTD[0], 0);
	TextDrawFont(RegSkinTD[0], 0);

	RegSkinTD[1] = TextDrawCreate(119.500000, 212.666656, "usebox");
	TextDrawLetterSize(RegSkinTD[1], 0.000000, 1.498149);
	TextDrawTextSize(RegSkinTD[1], 5.500000, 0.000000);
	TextDrawAlignment(RegSkinTD[1], 1);
	TextDrawColor(RegSkinTD[1], 0);
	TextDrawUseBox(RegSkinTD[1], true);
	TextDrawBoxColor(RegSkinTD[1], 10092458);
	TextDrawSetShadow(RegSkinTD[1], 0);
	TextDrawSetOutline(RegSkinTD[1], 0);
	TextDrawFont(RegSkinTD[1], 0);

	RegSkinTD[2] = TextDrawCreate(13.125000, 212.333328, "����� ���������");
	TextDrawLetterSize(RegSkinTD[2], 0.225620, 1.378332);
	TextDrawAlignment(RegSkinTD[2], 1);
	TextDrawColor(RegSkinTD[2], -1);
	TextDrawSetShadow(RegSkinTD[2], 1);
	TextDrawSetOutline(RegSkinTD[2], 0);
	TextDrawBackgroundColor(RegSkinTD[2], 51);
	TextDrawFont(RegSkinTD[2], 2);
	TextDrawSetProportional(RegSkinTD[2], 1);

	RegSkinTD[3] = TextDrawCreate(103.125000, 214.083343, "hud:radar_tshirt");
	TextDrawLetterSize(RegSkinTD[3], 0.449999, 1.600000);
	TextDrawTextSize(RegSkinTD[3], 12.500000, 12.249999);
	TextDrawAlignment(RegSkinTD[3], 1);
	TextDrawColor(RegSkinTD[3], -1);
	TextDrawSetShadow(RegSkinTD[3], 0);
	TextDrawSetOutline(RegSkinTD[3], 1);
	TextDrawBackgroundColor(RegSkinTD[3], 51);
	TextDrawFont(RegSkinTD[3], 4);
	TextDrawSetProportional(RegSkinTD[3], 1);

	RegSkinTD[4] = TextDrawCreate(13.250000, 265.666809, "ld_beat:left");
	TextDrawLetterSize(RegSkinTD[4], 0.449999, 1.600000);
	TextDrawTextSize(RegSkinTD[4], 21.875000, 16.916662);
	TextDrawAlignment(RegSkinTD[4], 1);
	TextDrawColor(RegSkinTD[4], -1);
	TextDrawSetShadow(RegSkinTD[4], 0);
	TextDrawSetOutline(RegSkinTD[4], 1);
	TextDrawBackgroundColor(RegSkinTD[4], 51);
	TextDrawFont(RegSkinTD[4], 4);
	TextDrawSetProportional(RegSkinTD[4], 1);
	TextDrawSetSelectable(RegSkinTD[4], true);

	RegSkinTD[5] = TextDrawCreate(91.625000, 265.833435, "ld_beat:right");
	TextDrawLetterSize(RegSkinTD[5], 0.449999, 1.600000);
	TextDrawTextSize(RegSkinTD[5], 21.875000, 16.916662);
	TextDrawAlignment(RegSkinTD[5], 1);
	TextDrawColor(RegSkinTD[5], -1);
	TextDrawSetShadow(RegSkinTD[5], 0);
	TextDrawSetOutline(RegSkinTD[5], 1);
	TextDrawBackgroundColor(RegSkinTD[5], 51);
	TextDrawFont(RegSkinTD[5], 4);
	TextDrawSetProportional(RegSkinTD[5], 1);
	TextDrawSetSelectable(RegSkinTD[5], true);

	RegSkinTD[6] = TextDrawCreate(90.750000, 307.166595, "usebox");
	TextDrawLetterSize(RegSkinTD[6], 0.000000, 1.433333);
	TextDrawTextSize(RegSkinTD[6], 33.000000, 0.000000);
	TextDrawAlignment(RegSkinTD[6], 1);
	TextDrawColor(RegSkinTD[6], 0);
	TextDrawUseBox(RegSkinTD[6], true);
	TextDrawBoxColor(RegSkinTD[6], -5963521);
	TextDrawSetShadow(RegSkinTD[6], 0);
	TextDrawSetOutline(RegSkinTD[6], 0);
	TextDrawFont(RegSkinTD[6], 0);

	RegSkinTD[7] = TextDrawCreate(39.375000, 306.833282, "�������");
	TextDrawLetterSize(RegSkinTD[7], 0.238122, 1.337499);
	TextDrawTextSize(RegSkinTD[7], 84.375000, 43.166667);
	TextDrawAlignment(RegSkinTD[7], 1);
	TextDrawColor(RegSkinTD[7], -1);
	TextDrawSetShadow(RegSkinTD[7], 1);
	TextDrawSetOutline(RegSkinTD[7], 0);
	TextDrawBackgroundColor(RegSkinTD[7], 51);
	TextDrawFont(RegSkinTD[7], 2);
	TextDrawSetProportional(RegSkinTD[7], 1);
	TextDrawSetSelectable(RegSkinTD[7], true);

	RegSkinTD[8] = TextDrawCreate(43.750000, 249.083343, "SKIN ID:");
	TextDrawLetterSize(RegSkinTD[8], 0.263123, 1.185832);
	TextDrawAlignment(RegSkinTD[8], 1);
	TextDrawColor(RegSkinTD[8], -1);
	TextDrawSetShadow(RegSkinTD[8], 1);
	TextDrawSetOutline(RegSkinTD[8], 0);
	TextDrawBackgroundColor(RegSkinTD[8], 51);
	TextDrawFont(RegSkinTD[8], 2);
	TextDrawSetProportional(RegSkinTD[8], 1);

	//---------------------------------[Male Skin TD]--------------------------------------
	{
		static str[4];
		for(new i; i < MALE_SKINS_COUNT; i++)
		{
			format(str, sizeof str, "%d", RegSkins[SKIN_TYPE_MALE-1][i]);
			RegSkinNumbTD[SKIN_TYPE_MALE-1][i] = TextDrawCreate(56.875000, 265.999908, str);
			TextDrawLetterSize(RegSkinNumbTD[SKIN_TYPE_MALE-1][i], 0.449999, 1.600000);
			TextDrawAlignment(RegSkinNumbTD[SKIN_TYPE_MALE-1][i], 1);
			TextDrawColor(RegSkinNumbTD[SKIN_TYPE_MALE-1][i], -1);
			TextDrawSetShadow(RegSkinNumbTD[SKIN_TYPE_MALE-1][i], 1);
			TextDrawSetOutline(RegSkinNumbTD[SKIN_TYPE_MALE-1][i], 0);
			TextDrawBackgroundColor(RegSkinNumbTD[SKIN_TYPE_MALE-1][i], 51);
			TextDrawFont(RegSkinNumbTD[SKIN_TYPE_MALE-1][i], 2);
			TextDrawSetProportional(RegSkinNumbTD[SKIN_TYPE_MALE-1][i], 1);
		}
	}
	//---------------------------------[/Male Skin TD]--------------------------------------
	//---------------------------------[Female Skin TD]-------------------------------------
	{
		static str[4];
		for(new i; i < FEMALE_SKINS_COUNT; i++)
		{
			format(str, sizeof str, "%d", RegSkins[SKIN_TYPE_FEMALE-1][i]);
			RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i] = TextDrawCreate(56.875000, 265.999908, str);
			TextDrawLetterSize(RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i], 0.449999, 1.600000);
			TextDrawAlignment(RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i], 1);
			TextDrawColor(RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i], -1);
			TextDrawSetShadow(RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i], 1);
			TextDrawSetOutline(RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i], 0);
			TextDrawBackgroundColor(RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i], 51);
			TextDrawFont(RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i], 2);
			TextDrawSetProportional(RegSkinNumbTD[SKIN_TYPE_FEMALE-1][i], 1);
		}
	}
	//---------------------------------[/Female Skin TD]-------------------------------------